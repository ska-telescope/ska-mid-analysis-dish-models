"""
    Copyright @ 2019 SARAO (http://www.ska.ac.za). All rights reserved.
    
    A simulations package for signals and signal processing.
    One possible example of use:
    
    <code>
    import numpy as np
    from rfsim.elements import OVERSAMPLE, ClockGenerator, ReceptorSimulator, siggen
    from rfsim.twoports import bpfilter
    
    
    class BPSimulator(ReceptorSimulator):
        def apply_analogue(self, t, s, **kwargs):
            s = self.make_refplane(t, s)
            s = bpfilter(s, [self.f_low,self.f_high], self.f_s, 0.1, [0.9*self.f_low,self.f_high*1.1], 30, ftype='cheby1')
            return t, s
    
    
    clkgen = ClockGenerator([0.1,1,10,100,1e3,10e3,100e3,1e6], [-13,-43,-73,-103,-128,-146,-148,-148], f_clock=4e9) # Hz, dBc/Hz
    print("Clock jitter: %g [sec]"%clkgen.jitter_rms(f_min=10, f_max=1e6))
    sim = BPSimulator("L-band", 1e9,1.6e9, clkgen.f_clock, nbits=8,dBmFS=-90, CLKGEN=clkgen, TABULATE=["SFDR","SINAD","ENOB"])
    
    # 2**18 sample points
    t = np.arange(0, 2**18)*1./(OVERSAMPLE*clkgen.f_clock)
    IN = siggen.WhiteNoiseSignal(t, Teq=3)
    t_s, OUT = sim.evaluate(t, IN)
    </code>
"""
