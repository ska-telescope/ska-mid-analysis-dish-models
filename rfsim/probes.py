"""
    Copyright @ 2019 SARAO (http://www.ska.ac.za). All rights reserved.
    
    A framework for probing & measuring signals.
    
    The following naming conventions are employed so that the probe-related code stand out differently from the actual business-level code:
    1) "root functions" start with a single underscore (e.g. _probe())
    2) Concrete classes are defined in ALL CAPS (e.g. TIMESERIES)
    3) Abstract classes are defined in the usual CamelCase (e.g. Probe).
    Of course there are exceptions.
    
    @author aph@ska.ac.za
"""
from __future__ import division
import atexit, pylab
import numpy as np
import scipy.signal as sig
from rfsim.base import kB, MHz, microsec, signal_power, scale_signal, interp, fft_filterbank, im_freqs, mean_dB, sum_dB, circular_buffer, _test_
from rfsim.twoports import sample, bpfilter, medfilt, medfilt2d
try:
    import mkl_fft._numpy_fft as np_fft
except:
    np_fft = np.fft


def _probe(label, signal, *probes):
    for p in probes:
        result = p(signal=signal,label=label) if (p.__class__ not in _probe.suppressed) else None
    return result
_probe.suppressed = [] # Specific subclasses to suppress


class Probe(object): # For _probe()
    def __init__(self, samplerate):
        """ @param samplerate: no discrete signal is completely defined without an explicit samplerate. """
        self.samplerate = samplerate
        self.__loggers__ = []
    
    def add_logger(self, logger):
        """ @param logger: a function that takes (label, return value of 'evaluate()') """
        self.__loggers__.append(logger)
        return logger
    
    def remove_logger(self, logger):
        try:
            self.loggers__.remove(logger)
        except:
            pass
    
    def __call__(self, signal=None, label=None, f_s=None, nyqzone=-1, *args, **kwargs):
        """ @param signal: if not null this is the data series to pass to evaluate(), otherwise evaluate() is not invoked.
            @param f_s: specify this if you wish the signal to be resampled from f_s to .samplerate before evaluation (default None)
            @param nyqzone: the Nyquist zone to isolate by filtering after re-sampling as per f_s (default <0 i.e. no filtering).
            @param kwargs: instance attributes to override for next call only, the rest will be passed as "evaluate(**kwargs)" on next call only.
            @return: the result from evaluate(), which is either self or some numeric result """
        # A copy of the "preset" settings
        presets = self.__dict__.get("__presets__", dict(self.__dict__))
        # Override settings as specified, leaving non-overrides in kwargs.
        for k in self.__dict__.keys():
            if not k.startswith("__"): # Leave hidden attributes
                try:
                    self.__dict__[k] = kwargs.pop(k, self.__dict__[k])
                except:
                    pass
        eval_args = self.__dict__.get("__eval_args__", args)
        eval_kwargs = self.__dict__.get("__eval_kwargs__", kwargs)
        
        if (signal is None): # Nothing to evaluate this round, so cache the settings to pass for the next call
            self.__presets__ = presets
            self.__eval_args__ = eval_args
            self.__eval_kwargs__ = eval_kwargs
            eval_kwargs.update({"f_s":f_s, "nyqzone":nyqzone}) # Store these to pull out in next round when signal is evaluated
            return self
        else: # Evaluate signal, and reset afterwards
            f_s, nyqzone = eval_kwargs.pop("f_s",f_s), eval_kwargs.pop("nyqzone",nyqzone) # These only get used here, never propagates down!
            try:
                signal = self._resample_(signal, f_s, nyqzone)
                result = self.evaluate(signal, label, *eval_args, **eval_kwargs)
                for logger in self.__loggers__:
                    try:
                        logger(label, result, f_s=f_s)
                    except Exception as e:
                        print("ERROR logging result with %s. %s"%(logger, e))
                return result
            finally: # Restore settings only after call to evaluate()
                for k in self.__dict__.keys():
                    if not k.startswith("__"): # Leave hidden attributes
                        try:
                            self.__dict__[k] = presets.pop(k, self.__dict__[k])
                        except:
                            pass
                try: # These may not exist in some call scenarios
                    del self.__presets__, self.__eval_args__, self.__eval_kwargs__
                except:
                    pass

    def _resample_(self, signal, f_s, nyqzone=0):
        """ Re-sample signal from f_s to match self.samplerate. If nyqzone is also specified (i.e. >0) then
            the re-sampled signal is also band-limited to isolate just the specified nyquist zone. """
        if f_s and (f_s != self.samplerate):
            signal = sample(signal, f_s, self.samplerate, nyqzone=nyqzone)[1]
        return signal
    
    def evaluate(self, signal, label, *args, **kwargs):
        """ @param signal: the signal to evaluate. Note: __call__() only invokes evaluate() if signal is not null.
            @return: the result of the evaluation of the signal. """
        raise Exception("Abstract class, method not defined!")


MARKERS = "ov^<>1234sp*hH+xDd|_" # All marker symbols that are recognised for plots

class LOGGER(object):
    """ May be registered as logger with Probe """
    def __init__(self, ylabel, *eval_functions):
        """ @param eval_functions: one or more functions that takes (return value of 'Probe.evaluate()') """
        self._MARKERS_ = circular_buffer(MARKERS)
        self.eval_functions = eval_functions
        self.clear()
        self.representation(ylabel)
    
    def clear(self):
        """ Clears all logged results. Note: does not reset self._MARKERS_. """
        self.logged_keys = {}
        self.logged_values = {}
        for evf in self.eval_functions:
            self.logged_keys[evf] = []
            self.logged_values[evf] = []
    
    def merge_from(self, _logger):
        """ Old eval functions are discarded, results are preserved. Used by TESTTONETOOL.adjust_freq() """
        for e in range(len(_logger.eval_functions)):
            self.logged_keys[_logger.eval_functions[e]] = self.logged_keys[self.eval_functions[e]]
            self.logged_values[_logger.eval_functions[e]] = self.logged_values[self.eval_functions[e]]
        self.eval_functions = _logger.eval_functions
    
    def __call__(self, label, probe_result, f_s=None): # Called from Probe, after evaluate().
        for evf in self.eval_functions:
            try:
                log_result = evf(probe_result, f_s=f_s)
            except: # The simplest functions don't handle additional arguments
                log_result = evf(probe_result)
            self.logged_keys[evf].append(label)
            self.logged_values[evf].append(log_result)
    
    def representation(self, ylabel, transform=lambda x: x):
        """ Define the default ylabel & transform to be used by tabulate() & plot().
            @param ylabel: the text label describing what the transformed value is
            @param transform: like 'lambda x: change(x)'
        """
        self.ylabel = ylabel
        self.transform = transform
    
    def plot(self, fmt='-', markers=True, match=None, title=None, grid=None, subplot=None, transform=None, legend_loc='best', **kwargs):
        """ @param fmt: line format with or without marker -- in the latter case line format gets combined with 'markers'.
            @param markers: sequence of marker symbols, as accepted by matplotlib; non-string defaults to self._MARKERS_ (default True).
            @param title: the title to set if a new figure is created (default None).
            @param match: if given as 'lambda key: boolean', will only plot values whose keys 'match' (default None).
            @param grid: True or False to add or remove the grid from the plot (default None ie. ignore).
            @param subplot: standard matplotlib-like subplot argument (default None)
            @param kwargs: passed on to pylab.plot(), with special treatment for "fig" & "ylabel" 
            @return: the figure
        """
        try:
            fig = kwargs.pop("fig")
            pylab.sca(fig.gca())
        except:
            fig = pylab.figure()
            if title: # Only if it's a new figure
                pylab.suptitle(title)
        if not (subplot is None):
            fig.add_subplot(subplot)
        
        ylabel = kwargs.pop("ylabel", self.ylabel)
        
        kvts = self._get_plot_values_and_tags_()
        if match:
            kvts = [(k,v,t) for (k,v,t) in kvts if match(k)]
        transform = self.transform if transform is None else transform
        values_ = transform(np.asarray([v for (k,v,t) in kvts])) # Transform all values across all dimensions at once
        kvts = [(k,v_,t) for (k,v,t),v_ in zip(kvts,np.atleast_2d(values_))] # Only keeps first "tag" if dimensions have been reduced
        
        # Set up formats & markers
        if (fmt[-1:] in MARKERS): # 'fmt' can combine both linestyle & markers (like matplotlib allows), in which case we need to split them 
            markers = fmt[-1:] + (markers if isinstance(markers, str) else '') 
            fmt = fmt[:-1]
        markers = circular_buffer(markers) if isinstance(markers, str) else self._MARKERS_
        fmts = [fmt+m for m in markers.clip(len(kvts))]
        
        for (keys,values,tag),fmt in zip(kvts,fmts):
            plot_cat(keys, values, fmt, label=tag+ylabel, ordered=False, **kwargs)
        legend = pylab.legend(loc=legend_loc, fontsize='small')
        if legend is not None:
            legend.get_frame().set_alpha(0.7)
        if not (grid is None):
            pylab.grid(grid)
        return fig
    
    def tabulate(self, match=None, transform=None, round_decimals=None, **kwargs):
        """ Prints out the logged values, in tabular format (row 1: header, row 2: values
            @param match: if given as 'lambda key: boolean', will only plot values whose keys 'match' (default None).
            @param kwargs: special treatment for "ylabel" 
            @return: [(keys, values, label_tag)...] as logged by all eval_functions """
        ylabel = kwargs.pop("ylabel", self.ylabel)
        
        kvts = self._get_plot_values_and_tags_()
        if match:
            kvts = [(k,v,t) for (k,v,t) in kvts if match(k)]
        transform = self.transform if transform is None else transform
        values_ = transform(np.asarray([v for (k,v,t) in kvts])) # Transform all values across all dimensions at once
        kvts = [(k,v_,t) for (k,v,t),v_ in zip(kvts,np.atleast_2d(values_))] # Only keeps first "tag" if dimensions have been reduced
        
        clip = lambda x: x if (round_decimals is None) else np.round(x, round_decimals)
        for (keys,values,tag) in kvts:
            print("\t\t" + "\t".join(keys))
            print(tag+ylabel + "\t\t"+ "\t".join(map(str,clip(values))))
        return kvts
    
    def _get_plot_values_and_tags_(self):
        """ @return: [(keys, values, label_tag)...] as logged by all eval_functions """
        k_v_t = []
        for i,evf in enumerate(self.eval_functions):
            tag = "" if (len(self.eval_functions) == 1) else "%drd "%i
            k_v_t.append((self.logged_keys[evf], self.logged_values[evf], tag))
        return k_v_t


def FX_detector(f, X, f_bound, func, f_ignore=[], discard_pct=0):
    """ An array power detector, e.g. for processing "filterbank" output. It ignores nan's. 
        @param f: the measurement points.
        @param X: values at each of the points of 'f' (nan's are ignored)
        @param f_bound: (f_min,f_max) to create the selection mask out of 'f', or a list of such pairs
        @param func: any function such as np.sum, np.mean or sum_dB, mean_dB.
        @param f_ignore: list of (f_min,f_max) ranges to ignore.
        @param discard_pct: only the central (1-pct/100) fraction of values are considered (default 0).
        @return: func(selection_from(X)) """
    mask = f < np.min(f) # All False i.e. nothing selected
    f_bound = [f_bound] if np.isscalar(f_bound[0]) else f_bound
    for f_b in f_bound: # Each pair opens up a part of the mask
        mask = mask | ((f>=f_b[0]) & (f<=f_b[1]))
    for f_b in f_ignore: # Each pair deletes a part of the mask
        mask = mask & ~((f>=f_b[0]) & (f<=f_b[1]))
    selected = X[mask]
    selected = selected[~np.isnan(selected)]
    if (discard_pct > 0):
        low = np.percentile(selected, discard_pct/2.)
        high = np.percentile(selected, 100-discard_pct/2.)
        selected = selected[np.logical_and(selected>=low, selected<=high)]
    return func(selected)


class FX_LOGGER(LOGGER):
    """ A logger employing lambda (f,X): FX_detector(f,X)s at a number of specified frequencies. """
    def __init__(self, ylabel, frequencies, RBW, det_func=np.mean, discard_pct=0, **kwargs):
        """ Sets up FX_detectors in RBW centered at specified frequencies.
            @param ylabel: the label for the values that are logged (should match units of 'X')
            @param frequencies: the frequencies at which to perform detection (units matching 'f').
            @param RBW: the bandwidth (units matching frequencies) within which to perform detection.
            @param det_func: the detector function, typically sum, mean, max or sum_dB, mean_dB (default np.mean).
            @param discard_pct: only the central (1-pct/100) fraction of values are considered (default 0).
        """
        self.frequencies = frequencies
        detectors = [lambda fX: FX_detector(*fX,f_bound=(f-RBW/2.,f+RBW/2.),func=det_func,
                                            discard_pct=discard_pct) for f in frequencies]
        LOGGER.__init__(self, ylabel, *detectors, **kwargs)
    

def PEAK_detector(f, X, f_bound, f_res, X_min):
    """ An array peak detector, e.g. for processing "filterbank" output. It ignores nan's. 
        @param f: the measurement points.
        @param X: values at each of the points of 'f' (nan's are ignored)
        @param f_bound: (f_min,f_max) to create the selection mask out of 'f'
        @param f_res: the resolution with which to blank out already found peaks.
        @param X_min: the lower threshold for X.
        @return: [selection_from(f), selection_from(X)] """
    selected_f = f[np.logical_and(f>=f_bound[0], f<=f_bound[1])] if f_bound else f
    selected_X = X[np.logical_and(f>=f_bound[0], f<=f_bound[1])] if f_bound else X
    # Average down resolution
    N = int((np.max(selected_f)-np.min(selected_f)+1)/f_res)
    decimate = int(len(selected_f)/N)
    selected_X = np.reshape(selected_X[:decimate*N],(N,decimate))
    max_X = selected_X.max(axis=1) # Max envelope at lower resolution
    selected_f = np.reshape(selected_f[:decimate*N],(N,decimate))
    # Discard points below threshold
    accept = max_X>=X_min
    max_X = max_X[accept]
    # Select the corresponding f
    selected_X = selected_X[accept,:]
    selected_f = selected_f[accept,:]
    max_f = np.diag(np.take(selected_f, np.argmax(selected_X, axis=1), axis=1))
    return (max_f, max_X)


@_test_()
def _test_PEAK_detector_(_test_):
    f = np.arange(256) # CAUTION: some combinations of len(f) & f_res make that one fewer spike is detected, then the tests below break
    
    # One spike per resolution increment exceeds X_min=0.5
    for f_res in [2,4,5,8,16]:
        X = 0.0*f+0.5-(np.random.randint(0,3,len(f))/3.) # All below threshold
        _F = np.arange(f.min(),f.max(),f_res) # Where the spikes are
        X[_F] = 0.5+(np.random.randint(0,6,len(_F))/3.) # These above threshold
        _f, _X = PEAK_detector(f, X, f_bound=None, f_res=f_res, X_min=0.5)
        _test_.assert_isclose(_F, _f, "Wrong frequencies identified")
        _test_.assert_isclose(np.take(X,_F), _X, "Wrong peaks identified")
        _test_.assert_isclose(np.take(X,_F), _X, "Wrong peaks identified")
    
    # Multiple spikes per resolution increment exceeds X_min=0.5
    for f_res in [4,5,16]:
        X = 0.0*f+0.5-(np.random.randint(0,3,len(f))/3.) # All below threshold
        _F = np.arange(f.min(),f.max(),f_res) # Where the spikes are
        X[_F] = 0.5+(np.random.randint(0,6,len(_F))/3.) # These above threshold
        for i in range(1,f_res,3):
            X[(_F+i)[:-1]] = X[_F[:-1]]-0.01 # These are just below the above peaks
        _f, _X = PEAK_detector(f, X, f_bound=None, f_res=f_res, X_min=0.5)
        _test_.assert_isclose(_F, _f, "Wrong frequencies identified")
        _test_.assert_isclose(np.take(X,_F), _X, "Wrong peaks identified")
        _test_.assert_isclose(np.take(X,_F), _X, "Wrong peaks identified")


def find_peaks(t_s, signal, f_bound, f_res, f_scale, PSD_min, debug=False):
    """
        @param f_bound: the min & max frequency within which to search.
        @param f_res: the bin resolution within which to look for peaks.
        @param f_scale: scale for frequency
        @param PSD_min: only points that exceed this threshold are returned.
        @return (freqs_peak, PSD_peak)
    """
    _PSD = PSD(1/np.diff(t_s).mean(), Navg=2, xscale=f_scale)
    _PL = _PSD.add_logger(LOGGER("Peak f,X", lambda fX: PEAK_detector(*fX, f_bound=f_bound, f_res=f_res, X_min=PSD_min)))
    _PSD(signal, label=("Peaks >= %g"%PSD_min) if debug else "")
    f_Pk = _PL._get_plot_values_and_tags_()[0][1][0] # i.e. [(keys, values, tag)][0][1] -> values; just one log point hence final [0]
    return f_Pk # (frequency, PSD_value)


class IM_LOGGER(LOGGER):
    """ A logger specifically for use by TESTTONETOOL to attach to PSD probes to detect intermodulation
        spurs from the generated test tones.
        The values logged are (f,X) in the same units as displayed by the PSD probe. Assumes X is in dB.
    """
    def __init__(self, detector, f1, f2, RBW, intermod_orders, f_ignore=None, freq_scale=MHz, **kwargs):
        """ Detectors compute tones in RBW centered at expected intermod frequencies, as per "func".
            @param detector: the detector to apply across RBW, either "peak","mean" or "integrate".
            @param f1, f2: the frequencies of the two test tones in same units as will be presented to this logger.
            @param RBW: the bandwidth (units matching f1,f2) within which to detect the intermodulation component.
            @param intermod_orders: a list of integers >= 1 identifying the intermodulation products to detect & log, e.g. [2,3]
            @param f_ignore: [(f_min,f_max)..] identifying frequency ranges to ignore, (default None).
            @param freq_scale: frequency scale to assume when applying detector="integrate" (default MHz).
        """
        self._detect_ = detector
        
        self.intermod_orders = list(intermod_orders)
        self.im_frequencies = set() # All intermod frequencies (including fundamentals) without duplicates
        self._identify_im_ = lambda f_im: "%.f^%.f->%s"%(f1,f2,f_im) # f_im may not always be scalar
        
        def make_detector(order): # Each detector creates a list including all (m*f1+n*f2, m*f1-n*f2) where m+n=order
            f_im = im_freqs(f1, f2, order, None if order==1 else f_ignore) # Fundamentals must not be ignored
            self.im_frequencies ^= set(f_im)
            def nyq_ignore(f):
                f = abs(f)
                for (f_min,f_max) in (f_ignore if (f_ignore is not None) else []):
                    if (f>f_min and f<f_max):
                        return True
                return (f in [0,f1,f2]) # Also ignore DC and aliases of fundamental frequencies
            def map_nyq(freqs,f_s): # Map aliased intermods to their Nyquist equivalents, except if it would coincide with a fundamental
                if (f_s is not None and order > 1):
                    freqs_s = [f if (f < f_s/2./freq_scale) else -(f_s/freq_scale-f) for f in freqs]
                    freqs_s = set(freqs_s) - set([-f1,-f2]) # Omit tones that alias into f_ignore or onto the fundamental frequencies
                    freqs = [f for f in freqs_s if not nyq_ignore(f)]
                return freqs
            det_func = lambda f: {"peak":np.max, "mean":mean_dB, \
                                  "integrate":lambda X: sum_dB(X+10*np.log10(np.diff(f).mean()*freq_scale))}[detector]
            return lambda fX,f_s: [(f,FX_detector(fX[0],fX[1],f_bound=(abs(f)-RBW/2.,abs(f)+RBW/2.),func=det_func(fX[0]))) for f in map_nyq(f_im,f_s)]
        detectors = [make_detector(order) for order in self.intermod_orders]
        
        LOGGER.__init__(self, "IMD", *detectors, **kwargs)
    
    def representation(self, ylabel):
        """ @param ylabel: "IMD" or "SFDR", to toggle between IMD [dBm] or HARMONIC SFDR [dBc] """
        self._as_IMD_ = (ylabel!="SFDR")
        if self._as_IMD_:
            LOGGER.representation(self, ylabel+(" [dBm/RBW]" if (self._detect_=="integrate") else (" %s [dBm/Hz]"%self._detect_)))
        else:
            LOGGER.representation(self, "HARMONIC SFDR [dBc]", transform=lambda x:x)
    # TODO: representation & _get_plot_values_and_tags_ are terribly confounded, can we use self.transform more elegantly?
    def _get_plot_values_and_tags_(self):
        """ @return: [(keys, values, label_tag)...] as logged by all eval_functions """
        k_v_t = []
        
        def find_max(logged_values): # return np.max(logged_values, axis=1) and corresponding frequencies
            # logged_values : logpoint x frequency x level
            # I can't figure out numpy axes for this one!@
            f_im, X_max = [], []
            for logpoint in range(len(logged_values)):
                fX = np.asarray(logged_values[logpoint]) # [(f,X)...] at this logged point
                if (len(fX) == 0): # An "empty log" e.g. when all intermods are in f_ignore
                    f_im.append(0); X_max.append(np.nan) # Makes for a blank series still appearing in the legend
                else:
                    imax = np.argmax(fX[:,1]) # max over X
                    f_im.append(fX[imax,0]); X_max.append(fX[imax,1])
            if (len(f_im) == 1):
                return f_im[0], X_max[0]
            else:
                return np.asarray(f_im), np.asarray(X_max)
        
        f1,peak1 = find_max(self.logged_values[self.eval_functions[0]]) # 0 -> order[0] i.e. the fundamental tones
        for i,evf in enumerate(self.eval_functions):
            order = self.intermod_orders[i]
            f_im, X_im = find_max(self.logged_values[evf])
            tag = "(Fundamental) " if (order == 1) else ("IM%d (%s) "%(order,self._identify_im_(set(f_im))))
            if self._as_IMD_: # For IMD we also plot fundamental "absolute" level
                k_v_t.append((self.logged_keys[evf], X_im, tag))
            elif (order != 1): # For SFDR we don't plot the fundamental since it's defined == 0dB
                k_v_t.append((self.logged_keys[evf], peak1-X_im, tag))
        return k_v_t


class TESTTONETOOL(object):
    """ A tool to inject one or more CW signals with evaluation by IM_LOGGER for IMD, or other LOGGERs for SINAD etc.
        NB: SINAD as measured here needs to be adjusted if (total signal power) < FS (i.e. HEADROOM > 0dB).
    """
    def __init__(self, freq_scale, intermod_orders=None, f_ignore=None, detector="integrate"):
        """ Note: by default a new instance "does nothing" until setup() has been completed.
            @param freq_scale: must match the frequency scale of the PSD probes, in [Hz].
            @param intermod_orders: a list of integers > 1 identifying the intermodulation products to detect & log, 1 for SINAD & SFDR.
            @param f_ignore: [(f_min,f_max)..] identifying frequency ranges to ignore, (default None).
            @param detector: the detector to apply across RBW, either "peak","mean" or "integrate"; use "integrate" for tone power monitoring (default "integrate").
        """
        self._freq_scale = freq_scale
        self._intermod_orders = intermod_orders
        self._f_ignore = f_ignore
        self._f_tones = []
        self._p_tot = 0
        self._detector = detector
        self.loggers = []
    
    def setup(self, f_tones, p_tot, **kwargs):
        """
            @param f_tones: list of frequencies, already normalized to freq_scale; at least one must be given.
            @param p_tot: total power of test tones to be injected into the signal, units as per signal_power().
                          Use p_tot=0 for passive monitoring i.e. no tones injected.
            @param kwargs: intermod_orders=..., if necessary to override the initial value
            @return: self
        """
        self._f_tones = f_tones
        self._p_tot = p_tot
        IMO = kwargs.get("intermod_orders", self._intermod_orders)
        self._intermod_orders = [] if (IMO is None or len(IMO) == 0) else sorted(set([1]+list(IMO))) # Explicitly add fundamental for IMD,IP2&IP3
        return self

    def attach_to(self, signal, f_s, PSD_PROBES, notch_depth=100, f_nyquist=None):
        """
            @param signal: the signal to prepare for intermodulation measurement.
            @param f_s: sampling rate of the signal [Hz]
            @param PSD_PROBES: PSD-type probes to attach to
            @param notch_depth: notch attenuation [dB] (default 100).
            @param f_nyquist: if not 'unsampled', the maximum frequency that can possibly carry information [Hz] (default None).
            @return: a copy of 'signal' with test tones and deep notches at expected intermod frequencies.
        """
        f_s = self._f_s = f_s/self._freq_scale
        RBW = f_s/10e3 # e.g. 1 MHz in 10 GHz, should be more than adequate
        
        # Attempt to clean up if there was a previous run
        PSD_PROBES = [PSD_PROBES] if isinstance(PSD_PROBES,Probe) else PSD_PROBES
        for logger in self.loggers:
            for P in PSD_PROBES:
                P.remove_logger(logger)
        self.loggers = []
        
        #
        # TODO: re-implement loggers to re-use expensive operations like FX_detector, e.g. use a listener pattern?
        #
        if (len(self._f_tones) > 1) and (max(self._intermod_orders) > 1): # Intermods
            logger = IM_LOGGER(self._detector, self._f_tones[0],self._f_tones[1], RBW, self._intermod_orders, self._f_ignore, freq_scale=self._freq_scale)
            # Apply notch filters to match the IM detectors
            for f in logger.im_frequencies:
                if (f not in self._f_tones): # Avoid nulling the fundamental signals, in case of passive monitoring.
                    signal = bpfilter(signal, [f-RBW,f+RBW], f_s, 0.1, [f-RBW/2.,f+RBW/2.], notch_depth, ftype="cheby1", lossless=True)
            self.loggers.append(logger)
        if (len(self._f_tones) > 0) and (max(self._intermod_orders) == 1): # SINAD & SFDR, computed in 1st Nyquist zone
            f_nyquist = f_nyquist/self._freq_scale if f_nyquist else f_s/2.
            f_tones = np.asarray(self._f_tones) if (min(self._f_tones) < f_nyquist) else np.abs(2*f_nyquist-np.asarray(self._f_tones))
            _floor = [(0+RBW/2.,f_nyquist-RBW/2.)] # Exclude DC & test tones, assume only one tone in Nyquist zone
            _tones = [(f-RBW/2.,f+RBW/2.) for f in f_tones]
            pk = lambda fX: FX_detector(fX[0],fX[1],f_bound=_tones,func=lambda X: np.max(X)+20*np.log10(len(f_tones))) # Total signal power assuming equal power
            self.loggers.append(LOGGER("SFDR [dBc]", lambda fX: pk(fX)-FX_detector(fX[0],fX[1],f_bound=_floor,f_ignore=_tones,func=np.max)))
            self.loggers.append(LOGGER("SINAD [dB]", lambda fX: pk(fX)-FX_detector(fX[0],fX[1],f_bound=_floor,f_ignore=_tones,func=sum_dB)))
            # The above assumes (total signal power) == FS i.e. HDRM = 0dB
        
        # Add the test signals
        if (self._p_tot > 0):
            t = np.arange(len(signal))/f_s
            TT = np.sum([np.sin(2*np.pi*f*t) for f in self._f_tones], axis=0)
            signal += scale_signal(TT, self._p_tot)
        
        # Attach the loggers
        for logger in self.loggers:
            for P in PSD_PROBES:
                P.add_logger(logger)
        
        return signal

    def adjust_freq(self, Df, f_nyquist=None):
        """ Adjusts all attached loggers by the specified frequency shift, in configured scale.
            This doesn't re-attach, notch out or generate new test tones.
            @param Df: delta to add to f_ignore & f_tones [Hz].
            @param f_s: new sampling frequency [Hz] (relevant for SINAD & SNR), or None if unchanged (default None)
            @param f_nyquist: if not 'unsampled', the maximum frequency that can possibly carry information [Hz] (default None).
        """
        Df /= self._freq_scale
        RBW = self._f_s/10e3
        
        f_tones = [abs(f+Df) for f in self._f_tones] # Wrap through 0
        
        # Rely on attach_to, e.g. the order in which loggers were instantiated
        if (Df != 0) and (len(f_tones) > 1) and (max(self._intermod_orders) > 1): # Intermods
            f_ignore = [(max(0,fs[0]+Df),max(0,fs[-1]+Df)) for fs in self._f_ignore]
            nl = IM_LOGGER(self._detector, f_tones[0],f_tones[1], RBW, self._intermod_orders, f_ignore, freq_scale=self._freq_scale)
            logger = self.loggers[0]
            logger.im_frequencies = nl.im_frequencies
            logger.merge_from(nl)
        if (len(f_tones) > 0) and (max(self._intermod_orders) == 1): # SINAD & SFDR
            f_nyquist = f_nyquist/self._freq_scale if f_nyquist else self._f_s/2.
            f_tones = np.asarray(f_tones) if (min(f_tones) < f_nyquist) else np.abs(2*f_nyquist-np.asarray(f_tones))
            _floor = [(0+RBW/2.,f_nyquist-RBW/2.)] # Exclude DC & test tones, assume only one tone in Nyquist zone
            _tones = [(f-RBW/2.,f+RBW/2.) for f in f_tones]
            pk = lambda fX: FX_detector(fX[0],fX[1],f_bound=_tones,func=lambda X: np.max(X)+20*np.log10(len(f_tones))) # Total signal power assuming equal power
            nl = LOGGER("SFDR [dBc]", lambda fX: pk(fX)-FX_detector(fX[0],fX[1],f_bound=_floor,f_ignore=_tones,func=np.max))
            self.loggers[-2].merge_from(nl)
            nl = LOGGER("SINAD [dB]", lambda fX: pk(fX)-FX_detector(fX[0],fX[1],f_bound=_floor,f_ignore=_tones,func=sum_dB))
            self.loggers[-1].merge_from(nl)
            # The above assumes (total signal power) == FS i.e. HDRM = 0dB
        
    def plot(self, repres="*", **kwargs):
        """ Plots results from all loggers matching repres string.
            IP2 & IP3 are transformations of the logged IMD2 & IMD3 and are therefore output-based, i.e. OIPx.
        """
        if (repres == "IP3"): # P_out1 = G+P_in; P_out3 = G+IIP3-3*(IIP3-P_in) = G+3*P_in-2*IIP3; P_out1-P_out3 = (G+P_in)-(G+3*P_in-2*IIP3) = 2*(IIP3-P_in); OIP3 = IIP3+G = (P_out1-P_out3)/2+P_in+G
            repres = "IMD"
            kwargs["ylabel"] = kwargs.get("ylabel", "OIP3 [dBm]")
            kwargs["transform"] = lambda IMD: 1.5*IMD[self._intermod_orders.index(1)]-0.5*IMD[self._intermod_orders.index(3)]
        elif (repres == "IP2"): # P_out1 = G+P_in; P_out2 = G+IIP2-2*(IIP2-P_in) = G+2*P_in-1*IIP2; P_out1-P_out2 = (G+P_in)-(G+2*P_in-1*IIP2) = IIP2-P_in; OIP2 = IIP2+G = P_out1-P_out2+P_in+G
            repres = "IMD"
            kwargs["ylabel"] = kwargs.get("ylabel", "OIP2 [dBm]")
            kwargs["transform"] = lambda IMD: 2*IMD[self._intermod_orders.index(1)]-IMD[self._intermod_orders.index(2)]
        fig = kwargs.pop("fig", None)
        for logger in self.loggers:
            if (repres=="*" or repres in logger.ylabel):
                fig = logger.plot(fig=fig, **kwargs)
        return fig

    def tabulate(self, repres="*", **kwargs):
        """ Tabulates results from all loggers matching repres string.
            IP2 & IP3 are transformations of the logged IMD2 & IMD3 and are therefore output-based, i.e. OIPx.
        """
        if (repres == "IP3"): # P_out1 = G+P_in; P_out3 = G+IIP3-3*(IIP3-P_in) = G+3*P_in-2*IIP3; P_out1-P_out3 = (G+P_in)-(G+3*P_in-2*IIP3) = 2*(IIP3-P_in); OIP3 = IIP3+G = (P_out1-P_out3)/2+P_in+G
            repres = "IMD"
            kwargs["ylabel"] = kwargs.get("ylabel", "OIP3 [dBm]")
            kwargs["transform"] = lambda IMD: 1.5*IMD[self._intermod_orders.index(1)]-0.5*IMD[self._intermod_orders.index(3)]
        elif (repres == "IP2"): # P_out1 = G+P_in; P_out2 = G+IIP2-2*(IIP2-P_in) = G+2*P_in-1*IIP2; P_out1-P_out2 = (G+P_in)-(G+2*P_in-1*IIP2) = IIP2-P_in; OIP2 = IIP2+G = P_out1-P_out2+P_in+G
            repres = "IMD"
            kwargs["ylabel"] = kwargs.get("ylabel", "OIP2 [dBm]")
            kwargs["transform"] = lambda IMD: 2*IMD[self._intermod_orders.index(1)]-IMD[self._intermod_orders.index(2)]
        for logger in self.loggers:
            if (repres=="*" or repres in logger.ylabel):
                logger.tabulate(**kwargs)


class P_dBm(Probe):
    def __init__(self, Z0=1):
        """ Samplerate is irrelevant, provided all signals are sampled & scaled with power conserved.
            @param Z0: the impedance relating amplitude and power (default 1, matched to signals module) """
        Probe.__init__(self, samplerate=None)
        self.Z0 = Z0
    
    def evaluate(self, signal, label):
        P = 10*np.log10(signal_power(signal, self.Z0))+30
        print("%s: RMS power = %.2f dBm" %(label, P))
        return P


@_test_()
def _test_P_dBm_(_test_):
    f_s = 1e3
    t = np.arange(0, 2**16)*1./f_s
    s = np.random.randn(len(t))
    for Teq in [3, 25]:
        s = scale_signal(s, kB*Teq*f_s/2.)
        _test_.assert_isclose(10*np.log10(kB*Teq*f_s/2.)+30, P_dBm()(s, "Teq"), "P_dBm is wrong for Teq=%gK"%Teq, 0.01)


class GraphProbe(Probe):
    """
        This is the base class for probes with graphical displays. Figures are generated only if
        a label is given and if the trace is recognised (or no traces defined and none requested).
        Otherwise, the probe is still evaluated but doesn't generate a figure.
    """
    def __init__(self, samplerate, traces='', title=None, fig=None, sharex=True, xlim=None, ylim=None, xscale=1, xlabel=None, grid=True):
        """@param traces: a string, each character identifies a permitted trace (default '', may be used with default call() """
        Probe.__init__(self, samplerate)
        self.__nested__ = [(traces,self)] # Permits probes to be nested - see nest()
        self.__title__ = title
        self.__fig__ = fig
        self.__axes__ = {} # trace:axis
        self.sharex = sharex
        self.grid = grid
        self.xlim = xlim
        self.ylim = ylim
        self.xscale = xscale
        self.xlabel = xlabel # Overrides the xlabel that may be set by _plot_
    
    def nest(self, gprobe):
        """ Nest the other _probe underneath this current instance so that they share a figure. IRREVERSIBLE, AND DO THIS UP FRONT! """
        assert isinstance(gprobe,GraphProbe), "Nesting anything that's not a GraphProbe could lead to hard-to-trace bugs."
        traces = "".join([t for t,p in gprobe.__nested__]) # All traces represented by gprobe
        self.__nested__.append([traces, gprobe])
        gprobe._prep_plot_ = self._prep_plot_
        return self
    
    def __call__(self, signal=None, label=None, f_s=None, nyqzone=-1, trace=None, *args, **kwargs):
        # Delegate evaluation to the correct sub-_probe
        _probe = [_probe for traces,_probe in self.__nested__ if trace in traces] if trace else None
        if _probe and not (_probe[0] == self):
            return _probe[0].__call__(signal=signal, label=label, f_s=f_s, nyqzone=nyqzone, trace=trace, *args, **kwargs)
        else: 
            return Probe.__call__(self, signal=signal, label=label, f_s=f_s, nyqzone=nyqzone, trace=trace, *args, **kwargs)
    
    def evaluate(self, signal, label, trace, **kwargs):
        """ @param signal: the signal to evaluate.
            @param label: if None, no plot will be generated although the data will be returned.
            @param trace: only if this string appears in the registered traces (or both are null) will a plot be displayed.
            @param kwargs: additional arguments passed through call().
            @return: the actual data as would appear on the plot.
        """
        plotidx = self._prep_plot_(label, trace)
        
        result = self._plot_(signal=signal,label=label,plotidx=plotidx,**kwargs)
        
        if (plotidx > 0):
            pylab.grid(self.grid)
            if (pylab.gca().get_legend_handles_labels()[0]): # Avoid errors & warnings if there happens to not be any labels
                pylab.legend(loc='best').get_frame().set_alpha(0.7)
            if self.xlim:
                pylab.xlim([x/self.xscale for x in self.xlim])
            if self.ylim:
                pylab.ylim(self.ylim)
            if self.xlabel:
                pylab.xlabel(self.xlabel)
        
        return result
    
    def _prep_plot_(self, label, trace):
        """ @param trace: only if this string appears in the registered traces (or both are null) will a plot be displayed.
            @return: plotidx, which is < 0 if either trace not recognised and/or label is None (i.e. not plot) """
        alltraces = "".join([t for t,p in self.__nested__]) # All traces in this and nested
        if (alltraces == trace) or ((not alltraces) and (not trace)): # Last clause covers ('' == None)
            tracepos = 0
        else:
            tracepos = -1 if (trace is None) else alltraces.find(trace)
        
        plotidx = -1 # Default return value means no plot must be drawn (no axis prepared)
        if (tracepos >= 0) and (label): # Prepare a plot if the trace is recognised
            plotidx = max(1,len(alltraces))*100 + 10 + (tracepos+1) # len(alltraces) rows x 1 column
            # Set up the figure
            if (self.__fig__ is None):
                self.__fig__ = pylab.figure()
                if self.__title__: # Only if it's a new figure
                    pylab.suptitle(self.__title__)
            # Set up the axis
            ref_axis = self.__axes__.get("REF", None)
            # We must do the caching since matplotlib's "subplot" & "add_subplot" functions create a NEW one if just sharex changes.
            if not (trace in self.__axes__.keys()):
                self.__axes__[trace] = self.__fig__.add_subplot(plotidx, sharex=ref_axis)
            if (self.sharex and ref_axis is None):
                self.__axes__["REF"] = self.__axes__[trace]
            pylab.sca(self.__axes__[trace])
    
        return plotidx
    
    def _plot_(self, signal, label, plotidx, **kwargs):
        """ @param label: the label for the trace on the plot
            @param plotidx: unique identifier of the plot in the top-level figure, or < 0 if no actual plot is made. 
            @return: (x,y) data points """
        raise Exception("Abstract class, method not defined!")
    
    def close(self):
        pylab.close(self.__fig__)
    
    @staticmethod
    def close_all():
        pylab.close('all')


# Before exiting make sure to show plots and pause
try:
    if (__IPYTHON__):
        pylab.show()
except:
    def _show_at_exit_():
        fignums = pylab.get_fignums()
        if (len(fignums) > 0):
            pylab.show()
    atexit.register(_show_at_exit_)



class TIMESERIES(GraphProbe):
    """ This probe produces plots of amplitude over time. """
    def __init__(self, samplerate, xscale=microsec, *args, **kwargs):
        GraphProbe.__init__(self, samplerate, xscale=xscale, *args, **kwargs)
    
    def _plot_(self, signal, label, plotidx, decimate=1, fmt='-'):
        """
            @param label: the label on the plot
            @param decimate: the stride length to use to select points to display (default 1)
        """
        V, samplerate, maxsamples = signal, self.samplerate, len(signal)
        decimate = int(max(decimate, 1 + maxsamples//1e5)) # This limits figures to no more than 1e6 points
        # Get max per decimation window
        N = maxsamples//decimate
        ts = np.arange(0,N)*decimate*(1/samplerate/self.xscale)
        V = np.reshape(V[:decimate*N],(N,decimate))
        V_max = V.max(axis=1)
        V_min = V.min(axis=1)
        # Plot only if plotidx >= 0
        if (plotidx >= 0):
            if (decimate > 1):
                pylab.plot(ts,V_max,'k', ts, V_min,'k', alpha=0.3) # MAX & MIN in window
            pylab.plot(ts, V[:,0], fmt, label=label, alpha=0.8, lw=1); pylab.axis('Tight')
            pylab.xlabel('Time [%ss]'%({1:"",1e-3:"m",microsec:"$\mu$"}.get(self.xscale,"/%g"%self.xscale)))
            pylab.ylabel('Sample value [linear]')
            
        return (ts, V_max)


class PSD(GraphProbe):
    """ This probe produces Auto-correlation Power Spectral Density plots. """
    def __init__(self, samplerate, correct_PG=True, Navg=1, xscale=MHz, nyqzone=-1, fft_func=np_fft.fft,  *args, **kwargs):
        """
            By default the PSD normalizes the spectrum with the incoherent window gain. The default window is the min 4-term Blackman Harris window
            [Harris, Fredric J. "On the Use of Windows for Harmonic Analysis with the Discrete Fourier Transform." Proceedings of the IEEE 66, no. 1 (1978)].
            Keep in mind that FFT gain for coherent signals is different to gain for incoherent signals.
            
            NW: For spectral analysis the window function should yield a "periodic" extensible window to minimize sidelobes (as opposed to "symmetric",
            which is appropriate for filters and time series response). The window may be overridden with a function that is defined like
            "lambda N [,sym]: w(N [,sym])" (like those in scipy.windows). This class always attempts "w(N,sym=True)" and falls back to "w(N)".
            
            @param correct_PG: True to scale gain of the FFT so that the noise floor is correctly scaled, False to have CW scale correct (default True).
            @param Navg: number of samples to average per spectral point for FFT (default 1 i..e no averaging).
                            If > 1 then FFTs will be computed with $len(signal)//Navg$ points, then stacked & averaged.
                            CAUTION, for noisy signals that decreases the level by sqrt(Navg)!
            @param fft_func: alternative FFT function to use, must be compatible with the default (default numpy.fft.fft)
        """
        GraphProbe.__init__(self, samplerate, xscale=xscale, *args, **kwargs)
        self.correct_PG = correct_PG
        self.window = sig.windows.blackmanharris
        self.Navg = Navg
        self.nyqzone = nyqzone
        self.fft_func = fft_func
    
    def _window_(self, fftlen):
        """ Creates the (periodic) window and takes care of processing gain correction as per correct_PG.
            Note that   numpy.fft.fft(signal*_window_)   will be correctly scaled to [/point]. Further scale
            transformations will be required e.g. to convert to amplitude density [/Hz] multiply by sqrt(1/f_s).
            @return: the window values
        """
        try: # Try to use periodic window (DFT-even) as recommended in [Harris 1976/78]
            win = self.window(fftlen, sym=False)
        except: # Doesn't use the "sym" keyword, so cannot enforce compliance
            win = self.window(fftlen)
        
        if (self.correct_PG):
            pg = np.sum(np.abs(win)**2) # Harris' Incoherent (power) Processing Gain (eq 14b), same applied in matplotlib.mlab._spectral_helper() 
        else:
            pg = np.sum(win)**2 # The Coherent (power) Processing Gain of [Harris 1976/78] eq 13 
            
        return win / pg**.5
    
    def _plot_(self, signal, label, plotidx, **kwargs):
        """
            @return (frequency, scaled PSD) for f >= 0
        """
        # Factored in the following way to permit sub-classing
        freq, FV = self._sd_(signal, self.nyqzone, **kwargs)
        # Scale to display
        FV, ylabel = self._scale_sd_(FV, **kwargs)
        freq /= self.xscale
        
        # Plot only if plotidx >= 0
        if (plotidx >= 0):
            pylab.plot(freq, FV, label=label, alpha=0.8); pylab.axis('Tight')
            pylab.xlabel('Frequency [%s]'%({MHz:"MHz",MHz/1e6:"Hz"}[self.xscale]))
            pylab.ylabel(ylabel)
            
        return (freq, FV)
    
    def _sd_(self, signal, nyqzone=-1, longfft=1):
        """
            Computes the single-sided PSD as subset of 2x the two-sided spectrum:
                Pxx = |fft(x * (window/sum(|window|))|^2 / f_s  [W/Hz]
            If configured the window is automatically scaled to corrected for processing gain in which case the noise floor power will be correct. 
            @param nyqzone: Nyquist zone to translate frequencies to sampled domain (default -1 i.e. none).
            @param longfft: 0 to compute gain on Navg contiguous segments or 1 to compute gain on the entire series and then decimate by Navg (default 1)
            @return (frequency [Hz], autcorrelation spectrum [W/Hz]) for f >= 0
        """
        fftlen = (len(signal)//self.Navg //2)*2 # Ensure it's at least an even number, or else FFT is extremely slow
        if (longfft==1): fftlen = fftlen*self.Navg
        win = self._window_(fftlen) # Normalized for processing gain if necessary
        
        # Stack & average for specified number of points, and scale for window & sample rate
        if (longfft==0): # Break signal into consecutive time segments, take ratios of FFTs then average across segments
            V = np.reshape(signal[:self.Navg*fftlen],(-1,fftlen)) * win # axis=1 contains contiguous samples, axis=0 has sub-samples
            FV = self.fft_func(V, axis=1, n=fftlen) * (1/self.samplerate)**.5 # Normalize numpy FFT to "/sqrt(Hz)"
            FV = FV[:,:fftlen//2]
        elif (longfft==1): # Take the whole time extent, take ratios of FFTs then decimate the spectrum
            V = signal[:fftlen] * win
            FV = self.fft_func(V, axis=0, n=fftlen) * (1/self.samplerate)**.5 # Normalize numpy FFT to "/sqrt(Hz)"
            fftlen = fftlen//self.Navg
            FV = np.reshape(FV[:self.Navg*fftlen//2], (fftlen//2,-1)) # axis=1 contains contiguous samples, axis=0 has sub-samples
        
        FV = 2*np.mean(np.abs(FV)**2,axis=longfft) # Correct to take mean(|x|^2) since x is complex when we only sum the f>0 part 
        FV[0] /= 2. # Half the DC bin
        
        # Translate frequencies to display in specified Nyquist zone
        freq = np.fft.fftfreq(fftlen, 1.0/self.samplerate)[:fftlen//2]
        if (nyqzone == 1):
            pass
        elif (nyqzone == 2):
            freq = self.samplerate - freq
        
        return (freq, FV)
    
    def _scale_sd_(self, psd, longfft=1):
        return (10*np.log10(psd)+30, "Spectral density [dBm/Hz]")


@_test_()
def _test_PSD_(_test_):
    # Confirm Incoherent Processing Gain as employed in PSD
    f_s = 1e6
    _test_.assert_is(True == PSD(f_s).correct_PG, "Default setting for correct_PG must be True")
    
    N = 2**16
    t = np.arange(0, N, 1)*1./f_s
    s_in = scale_signal(np.random.randn(len(t)), kB*100*f_s/2.) # 100K
    w = sig.windows
    for window in [w.boxcar, w.triang, w.hamming, w.blackman, w.blackmanharris]:
        _PSD = PSD(f_s, Navg=1)
        _test_.assert_isclose(kB*100, _PSD(window=window)._sd_(s_in)[1].mean(), "PSD [W/Hz] of noise wrong by >1%%", rtol=0.01)
    
    # Resampling should not change power spectral density
    # Same time extent, just under-sampled by some factor
    for M in [2,3]:
        t_sampled, s_out = sample(s_in, f_s, f_s/float(M), nyqzone=1) # nyqzone=1 performs LP filtering before downsample, which decreases total power but not spectral density
        _PSD = PSD(1/np.diff(t_sampled).mean(), Navg=1)
        _test_.assert_isclose(kB*100, _PSD._sd_(s_out)[1].mean(), "PSD [W/Hz] of RESAMPLED noise wrong by >4%%", rtol=0.04)
        # Check PSD results agree with matplotlib's PSD
        import matplotlib
        _Pxy, _freqs, _t = matplotlib.mlab._spectral_helper(s_in, s_in, len(s_in), 1/np.diff(t).mean(), scale_by_freq=True, sides='onesided')
        _test_.assert_isclose(kB*100, _Pxy.mean(), "matplotlib.PSD [W/Hz] of ORIGINAL differs by >1%%", rtol=0.01)
        _Pxy, _freqs, _t = matplotlib.mlab._spectral_helper(s_out, s_out, len(s_out), 1/np.diff(t_sampled).mean(), scale_by_freq=True, sides='onesided')
        _test_.assert_isclose(kB*100, _Pxy.mean(), "matplotlib.PSD [W/Hz] of RESAMPLED differs by >3%%", rtol=0.03)
    
    # Test absolute value of spectral density over different rates & N points
    for f_s in [1e7,1e9]:
        for N in [13,14,18]:
            t = np.arange(0, 2**N)*1./f_s
            np.random.seed(1) # This way we always have the same test signals so that we can isolate changes in behaviour under test
            s_in = scale_signal(np.random.randn(len(t)), kB*100*f_s/2.) # 100K
            _PSD = PSD(f_s, Navg=1)
            _test_.assert_isclose(kB*100, _PSD._sd_(s_in)[1].mean(), "PSD [W/Hz] differs by >2%% for f_s=%g, N=%d"%(f_s,N), rtol=0.02)

    # Test absolute value of spectral density remains constant over different averaging (the std dev should decrease)
    t = np.arange(0, 2**N)*1./f_s
    s_in = scale_signal(np.random.randn(len(t)), kB*100*f_s/2.) # 100K
    for Navg in [1,2,8,64]:
        _PSD = PSD(f_s, Navg=Navg)
        _test_.assert_isclose(kB*100, _PSD._sd_(s_in)[1].mean(), "PSD [W/Hz] differs by >1%% for Navg=%d"%(Navg), rtol=0.01)

    # Confirm total power conservation relative to original time domain
    for f_s in [1e7,1e9]:
        # Since we don't control scalloping loss this test is tight for coherent tones -- as bad old boxcar needs 15% so we leave that one out
        for window in [w.triang, w.hann, w.hamming, w.bohman, w.blackman, w.blackmanharris]:
            # Test with noise signal
            _PSD = PSD(f_s, Navg=1)
            _PSD.window = window
            for N in [12,16,18]:
                t = np.arange(0, 2**N)*1./f_s
                np.random.seed(1) # This way we always have the same test signals so that we can isolate changes in behaviour under test
                s = np.random.randn(len(t))
                for Teq in [10, 1e6]:
                    p = kB*Teq*f_s/2.
                    s = scale_signal(s, p)
                    f, psd = _PSD._sd_(s)
                    _test_.assert_isclose(p, np.sum(psd)*np.diff(f).mean(), "%s 2^%d: int(psd,noise) is wrong by >2%% for Teq=%g K"%(window.__name__,N,Teq), rtol=0.02)
            
            # Test with coherent tones
            _PSD = PSD(f_s, Navg=1)
            _PSD.window = window
            for N in [12,16,18]:
                t = np.arange(0, 2**N)*1./f_s
                s = np.sum([np.sin(2*np.pi*a*np.linspace(0,0.5,len(t))) for a in [0.1,0.2,0.3,0.4]], axis=0)
                s = scale_signal(s, p) # p from last Teq above
                f, psd = _PSD._sd_(s)
                
                #TODO: figure out why - empirically determined to yield agreement to < 2% for all but boxcar & flattop
                # This is equivalent to having the "coherent processing gain" as pg = np.sum(np.abs(win)**2) * 3.862
#                 win = _PSD.window(len(s), sym=False)
#                 D = np.sum(win)**2 /(np.sum(np.abs(win)**2) * 3.862)
                D = 1/3.862
                psd *= D
    
                _test_.assert_isclose(p, np.sum(psd)*np.diff(f).mean(), "%s 2^%d: int(psd,tones) differs by >3%%"%(window.__name__,N), rtol=0.03)


class CSD(PSD):
    """ This probe produces Cross-correlation Power Spectral Density plots. """
    def _resample_(self, signal, f_s, nyqzone=0):
        """ Re-sample signal from f_s to match self.samplerate. If nyqzone is also specified (i.e. >0) then
            the re-sampled signal is also band-limited to isolate just the specified nyquist zone. """
        signal_i, signal_j = signal
        return (PSD._resample_(self, signal_i, f_s, nyqzone), PSD._resample_(self, signal_j, f_s, nyqzone))
    
    def _sd_(self, signal, nyqzone=-1, quantity='mag'):
        """
            Computes the single-sided Cross-PSD as subset of 2x the two-sided spectrum:
                Pij = fft(i * (window/sum(|window|)) * conjugate(|fft(j * (window/sum(|window|))) / f_s  [W/Hz]
            If configured the window is automatically scaled to corrected for processing gain in which case the noise floor power will be correct. 
            @param nyqzone: Nyquist zone to translate frequencies to sampled domain (default -1 i.e. none).
            @param signal: (signal_i, signal_j)
            @param quantity: either "mag" or "phase"
            @return (frequency [Hz], I * J spectrum [W/Hz]) for f >= 0
        """
        assert (quantity in ["mag","phase"]), "'quantity' must be one of {'mag','phase'}"
        signal_i, signal_j = signal
        
        fftlen = (min([len(signal_i),len(signal_j)])//self.Navg //2)*2 # Ensure it's at least an even number, or else FFT is extremely slow
        win = self._window_(fftlen) # Normalized for processing gain if necessary
        
        # Stack & average for specified number of points, and scale for window
        V_i = np.reshape(signal_i[:self.Navg*fftlen],(-1,fftlen)) * win
        FV_i = self.fft_func(V_i, axis=1, n=fftlen) * (1/self.samplerate)**.5 # Normalize numpy FFT to "/sqrt(Hz)"
        V_j = np.reshape(signal_j[:self.Navg*fftlen],(-1,fftlen)) * win
        FV_j = self.fft_func(V_j, axis=1, n=fftlen) * (1/self.samplerate)**.5 # Normalize numpy FFT to "/sqrt(Hz)"
        
        X_ij = FV_i*np.conjugate(FV_j)
        
        X_ij = X_ij.mean(axis=0) # Apply coherent averaging to collapse time dimension
        if (quantity == 'mag'):
            X_ij = 2*np.abs(X_ij[:fftlen//2]) #Take abs(x) since x is complex when we only sum the f>0 part 
            X_ij[0] /= 2. # Half the DC bin
        elif (quantity == 'phase'):
            X_ij = np.angle(X_ij[:fftlen//2])
        
        # Translate frequencies to display in specified Nyquist zone
        freq = np.fft.fftfreq(fftlen, 1.0/self.samplerate)[:fftlen//2]
        if (nyqzone == 1):
            pass
        elif (nyqzone == 2):
            freq = self.samplerate - freq
        
        return (freq, X_ij)

    def _scale_sd_(self, psd, quantity='mag'):
        if (quantity=='phase'):
            return (np.unwrap(psd), "Unwrapped Phase [rad]")
        else:
            return PSD._scale_sd_(self, psd)


@_test_()
def _test_CSD_(_test_):
    # Confirm CSD reduces to PSD
    f_s = 1e6
    N = 2**16
    t = np.arange(0, N, 1)*1./f_s
    s_in = scale_signal(np.random.randn(len(t)), kB*100*f_s/2.) # 100K
    for Navg in [1, 4, 16]:
        _PSD = PSD(f_s, Navg=Navg)._sd_(s_in,longfft=0)[1]
        _CSD = CSD(f_s, Navg=Navg)._sd_((s_in,s_in))[1]
        _test_.assert_isclose(np.mean(_PSD), np.mean(_CSD), "mean PSD of noise differs from mean CSD by >0.01%%", rtol=0.0001)
        _test_.assert_isclose(np.sum(_PSD), np.sum(_CSD), "sum PSD of noise differs from sum CSD by >0.01%%", rtol=0.0001)
        _test_.assert_isclose(_PSD, _CSD, "PSD of noise differs from CSD by >0.01%%", rtol=0.0001)


class ZEROSPAN(GraphProbe):
    """ This probe produces plots of Total Power over time. """
    def __init__(self, samplerate, psd_func, f_c=None, BW=None, yscale="linear", xscale=microsec, *args, **kwargs):
        """ This _probe forms a time series of PSDs, only reflecting the power summed over f_c+/-BW/2 frequency range.
            The PSD spectral resolution is set to match the bandwidth BW, so averaging (PSD.Navg) must be off.
            @param psd_func: the detector function to compute the input for the zero span detector, as lamda time_series: (f, X)
            @param yscale: either "dB" or "linear" (default "linear")
            @param f_c: centre frequency [Hz], defining the band-selection employed by the detector.
            @param BW: bandwidth [Hz], defining the band-selection employed by the detector.
            @param xscale: (default 1 microsec)
        """
        GraphProbe.__init__(self, samplerate, xscale=xscale, *args, **kwargs)
        self.psd_func = psd_func
        self.yscale = yscale
        self.f_c = f_c
        self.BW = BW
    
    def _plot_(self, signal, label, plotidx, fmt='-', M=1):
        """
            @param M: controls (non-overlapping) segmentation of samples sent to PSD function
                      so that detected output is decimated to match averaging or PFB taps (default 1).
            @return (channel power over time) with time 
        """
        # TODO: re-factor the below to use fft_filterbank
        # Transform requested resolution to closest realizable settings
        N = int(self.samplerate/self.BW+0.5) # Number of samples to push through the detector to provide closest match to requested resolution
        # Actual time and frequency resolution as implemented
        DF = self.samplerate/N
        F_c = int(self.f_c/DF+0.5) # The FFT bin corresponding to the detector channel
        DT = 1/(DF/2.) # Equivalent time resolution
        
        # Break the signal into desired time resolution chunks
        ts = np.arange(0,(len(signal)//(N*M))*M, step=M) # Integer time ticks for detected values
        S = np.reshape(signal[:N*M*len(ts)],(-1,M*N)) # Non-overlapping segments!
        
        # Implement detector in what is hopefully an optimised manner
        P = [0]*(len(ts))
        for t in ts: # For each time segment, perform FX_detector - style detection.
            f, X = self.psd_func(S[t//M,:]) # No overlap!
            P[t//M] = np.sum(X[F_c]*(f[1]-f[0])) # Integrated power
        ts = ts*DT # Convert from ticks to seconds
        
        # Apply requested scale transformations
        if (self.yscale == "dB"):
            P = 10*np.log10(P)
        ts /= self.xscale
        
        # Plot only if plotidx >= 0
        if (plotidx >= 0):
            pylab.plot(ts, P, fmt, label=label, alpha=0.8, lw=1); pylab.axis('Tight')
            pylab.xlabel('Time [%ss]'%({1:"",1e-3:"m",microsec:"$\mu$"}.get(self.xscale,"/%g"%self.xscale)))
            pylab.ylabel('Sampled power [%s]'%self.yscale)
            
        return (ts, P)

@_test_()
def _test_ZEROSPAN_(_test_): # TODO: change from current visual inspection
    from rfsim.signals import CHIRP_series
    
    f_s = 1e9
    t_sample = np.arange(2**18)*1/f_s
    DF = f_s/len(t_sample) # Highest frequency resolution achievable
    BW = 800*DF # Channels are traced with this many points across
    Nchans = 10
    s = CHIRP_series(t_sample, 0, Nchans*BW, t_sample[-1], 30) # scan 10 channels @ +30 dBm = 0dBW
    xscale = 2*t_sample[-1]/(Nchans*BW/1e3) # sec/kHz -- see CHIRP_series() note on rate of change of frequency
#     _probe("CHIRP", s, TIMESERIES(f_s))
    
    _Z = ZEROSPAN(f_s, psd_func=PSD(f_s, Navg=1)._sd_, yscale="dB", xscale=xscale, xlabel="f [kHz]", title="ZEROSPAN Test")
    for ch in range(1,Nchans//2):
        _probe("f_c %gkHz"%(ch*BW/1e3), s, _Z(f_c=ch*BW,BW=BW))


class GAIN(PSD):
    def __init__(self, samplerate, ref_signal=None, *args, **kwargs):
        """ Note: Since gain is computed as a ratio of PSD's, it doesn't care about processing gain as long as it's applied consistently.
            @param ref_signal: the signal to measure gain relative to, must represent same time series as signal.
        """
        PSD.__init__(self, samplerate, *args, **kwargs)
        self.ref_signal = ref_signal
    
    def _sd_(self, signal, nyqzone=-1):
        """ @return (frequencies, "power gain" spectrum [linear/Hz]) for f >= 0 """
        f, a_g = a_gain(self.ref_signal, signal, self.samplerate, nyqzone, self._window_, self.Navg)
        return (f, np.abs(a_g)**2)
    
    def _scale_sd_(self, gain_sd):
        return (10*np.log10(gain_sd), 'Gain [dB]')

def a_gain(signal_in, signal_out, samplerate, nyqzone, window=None, Navg=1, longfft=1):
    """ Power gain must be computed from these amplitude ratios (g = |a_gain|^2) rather than the
        ratio of power levels or otherwise additive noise signals will appear to increase the gain!
        
        This function therefore computes "amplitude gain" as follows:
        s_in <-> S_in
        s_out = g***(s_in + additive_noise + distortion(s_in)) <-> G*(S_in + N + D) = S_out
        S_out/S_in = G + G*N/S_in + G*D/S_in
        With averaging, the unwanted "N" term will approach 0 while the "D" term will either not or much slower.
        "D" should be small by design or else it will influence the results.
        From the equations above it is apparent that the result only converges for SNR >= 1/sqrt(Navg).
        
        @param window: the window function, as "lambda N: [w(N)]" (default None i.e. boxcar)
        @param longfft: 0 to compute gain on Navg contiguous segments or 1 to compute gain on the entire series and then decimate by Navg (default 1)
        @return (frequencies, "amplitude gain" spectrum [linear]) for f >= 0
    """
    # TODO: re-factor below to use fft_filterbank
    signal_len = (len(signal_in)+len(signal_out))//2 # Small discrepancies in length of signals (e.g. as may arise when comparing re-sampled signals) should not affect results
    fftlen = (signal_len//Navg //2)*2 # Ensure it's at least an even number, or else FFT is extremely slow
    if (longfft==1): fftlen = fftlen*Navg
    win = window(fftlen) if window else 1.
    
    # It's not necessary to normalize these FFT's since it's the same factors for both.
    # Stack & average for specified number of points.
    if (longfft==0): # Break signal into consecutive time segments, take ratios of FFTs then average across segments
        s_i = np.reshape(signal_in[:Navg*fftlen],(-1,fftlen)) * win # axis=1 contains contiguous samples, axis=0 has sub-samples
        S_i = np_fft.fft(s_i, axis=1, n=fftlen)
        s_o = np.reshape(signal_out[:Navg*fftlen],(-1,fftlen)) * win
        S_o = np_fft.fft(s_o, axis=1, n=fftlen)
        G_ = (S_o/S_i)[:,:fftlen//2] # All scale factors including 2* for one-sidedness cancel in this ratio.
        G_ = medfilt2d(G_, [1,5]) # Smooth over frequency to be comparable to X=1 below
    elif (longfft==1): # Take the whole time extent, take ratios of FFTs then smooth & decimate the spectrum
        S_i = np_fft.fft(signal_in[:fftlen]*win, n=fftlen)
        S_o = np_fft.fft(signal_out[:fftlen]*win, n=fftlen)
        G_ = (S_o/S_i)[:fftlen//2] # All scale factors including 2* for one-sidedness cancel in this ratio.
        fftlen = fftlen//Navg
        G_ = np.reshape(G_,(fftlen//2,-1)) # axis=1 contains contiguous samples, axis=0 has sub-samples
        G_ = medfilt2d(G_, [5,1]) # Smooth over frequency (at the final resolution) to filter out sub-sampling effects (> 5 introduces systematic errors)
    # Must take mean(x) before |x|^2 (power gain) otherwise zero-mean terms don't average down!
    G = G_.mean(axis=longfft) # Average over sub-samples
    
    # Translate frequencies to display in specified Nyquist zone
    freq = np.fft.fftfreq(fftlen, 1./samplerate)[:fftlen//2]
    if (nyqzone == 1):
        pass
    elif (nyqzone == 2):
        freq = samplerate - freq
    
    return (freq, G)
    

@_test_()
def _test_GAIN_(_test_):
    f_s = 1e9
    t = np.arange(0, 2**22)*1./f_s
    s_in = scale_signal(np.random.randn(len(t)), 1)
    s_noise = scale_signal(np.random.randn(len(t)), 1) # snr=1. Use the same input-referred noise to separate effect of random test vectors, from scaling rules
    
    # Test averaging including trivial case
    snr = 10 # Results seem to converge readily for >= 10 
    for g in [0.1, 3, 713]:
        for Navg in [2,4,16,64]: # Fails with Navg=1 (out by >20%), but OKAY since the method relies on averaging
            f,a = a_gain(s_in, g*(s_in + s_noise/snr**.5), f_s, 1, Navg=Navg)
            _test_.assert_isclose(len(f), len(a), "len(f) != len(g) for Navg=%d"%Navg)
            _test_.assert_isclose(g, a.mean(), "gain is >1%% wrong with Navg=%d"%Navg, rtol=0.01)
            _test_.assert_isclose(g, a[10:-10].mean(), "gain(excl. edges) is >1%% wrong with Navg=%d"%Navg, rtol=0.01)
            _test_.assert_isclose(g, np.abs(a).mean(), "|gain| is >3%% wrong with Navg=%d"%Navg, rtol=0.03)
            _test_.assert_isclose(g, np.abs(a[10:-10]).mean(), "|gain(excl. edges)| is >3%% wrong with Navg=%d"%Navg, rtol=0.03)
    
    # Test for SNR<=1, where convergence is slow. For snr < 0.01 need Navg>2**10 and hence Npts>2**20
    g = 10 # Doesn't make a difference for SNR, but multiples of 10 give results that are easy to review
    for snr in [18,10,0.1,0.01,0.001]:
        Navg = 128 if snr > 0.01 else 2**11
        f,a = a_gain(s_in, g*(s_in + s_noise/snr**.5), f_s, 1, Navg=Navg)
        _test_.assert_isclose(g, a.mean(), "gain is >4%% wrong for snr=%g"%(snr), rtol=0.04)
        _test_.assert_isclose(g, np.abs(a).mean(), "|gain| is >4%% wrong for snr=%g"%(snr), rtol=0.04)
    
    # Compare longfft=0 && longfft=1
    # longfft=1 seems to reflect less the "total power" and therefore more robust against noise and distortion, preferred over =0.
    for snr in [0.001,0.0001]:
        f,a0 = a_gain(s_in, g*(s_in + s_noise/snr**.5), f_s, 1, Navg=2**11, longfft=1)
        f,a1 = a_gain(s_in, g*(s_in + s_noise/snr**.5), f_s, 1, Navg=2**11, longfft=0)
        _test_.assert_isclose(a0.mean(), a1.mean(), "longfft=0 gain differs more than >3%% from longfft=1 for snr=%g"%(snr), rtol=0.03)
        _test_.assert_isclose(np.abs(a0).mean(), np.abs(a1).mean(), "longfft=0 |gain| differs more than >3%% from longfft=1 for snr=%g"%(snr), rtol=0.03)
    

class NOISEFIGURE(PSD):
    """ This probe produces Noise Figure spectral plots. """
    def __init__(self, samplerate, ref_signal=None, unit="dB", *args, **kwargs):
        """ Note: Since Noise Figure reflects the noise density scale, processing gain is always corrected for.
                  Consequently, results where coherent signals dominate are unreliable.
            See PSD for default window. Only the Boxcar window is unusable for noise analysis (experimental evidence, why is TBD). 
            @param ref_signal: the signal to measure noise (and gain) relative to, must represent same time series as signal.
            @param unit: must be either dB or K
        """
        PSD.__init__(self, samplerate, *args, **kwargs)
        self.ref_signal = ref_signal
        self.unit = unit
    
    def _window_(self, fftlen): # Overridden to always force processing gain correction for noise.
        self.correct_PG = True
        return PSD._window_(self, fftlen)
    
    def _sd_(self, signal, nyqzone=-1):
        """ Noise factor spectrum is calculated as follows
                S_out = G*(S_in + S_eq)
                S_eq = S_out/G - S_in
                S_eq = kB*T_eq
                T_eq = T0*(F - 1)
                F = T_eq/T0 + 1
                S_eq = (psd_out/gain_sd) - psd_in
                F = S_eq/(kB*T0) + 1
            Use .Navg for vector averaging to reduce scatter in the noise power density - at the expense of spectral resolution.
            @return (frequencies, noise factor spectrum) for f >= 0
        """
        psd_T0 = kB * 290 # 290 K at reference plane, equivalent single sided density
        
        freq, gain_sd = a_gain(self.ref_signal, signal, self.samplerate, nyqzone, self._window_, self.Navg)
        gain_sd = np.abs(gain_sd)**2 # Convert to power gain
        
        # Discrepancies in length of signals (e.g. as may arise when comparing re-sampled signals) that don't affect gain measurement should not affect results
        _len = len(gain_sd)*2 * self.Navg
        psd_in = PSD._sd_(self, self.ref_signal[:_len], nyqzone)[1]
        psd_out = PSD._sd_(self, signal[:_len], nyqzone)[1]
        
        psd_out_in = psd_out/gain_sd
        F = (psd_out_in - psd_in)/ psd_T0 + 1 # All at same reference plane, should be numerically OK.
        F = sig.medfilt(F, 3) # Just enough to counter error propagation. More than 5 seems to introduce systematic error
        
        return (freq, F)
    
    def _scale_sd_(self, F_sd):
        if self.unit == "dB":
            return (10*np.log10(F_sd), 'Noise Figure [dB]')
        elif self.unit == "K":
            return (290*(F_sd-1), 'Noise Temperature[K]')


@_test_()
def _test_NOISEFIGURE_(_test_):
    # Test absolute value of spectral density over different averaging
    f_s = 1e3
    N = 21 # NF converges quite slowly
    t = np.arange(0, 2**N)*1./f_s
    s_in = scale_signal(np.random.randn(len(t)), kB*100*f_s/2.) # 100K
    s_noise = scale_signal(np.random.randn(len(t)), kB*10*f_s/2.) # 10K
    for Navg in [16,64,128,1024]: # < 4 gives VERY noisy results, which is probably OKAY. >= 14 passes at 10%
        _PSD = PSD(f_s, Navg=Navg)
        _test_.assert_isclose(kB*100, _PSD._sd_(s_in)[1].mean(), "PSD [W/Hz] differs by >1%% for Navg=%d"%(Navg), rtol=0.01)
        _test_.assert_isclose(kB*10, _PSD._sd_(s_noise)[1].mean(), "PSD [W/Hz] differs by >1%% for Navg=%d"%(Navg), rtol=0.01)
        _NF_K = NOISEFIGURE(f_s, Navg=Navg, ref_signal=s_in, unit="K")
        _test_.assert_isclose(10, np.mean(_NF_K(3*(s_in+s_noise))[1]), "NF [K] for 3*(s+n) differs by >8%% for Navg=%d"%(Navg), rtol=0.08) # NF is a ratio, significantly more noisy than just PSD
        _test_.assert_isclose(10/2**2, np.mean(_NF_K(2*s_in+s_noise)[1]), "NF [K] for 2*s+n differs by >8%% for Navg=%d"%(Navg), rtol=0.08)
    
    # Test different rates & N points
    for N in [16,18,20]: # Frequent failures here due to slow convergence
        n_pts = 2**N
        s_i = np.random.randn(n_pts) # Use the same signals at different rates
        s_n = np.random.randn(n_pts)
        for f_s in [1e6,1e7,1e9]:
            s_in = scale_signal(s_i, kB*100*f_s/2.) # 100K
            s_noise = scale_signal(s_n, kB*10*f_s/2.) # 10K
            _NF_K = NOISEFIGURE(f_s, Navg=128, ref_signal=s_in, unit="K")
            _test_.assert_isclose(10, np.mean(_NF_K(1*(s_in+s_noise))[1]), "NF [K] differs by >10%% for f_s=%g, Npts=2^%d"%(f_s,N), rtol=0.1)
    
    # Test other factors
    for f_s in [11e9,22e9]:
        s_in = scale_signal(np.random.randn(len(t)), kB*100*f_s/2.) # 100K
        s_noise = scale_signal(np.random.randn(len(t)), kB*10*f_s/2.) # 10K Use the same input-referred noise to separate effect of random test vectors, from scaling rules
        for win in [sig.windows.blackmanharris, sig.windows.boxcar]:
            _NF = NOISEFIGURE(f_s, Navg=128, ref_signal=s_in, unit="dB")
            _NF.window = win
            _NF_K = NOISEFIGURE(f_s, Navg=128, ref_signal=s_in, unit="K")
            _NF_K.window = win
            # Test for gain
            for g in [1, 2, 4, 40, 400, 4000]:
                s = g*(s_in + s_noise) # Input referred noise
                _test_.assert_isclose(10*np.log10(10/290. + 1), _NF(s)[1].mean(), "f_s=%g, %s NF [dB] differs by >0.1dB for g=%g"%(f_s,win,g), 0.1)
                _test_.assert_isclose(10, np.mean(_NF_K(s)[1]), "f_s=%g, %s NF [K] differs by >10%% for amplitude g=%g"%(f_s,win,g), rtol=0.1)
            
            # Test for additive noise (with g=1)
            s_in = scale_signal(s_in, kB*100*f_s/2.) # 100K
            for Teq in [10, 20, 100, 200, 400, 1000]:
                s = 1*(s_in + scale_signal(s_noise, kB*Teq*f_s/2.))
                _test_.assert_isclose(10*np.log10(Teq/290. + 1), _NF(s)[1].mean(), "f_s=%g, %s NF [dB] differs by >10%% for Teq=%gK"%(f_s,win,Teq), rtol=0.1)
                _test_.assert_isclose(Teq, np.mean(_NF_K(s)[1]), "f_s=%g, %s NF [K] differs by >10%% for Teq=%gK"%(f_s,win,Teq), rtol=0.1)


class HISTOGRAM(GraphProbe):
    """ This probe produces histogram plots. """
    def __init__(self, samplerate, Nfullscale=None, logscale=False, stem=True, *args, **kwargs):
        GraphProbe.__init__(self, samplerate, *args, **kwargs)
        self.Nfullscale = Nfullscale
        self.logscale = logscale
        self.stem = stem
        
    def _plot_(self, signal, label, plotidx, discardLSB=False):
        std = np.std(signal)
        V = signal+self.Nfullscale/2. if (np.min(signal)<0) else signal # Lift values to >= 0 so bincount works correctly
        V = np.asarray(V, np.int) 
        if discardLSB: # Work-around for DNL? Reduce resolution, effectively average adjacent bins
            V = (V//2) *2
        n = np.bincount(V, minlength=int(self.Nfullscale+0.5))
        if discardLSB:
            n = n / 2.
        p = n / float(len(V))
        
        # Plot only if plotidx >= 0
        if (plotidx >= 0):
            # The way discardLSB is implemented every second bin is == 0, therefore semilogy ends up with a set of points separated by -inf.
            # That is why the plot style MUST be "point styles" only -- can't use any of the '-' line styles.
            plt_type = pylab.semilogy if self.logscale else (pylab.stem if self.stem else pylab.plot)
            plt_fmt = ('C0-','C0o',' ') if self.stem else ('o',) # For pylab.plot can't pass 'fmt' as a keyword argument
            plt_type(p, *plt_fmt, label=label+" ($\sigma=%.1f$)"%std, alpha=0.8); pylab.axis('Tight')
            pylab.xlabel('Bin [#]')
            pylab.ylabel('Probability [fraction]')
            
        return (range(len(p)), p)


class SPECTROGRAM(GraphProbe):
    """ This probe produces time (y-axis) vs. frequency (x-axis) intensity maps of power spectral density.
        Intensity units are dBm/Hz. """
    def __init__(self, samplerate, f_scale=MHz, f_resolution=None, f_range=None, t_scale=microsec, P_range=None, filterbank_func=None, nyqzone=-1, cmap='hot', *args, **kwargs):
        """ 
            @param samplerate: [Hz]
            @param f_scale: (default MHz).
            @param f_resolution: desired spectral resolution or None to set resolution to f_scale/10 (default None) [f_scale]
            @param f_range: (f_lower, f_upper) to set the initial display zoom (default None) [f_scale]
            @param t_scale: (default microsec).
            @param P_range: colour map saturation limits in [dBm/Hz] or None to use 1st & 99th percentiles (default None)
            @param filterbank_func: the detector function to compute the time series spectra, as lamda time_series: (t, f, X) (default is fft_filterbank)
            @param nyqzone: >1 to translate the filterbank output to reflect sampling in this Nyquist zone (default <0).
            @param cmap: the colour map to use - as defined by matplotlib (default 'hot').
        """
        GraphProbe.__init__(self, samplerate, xscale=f_scale, grid=False,
                            title=kwargs.get("title","Power spectral density [dBm/Hz]"), *args, **kwargs)
        self.samplerate = samplerate
        self.f_range = f_range
        f_resolution = f_resolution if f_resolution else 1/10. # [f_scale]
        if (filterbank_func is None):
            filterbank_func = fft_filterbank(samplerate, f_resolution*f_scale, correct_PG=True)
        self.filterbank_func = filterbank_func
        self.nyqzone = nyqzone
        self.f_scale = self.xscale = f_scale
        self.t_scale = self.yscale = t_scale # Synonym similar to xscale.
        self.cmap = cmap
        self.P_range = P_range
    
    def _plot_(self, signal, label, plotidx):
        """
            @return (time [t_scale], frequency [f_scale], PSD [dBm/Hz]) for time and frequency ranges
        """
        freq, S = self.filterbank_func(signal, self.samplerate, nyqzone=self.nyqzone)
        time = np.arange(S.shape[0])*S.shape[1]/self.samplerate
        time /= self.t_scale
        freq /= self.f_scale
        S = 10*np.log10(np.abs(S)**2) + 30 # complex amplitude/Hz -> [dBm/Hz]
        
        # Plot only if plotidx >= 0
        if (plotidx >= 0): # TODO: override prepare() to display traces in columns rather than rows.
            v_range = [np.percentile(S,1), np.percentile(S,99)] if (self.P_range is None) else self.P_range
            pylab.imshow(S, aspect='auto', extent=(freq[0],freq[-1], time[-1],time[0]),
                         vmin=v_range[0], vmax=v_range[-1], cmap=self.cmap, interpolation=None)
            if (self.f_range is not None): # Default display range
                pylab.xlim(self.f_range)
            pylab.xlabel('Frequency [%s]'%({MHz:"MHz",MHz/1e6:"Hz"}[self.xscale]))
            pylab.ylabel('Time [%ss]'%({1:"",1e-3:"m",microsec:"$\mu$"}.get(self.yscale,"/%g"%self.yscale)))
            pylab.title(label)
            
        # Return data cropped for default frequency display range
        if (self.f_range is not None):
            indexrange = np.logical_and(freq>=self.f_range[0], freq<=self.f_range[-1])
            freq = freq[indexrange]
            S = S[:,indexrange]
            
        return (time, freq, S)

@_test_()
def _test_SPECTROGRAM_(_test_):
    f_s = 2e9
    t = np.arange(2**18)*1/f_s
    s = np.random.randn(len(t)) + np.sum([np.sin(2*np.pi*(f_s/4.+df*MHz)*t) for df in [-10,-5,0,5,10]], axis=0)
    
    # Defaults
    _S = SPECTROGRAM(f_s)
    _S(s, "A comb in a haystack")

    # Crop frequency
    _S = SPECTROGRAM(f_s, f_range=0.5*f_s/2./MHz+np.asarray([-20,20]), f_resolution=1, f_scale=MHz)
    _S(s, "It's a 5 toothed comb!")


class CONSTELLATION_DIAGRAM(GraphProbe):
    """ This probe produces constellation diagrams (amplitude & phase) of modulated signals. """
    def _plot_(self, signal, label, plotidx, f_c, BW):
        """
            Make a constellation diagram for the signal when de-modulated with the specified carrier & bandwidth.
            @param f_c: the centre frequency of the modulated signal of interest [Hz].
            @param BW: the bandwidth of the modulated signal of interest [Hz].
            @return (I, Q) - the down-sampled, band-limited, demodulated in-phase and quadrature phase components.
        """
        t_sample = np.arange(len(signal))*1/self.samplerate
        I, Q = self.mcd(t_sample, signal, f_c, BW)
        
        # Plot only if plotidx >= 0
        if (plotidx >= 0):
            pylab.plot(I, Q, '.')
            pylab.xlabel('In-phase amplitude')
            pylab.ylabel('Quadrature amplitude')
            pylab.title(label)
            
        return (I, Q)
    
    @staticmethod
    def mcd(t_sample, signal, f_c, BW, downsample=True):
        """
            Determines the in-phase and quadrature phase components of a modulated signal.
            @param t_sample: time series for which to generate the signal [sec].
            @param signal: the series within which the modulated signal is embedded.
            @param f_c: the centre frequency of the modulated signal of interest [Hz].
            @param BW: the bandwidth of the modulated signal of interest [Hz].
            @param downsample: False to leave the I,Q components at the original sampling rate, or True to downsample to 10*BW/2 (default True).
            @return: (in-phase, quadrature phase) signal components.
        """
        samplerate = 1/np.diff(t_sample).mean()
        # Band limit
        signal = bpfilter(signal, [f_c-BW/2.,f_c+BW/2.], samplerate, 0.01, [f_c-BW,f_c+BW], 20, ftype="cheby1")
        # Project I & Q components
        s_sin = signal * np.sin(2*np.pi*f_c*t_sample)
        s_cos = signal * np.cos(2*np.pi*f_c*t_sample)
        s_sin = bpfilter(s_sin, [0,BW/2.], samplerate, 0.01, [0,BW], 20, ftype="cheby1")
        s_cos = bpfilter(s_cos, [0,BW/2.], samplerate, 0.01, [0,BW], 20, ftype="cheby1")
        if downsample: # Down-sample since the signal is now band-limited
            x = np.arange(0,len(signal), 1)
            x_new = np.arange(0,len(signal), samplerate/(8 * BW/2.)) # Over-sample by factor 8 (>4) to represent trajectory between constellation points
            s_sin = interp(x_new, x, s_sin)
            s_cos = interp(x_new, x, s_cos)
        return s_sin, s_cos


@_test_()
def _test_CONSTELLATIONDIAGRAM_(_test_):
    from rfiuck.siggen import QAM_series
    # Visually inspect & confirm
    f_s = 2e9
    t_s = np.arange(2**20)/f_s
    
    f_c = f_s/11.
    T_symbol = (t_s.max()-t_s.min())/4000. # 4000 symbols in the series
    BW = 2/T_symbol # Width to first null
    
    for NSYMB in [4,16]:
        _CD = CONSTELLATION_DIAGRAM(f_s)
        samples = QAM_series(t_s, f_c/MHz, T_symbol, NSYMB)
        _CD(samples, "NQAM=%d"%(NSYMB), f_c=f_c, BW=BW)


def plot_cat(categories, values, fmt='-', fontsize='x-small', rotation=15, ordered=False, **kwargs):
    """ Plot categorical data, without matplotlib v2.0. Use as e.g.
            ax = plot_cat('abcd', [1,2,1,3], '-o', markersize=10)
        @param fontsize: x-axis tick font size (default 'x-small').
        @param rotation: x-axis tick rotation [deg] (default 15).
        @param ordered: set to False if the categories must not be re-ordered automatically (default False).
        @param kwargs: additional arguments to pass to axis.plot() (may include 'axis', or else plots on current axis).
    """
    axis = kwargs.pop("axis", pylab.gca())
    if (len(categories) == 0):
        return axis
    ordered = lambda x: x if ordered else sorted(list(set(x)))
    if not hasattr(axis, "__cat_idx__"): # First time to plot on this axis
        cats = [c for c in categories]
        axis.__cats__ = ordered(cats) # A set of unique categories with order fixed in a list
        axis.__cat_idx__ = np.arange(len(axis.__cats__)).tolist() # The index along the abscissa for each category
    else: # Axis has previous list of categories, need to merge the new set
        cats = [c for c in categories if c not in axis.__cats__] # New categories (may overlap existing)
        axis.__cats__ += ordered(cats)
        axis.__cat_idx__ += (np.arange(len(cats)) + max(axis.__cat_idx__)+1).tolist()
    
    cat_idx = [axis.__cat_idx__[axis.__cats__.index(c)] for c in categories]
    axis.plot(cat_idx, values, fmt, **kwargs)
    
    axis.xaxis.set_ticks(axis.__cat_idx__)
    labels = axis.xaxis.set_ticklabels(axis.__cats__)
    for label in labels:
        label.set_rotation(rotation)
        label.set_fontsize(fontsize)
    return axis


if __name__ == "__main__":
    _test_PEAK_detector_()
    _test_P_dBm_()
    _test_PSD_()
    _test_CSD_()
    _test_GAIN_()
    _test_NOISEFIGURE_()
    _test_ZEROSPAN_()
    _test_SPECTROGRAM_()
    _test_CONSTELLATIONDIAGRAM_()