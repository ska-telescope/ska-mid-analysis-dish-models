"""
    Copyright @ 2019 SARAO (http://www.ska.ac.za). All rights reserved.
    
    "Two port" analogue & digital signal processing blocks
    
    @author: aph@ska.ac.za
"""
from __future__ import division
import numpy as np
import scipy.signal
import scipy.optimize
from rfsim.base import kB, scale_signal, signal_power, upsample, interp, _test_
try:
    import mkl_fft._numpy_fft as np_fft
except:
    np_fft = np.fft


def medfilt(s, N):
    if np.any(np.iscomplex(s)):
        return scipy.signal.medfilt(np.real(s),N) + 1j*scipy.signal.medfilt(np.imag(s),N)
#         return scipy.signal.medfilt(np.abs(s),N)*np.exp(-1j*scipy.signal.medfilt(np.angle(s),N))
    else:
        return scipy.signal.medfilt(s,N)
def medfilt2d(s, NM):
    if np.any(np.iscomplex(s)):
        return scipy.signal.medfilt2d(np.real(s),NM) + 1j*scipy.signal.medfilt2d(np.imag(s),NM)
#         return scipy.signal.medfilt2d(np.abs(s),NM)*np.exp(-1j*scipy.signal.medfilt2d(np.angle(s),NM))
    else:
        return scipy.signal.medfilt2d(s,NM)


def div(x, y, z=np.inf):
    """
        @param x: any number
        @param y: any other number
        @param z: any number, default inf
        @return: x/y unless y == 0, in which case z is returned.
    """
    return z if (y == 0) else x/y


def mix(signal, f_s, f_LO, T=None, asList=False):
    """
        @param f_s: The rate at which "s" is sampled. Must be >> f_LO*max(shape(T))
        @param T: table of mixer products i.e. a 2x2 matrix with RF harmonics on rows and LO harmonics on columns,
        in dBc below IF output. Default is [[None, -10], [-10, 0]] (entry[0,0] represents DC*DC)
        @param asList: True to return a list of terms, False to return the sum of the terms.
        @return: either [T[0][1]*LO, T[0][2]*LO^2, ..., T[1][1]*signal*LO, ..., T[M][N]*(signal^M*LO^N)], ["0x1", .. "MxN"]
        or only the sum.
    """
    if (T == None):
        T = [[None, -10],
             [-10, 0]]
    
    sout, lout = [], []
    
    if (f_LO*max(np.shape(T)) > f_s/2.):
        print("WARNING: Aliasing might occur while mixing. Use a higher-rate input signal.")
    t = np.arange(len(signal))*1/f_s
    
    for i in range(len(T)):
        for j in range(len(T[i])):
            if not (i == 0 and j == 0) and not (T[i][j] == None):
                LO_j = np.cos(2*np.pi*f_LO*j*t)
                signal_i = signal**i # TODO: Unfortunately this produces not just frequencies f_signal*i -- can we use upsample()?
                s_ij = signal_i*LO_j # Produces frequency components f_signal_i +- f_LO*j
                sout.append(np.asarray(10**(-T[i][j]/20.)*div(s_ij,np.std(s_ij),s_ij)))
                lout.append("%dx%d"%(i,j))
    
    if (asList):
        return sout, lout
    else:
        return sum(sout, 0)


def _filtfilt_(TFT, coeff, signal, causal, debug=False):
    """ Applies 2x cascaded filter using either 'scipy.signal.filtfilt()' (causal=False) to yield a zero phase
        (and zero delay!) response, or 'scipy.signal.lfilter()' (causal=True) which will have non-zero phase.
        With causal=False the signal is zero padded by len(signal)//2 with "odd" symmetry, to minimize the filter's
        transient effects. With causal=False there's no padding.
        
        With TFT='sos' the functions used are either 'scipy.signal.sosfiltfilt()' or 'scipy.signal.sosfilt()'
        
        @param TFT: 'ba' or 'sos', used to interpret 'coeff'.
    """
    # TODO: understand why this extra padding, with default padtype ('odd'), lowers ringing at start and/or end, when same filter is applied twice.
    signal = np.r_[0, signal, 0]
    
    padlen = len(signal)//2
    if (TFT == 'ba'):
        b, a = coeff
        assert (max(abs(np.roots(a))) <= 1), "The filter is unstable. Consider applying reduced or cascaded filters"
        if causal:
            zi = scipy.signal.lfilter_zi(b, a)
            y,zf = scipy.signal.lfilter(b, a, signal, zi=zi)
            y,zf = scipy.signal.lfilter(b, a, y, zi=zi)
        else: # filtfilt() applies the filter twice
            y = scipy.signal.filtfilt(b, a, signal, padlen=padlen, padtype='odd')
        description = "%s / %s"%(b, a)
        c2zpk = lambda coeff: scipy.signal.tf2zpk(*coeff)
    elif (TFT == 'sos'):
        sos = coeff
        if causal:
            zi = scipy.signal.sosfilt_zi(sos)
            y,zf = scipy.signal.sosfilt(sos, signal, zi=zi)
            y,zf = scipy.signal.sosfilt(sos, y, zi=zi)
        else: # sosfiltfilt() applies the filter twice
            y = scipy.signal.sosfiltfilt(sos, signal, padlen=padlen, padtype='odd')
        description = "Cascade[%d] of %s"%(len(sos), sos[0])
        c2zpk = scipy.signal.sos2zpk
    
    y = y[1:-1] # Remove extra padding
    
    if debug:
        try:
            z, p, k = c2zpk(coeff)
            O = max([np.argmax(z!=0), np.argmax(p!=0)])+1
            print("DEBUG: _filtfilt_ for 2x O(%d): %s"%(O,description))
            # Plot pole/zero diagrams
            import pylab as plt
            fig,ax = plt.subplots(2, 1)
            ax[0].axhline(0, ls=":")
            ax[0].axvline(0, ls=":")
            ax[0].add_patch(plt.Circle((0, 0), radius=1, fill=False, linestyle="dotted"))
            ax[0].scatter([x.real for x in z], [x.imag for x in z], marker="o", edgecolor="r", facecolor="none", linewidths=1.5, s=60)
            ax[0].scatter([x.real for x in p], [x.imag for x in p], marker="x", c="b", linewidths=1.5, s=60)
            ax[0].set_title("Pole/Zero diagram for %s"%description)
            w,H = scipy.signal.freqz_zpk(z,p,k)
            ax[1].plot(w, 20*np.log10(np.abs(H)))
            ax[1].set_xlabel("rad/s"); ax[1].set_ylabel("dB")
            ax[1].set_title("Filter magnitude plot")
             
        except Exception as e:
            print("DEBUG: ignoring an error that's prevented generating debug info for _fitfilt_\n", e)
    return y


def _iirdesign_(wp, ws, gpass, gstop=None, N=None, analog=False, ftype='ellip', output='ba'):
    """ A minor extension of the basic scipy.signal.iirdesign to afford both the standard use (default behaviour)
        while also allowing one to force the filter order and critical frequencies.
        @param wp, ws: passband and stopband frequencies; always mandatory since it determines the kind of filter being specified.
        @param N: > 0 to prescribe filter order, in which case the critical frequency(ies) are calculated from 'wp' & 'gpass'
                and gstop is effectively ignored (default None).
    """
    # The code below is an exact copy of the scipy implementation, except where explicitly marked
    try:
        ordfunc = scipy.signal.filter_design.filter_dict[ftype][1]
    except KeyError:
        raise ValueError("Invalid IIR filter type: %s" % ftype)
    except IndexError:
        raise ValueError(("%s does not have order selection. Use "
                          "iirfilter function.") % ftype)

    wp = np.atleast_1d(wp)
    ws = np.atleast_1d(ws)
    band_type = 2 * (len(wp) - 1)
    band_type += 1
    if wp[0] >= ws[0]:
        band_type += 1

    btype = {1: 'lowpass', 2: 'highpass',
             3: 'bandstop', 4: 'bandpass'}[band_type]

    if N: # Extended beyond scipy implementation
        # TODO: Currently only for ftype 'butter'
        W0 = (10**(abs(gpass)/10.)-1)**((-1 if (band_type in (1,4)) else 1)/(2.*N))
        PB = np.tan(np.pi*wp/2.)
        if (band_type == 4): W0 = np.array([-W0, W0]); W0 = np.sort(np.abs(-W0/2.*(PB[-1]-PB[0]) + ((W0/2.*(PB[-1]-PB[0]))**2 + PB[0]*PB[-1])**.5))/PB
        Wn = 2/np.pi * np.arctan(PB*W0)
    else: # Standard scipy implementation
        N, Wn = ordfunc(wp, ws, gpass, gstop, analog=analog)
    Wn = [np.round(W,15) for W in np.atleast_1d(Wn)] # The extended & standard calculations above propagate errors slightly differently - caught by _test_filters_
    return scipy.signal.iirfilter(N, Wn, rp=gpass, rs=gstop, analog=analog, btype=btype,
                     ftype=ftype, output=output)


def bpfilter(signal, passband, f_sample, ripple, stopband=None, suppression=None, N=None, ftype="ellip", TFT='sos', lossless=False, causal=False, debug=False):
    """ Band pass filtering using either 'scipy.signal.sosfiltfilt()' (causal=False) to yield a zero phase
        (and zero delay!) response, or 'scipy.signal.sosfilt()' (causal=True) which will have non-zero phase.
        Note: 'sos'=cascaded second order sections are numerically more robust for high order filters than 'b/a'.
        @param ripple: filter permissible ripple or worst case attenuation in the pass band [dB]
        @param suppression: filter minimum attenuation at stopband frequencies [dB]
        @param N: force the filter order, in which case the critical frequencies are computed from 'passband' and 'ripple' (default None).
        @param ftype: 'cheby1' has fast stopband transition with monotonic response, but passband ripple.
                      'cheby2' has monotonic/flat passband with slower stopband transition with stopband ripple.
                      'ellip' has fastest stopband transition with ripple in both pass and stopbands.
                      (default 'ellip', which usually has lowest order of all types for similar characteristics).
        @param TFT: 'sos' for cascaded 2nd order realisation, or 'ba' for generic transfer function (default 'sos' since
                    'ba' sometimes fails silently if filter order becomes too large!)
        @param lossless: True to boost the signal with ripple/2; for 'cheby1' & 'ellip' this makes DC gain = 0dB (default False).
        @param causal: (default False)
        @return: band-pass filtered version of signal
    """
    stopband = stopband if (stopband is not None) else (0.99*passband[0],min(f_sample/2.,1.001*passband[1])) # Avoid potential ambiguity implicit by using _iirdesign_()
    assert (max(passband) <= f_sample/2.), "Passband frequencies cannot exceed Nyquist rate"
    assert (max(stopband) <= f_sample/2.), "Stopband frequencies cannot exceed Nyquist rate"
    wp = [2*passband[0]/f_sample, 2*passband[1]/f_sample]
    ws = [2*stopband[0]/f_sample, 2*stopband[1]/f_sample]
    if (wp[0] == 0) and (ws[0] == 0): # It's a low pass filter
        wp = wp[1]
        ws = ws[1]
    elif (wp[1] == 1) and (ws[1] == 1): # It's a high pass filter
        wp = wp[0]
        ws = ws[0]
    # Attenuation and order are doubled by filtfilt applying twice
    coeff = _iirdesign_(wp, ws, gpass=ripple/2., analog=0, ftype=ftype, output=TFT,
                        gstop=suppression/2. if suppression else None, N=(N+1)//2 if N else None)
    if lossless:
        signal = signal * 10**(ripple/2./20)
    return _filtfilt_(TFT, coeff, signal, causal, debug)


def lpfilter(signal, f_upper, f_sample, ripple, f_stop=None, suppression=None, N=None, ftype="ellip", TFT='sos', lossless=False, causal=False, debug=False):
    """ Low-pass filtering using either 'scipy.signal.sosfiltfilt()' (causal=False) to yield a zero phase
        (and zero delay!) response, or 'scipy.signal.sosfilt()' (causal=True) which will have non-zero phase.
        Note: 'sos'=cascaded second order sections are numerically more robust for high order filters than 'b/a'.
        @param ripple: filter permissible ripple or worst case attenuation in the pass band [dB]
        @param suppression: filter minimum attenuation at f_stop [dB]
        @param N: force the filter order, in which case the critical frequency is computed from 'f_upper' and 'ripple' (default None).
        @param ftype: 'cheby1' has fast stopband transition with monotonic response, but passband ripple.
                      'cheby2' has monotonic/flat passband with slower stopband transition with stopband ripple.
                      'ellip' has fastest stopband transition with ripple in both pass and stopbands.
                      (default 'ellip', which usually has lowest order of all types for similar characteristics).
        @param TFT: 'sos' for cascaded 2nd order realisation, or 'ba' for generic transfer function (default 'sos' since
                    'ba' sometimes fails silently if filter order becomes too large!)
        @param lossless: True to boost the signal with ripple/2; for 'cheby1' & 'ellip' this makes DC gain = 0dB (default False).
        @param causal: (default False)
        @return: low-pass filtered version of signal
    """
    f_stop = f_stop if (f_stop is not None) else min(f_sample/2.,1.001*f_upper) # Avoid potential ambiguity implicit by using _iirdesign_()
    assert (f_upper <= f_sample/2.), "Passband frequency cannot exceed Nyquist rate"
    assert (f_stop <= f_sample/2.), "Stopband frequency cannot exceed Nyquist rate"
    wp = 2*f_upper/f_sample
    ws = 2*f_stop/f_sample
    # Attenuation and order are doubled by filtfilt applying twice
    coeff = _iirdesign_(wp, ws, gpass=ripple/2., analog=0, ftype=ftype, output=TFT,
                        gstop=suppression/2. if suppression else None, N=(N+1)//2 if N else None)
    if lossless:
        signal = signal * 10**(ripple/2./20)
    return _filtfilt_(TFT, coeff, signal, causal, debug)


def hpfilter(signal, f_lower, f_sample, ripple, f_stop=None, suppression=None, N=None, ftype="ellip", TFT='sos', lossless=False, causal=False, debug=False):
    """ High-pass filtering exactly like 'lpfilter()'
        @return: high-pass filtered version of signal
    """
    f_stop = f_stop if (f_stop is not None) else 0.999*f_lower # Avoid potential ambiguity implicit by using lpfilter()
    return lpfilter(signal, f_lower, f_sample, ripple, f_stop, suppression, N, ftype, TFT, lossless, causal, debug)


@_test_()
def _test_filters_(_test_):
    n_pts = 2**16
    
    # Explicitly prescribed order 'N' must be identical to special general cases
    f_s = 1.
    BW = 0.1 # Normalised to f_s = 1
    s = np.cos(2*np.pi*0.15*np.arange(n_pts)/f_s) + np.random.randn(n_pts)/n_pts
    ftype = 'butter' # TODO: implement other filter types
    for (filter,f_3dB,f_stop) in [(lpfilter,BW,BW*3), (hpfilter,0.5-BW,0.5-BW*3), (bpfilter,(0.5*BW,1.5*BW),(0.5*BW/3.,1.5*BW*3))]:
        for N in [1,2,4,10]:
            sN = filter(s, f_3dB,f_s, 3, N=N, ftype=ftype)
            N_actual = ((N+1)//2) * 2 # All filters use _filtfilt_ i.e. N_actual is the next higher even number
            # Due to slow convergence of slope high N require f_stop > 2*f_3dB (20dB/decade ~ 10dB/x3 ~ 6dB/x2)
            sg = filter(s, f_3dB,f_s, 3, f_stop, suppression=N_actual*9.6-3, ftype=ftype)
            _test_.assert_isclose(sg, sN, "Filters differ for %s order %d"%(filter,N), rtol=0.01) # TODO: Currently fails for hpfilter@N=10
    
    # Transient response after repeated application of the same filter as originally noticed with amplify() when operating in saturation
    # NB: Expect failures if _filtfilt_() doesn't implement "extra padding"
    f_s = n_pts
    for sig in [np.cos, np.sin]: # cos starts at 1, sine wave starts at 0
        s = sig(2*np.pi*100*np.arange(n_pts)/f_s) # Exactly 100 cycles, i.e. start and end on the same amplitude
        for (filter,f_3dB) in [(lpfilter,0.299*f_s/2.),(lpfilter,0.499*f_s/2.), (bpfilter,(0.001*f_s/2.,0.499*f_s/2.))]:
            pylab.figure(); pylab.suptitle("_test_filters_\n%s(%s)"%(filter,sig))
            for N in [2,4]:
                s_1 = filter(s, f_3dB,f_s, 3, N=N, ftype='butter')
                s_2 = filter(s_1, f_3dB,f_s, 3, N=N, ftype='butter')
                pylab.subplot(211)
                pylab.plot(s_2, ".-", label="%s order %d"%(filter,N), alpha=0.8)
                pylab.subplot(212)
                pylab.psd(s_2, n_pts, f_s, label="%s order %d"%(filter,N), alpha=0.8)
                A, M, Z = s_2[:n_pts//100], s_2[2*n_pts//100:10*n_pts//100], s_2[-n_pts//100:]
                METRIC = lambda x: np.max(np.abs(x)); TOL = 0.12
                _test_.assert_isclose(METRIC(M), METRIC(A), "MAX error at start of %s^%d(%s) is excessive for BW %s"%(filter,N,sig,f_3dB), rtol=TOL)
                _test_.assert_isclose(METRIC(M), METRIC(Z), "MAX error at end of %s^%d(%s) is excessive for BW %s"%(filter,N,sig,f_3dB), rtol=TOL)
                METRIC = lambda x: np.std(x); TOL = 0.012
                _test_.assert_isclose(METRIC(M), METRIC(A), "STDDEV error at start of %s^%d(%s) is excessive for BW %s"%(filter,N,sig,f_3dB), rtol=TOL)
                _test_.assert_isclose(METRIC(M), METRIC(Z), "STDDEV error at end of %s^%d(%s) is excessive for BW %s"%(filter,N,sig,f_3dB), rtol=TOL)
            pylab.legend()


def add_noise(signal, f_s, Te=None, NF_dB=None, T0=290, Z0=1):
    """
        Add noise as per either noise figure or equivalent noise.
        @param signal: sampled signal, scaled relative to W.
        @param f_s: the sample rate of the signal in [Hz]
        @param Te: input-referred equivalent noise in [K]
        @param NF_dB: input-referred noise figure in [dB]
        @param T0: the reference temperature for the noise figure [K] (default 290).
    """
    T_noise = Te if (Te is not None) else T0*(10**(NF_dB/10)-1)
    if (T_noise > 0):
        noise = np.random.randn(*np.shape(signal))
        noise = scale_signal(noise, kB*T_noise*f_s/2., Z0)
        return signal + noise
    else:
        return signal

@_test_()
def _test_add_noise(_test_):
    f_s = 1e43 # Arbitrary
    
    # Basic tests for levels
    n_pts = 1e6 # Must be > 1e6 to converge to rtol=1e-6
    p0 = 0 # Test for additivity are further below
    s_0 = scale_signal(np.random.randn(int(n_pts)), p0)
    for Te in [100, 700, 3000]:
        s = add_noise(s_0, f_s, Te=Te)
        _test_.assert_isclose(kB*Te*f_s/2., signal_power(s)-p0,
                              "Wrong signal power after adding wide band noise:Te", rtol=1e-6)
    T0 = 290
    for NF in [1, 3, 7]:
        s = add_noise(s_0, f_s, NF_dB=NF, T0=T0)
        Te = T0*(10**(NF/10.)-1)
        _test_.assert_isclose(kB*Te*f_s/2., signal_power(s)-p0,
                              "Wrong signal power after adding wide band noise:NF", rtol=1e-6)
    
    # Test for no dependency on "system" parameters
    for n_pts in [2**10, 2**18, 2**19]:
        for f_s in [1e6, 1e8, 1e9, 1e10]:
            s_0 = scale_signal(np.random.randn(int(n_pts)), p0)
            s_1 = add_noise(s_0, f_s, Te)
            _test_.assert_isclose(kB*Te*f_s/2., signal_power(s_1)-p0,
                                  "Noise for 2^%.3f pts @%g MHz differs by more than 1%%"%(np.log2(n_pts),f_s/1e6), rtol=0.01)
    
    # Tests for additivity
    n_pts = 2**25 # With 2**28 converges to rtol=0.001 if p0/(kB*Te*f_s/2) < 150 (worst case below)
    f_s = 1e10
    for _s_ in ["sin", "rand"]:
        for p0 in [0, 1e-12, 1e-9]:
            s_0 = {"sin":lambda n_pts: np.sin(2*np.pi*100*np.arange(n_pts)/n_pts),
                   "rand":lambda n_pts: np.random.randn(int(n_pts))}[_s_](n_pts)
            s_0 = scale_signal(s_0, p0)
            for Te in [100, 700, 3000, 9e9]: 
                s = add_noise(s_0, f_s, Te=Te)
                _test_.assert_isclose(kB*Te*f_s/2., signal_power(s)-signal_power(s_0),
                                      "Power of added wide band noise (%s %g W + %g K) is out by more than 0.3%%"%(_s_,p0,Te), rtol=0.003)


def amplify(signal, G, IIP2=None, IIP3=None, IP1dBCP=None, OIP2=None, OIP3=None, OP1dBCP=None, Zin=1, Zout=1):
    """
        The signal is amplified with distortion, based on a fifth order polynomial gain function.
        Implemented as per [1] NI VSS, https://awrcorp.com/download/faq/english/docs/VSS_System_Blocks/AMP_B.htm
        NB: IP2 distortion generates distortion around DC, which is not filtered out.
  
        Note: "signal", "IP2", "IP3" & "P1dBCP" must all be on the same magnitude scale, i.e. [mW] or [W].
        @param signal: the input signal
        @param G: small signal power gain if input and output impedances are the same (i.e. S21) [dB]
        @param IIP2 | OIP2: input | output 2nd order intermodulation (not harmonic) intercept point [dBm|dBW], default None.
        @param IIP3 | OIP3: input | output 3rd order intercept point [dBm|dBW], default None.
        @param IP1dBCP | OP1dBCP: input | output 1dB compression point [dBm|dBW] - only applies if no IP3 given, default None.
        @param Zin: impedance relating input signal's amplitude to power, in Ohm (default 1, matched to signals module).
        @param Zout: impedance relating output signal's amplitude to power, in Ohm (default 1, matched to signals module).
        @return: an amplified (& distorted) version of signal
    """
    # Determine input-referred compression points
    assert (OIP2 is None or IIP2 is None), "Specifying both input and output IP2 is ambiguous - choose one!"
    if (OIP2 is not None):
        IIP2 = OIP2 - G
    assert (OIP3 is None or IIP3 is None), "Specifying both input and output IP3 is ambiguous - choose one!"
    if (OIP3 is not None):
        IIP3 = OIP3 - G
    assert (OP1dBCP is None or IP1dBCP is None), "Specifying both input and output 1dBCP is ambiguous - choose one!"
    if (OP1dBCP is not None):
        IP1dBCP = OP1dBCP - (G-1)
    
    # Linear S21 gain excluding compression
    a1 = 10**(G/20.)
    
    # Nonlinear coefficients are based on a polynomial, normalised to "linear gain"
    a2_1 = 0
    if (IIP2 is not None):
        IP2H = IIP2 + 10*np.log10(2)
        VIIP2 = np.sqrt(2*Zin*10**(IP2H/10.)) # Should use IP2H instead of IIP2 which is used in [1].
        a2_1 = - 1/VIIP2 # a2/a1 should be < 0 for compressive nonlinearity as per [1]. ASTRON report doesn't show this?
    a3_1 = 0
    if (IIP3 is not None):
        VIIP3 = np.sqrt(2*Zin*10**(IIP3/10.))
        a3_1 = - 4/3./VIIP3**2 # a3/a1 must be < 0 for compressive nonlinearity [1]. ASTRON report doesn't show this
    a5_1 = 0
    if (IIP3 is None and IP1dBCP is not None): # Only use a5_1 if IIP3 is unspecified [1].
        if False: # Does not yet give expected result for single tone tests.
            VIP1dBCP = np.sqrt(2*Zin*10**(IP1dBCP/10.))
            a5_1 = 8/5.*((10**(-1/20.)-1)/VIP1dBCP**4 - a3_1*3/4./VIP1dBCP**2) # [1] mistakenly suggests **3 instead of **4
        else: # Use IP3 estimated from 1dBCP, rather than a5_1 above. Gives exactly the expected result for single tone tests.
            _IIP3_ = IP1dBCP + 9.636
            VIIP3 = np.sqrt(2*Zin*10**(_IIP3_/10.)) # Calculation proceeds exactly as for a3_1 above
            a3_1 = - 4/3./VIIP3**2
    
    # Generation distortion based on small signal polynomial normalised to "linear gain"
    D = lambda signal: a2_1*signal**2 + a3_1*signal**3 + a5_1*signal**5
    upsample = 5 if (a5_1 != 0) else (3 if ((a2_1 != 0) or (a3_1 != 0)) else 0) # Keeping in mind the signal should already be oversampled, upsample 2 should be OK.
    if (upsample > 0): # Up-sample to limit aliasing of high frequency intermods, down-sample at the end again
        _D_ = D
        D = lambda signal: sample( _D_( sample(signal, 1, upsample+1, nyqzone=1)[1] ), upsample+1, 1, nyqzone=1)[1]
    distortion = D(signal)
    
    # Since nonlinear coefficients are normalised to "linear gain", the "compressed gain" is scaled assuming conservation of power
    a1 /= (1+np.var(distortion)/np.var(signal))**.5
    
    # Compute output and adjust the power scale
    signal = a1*(signal + distortion) * (Zout/Zin)**.5
    
    return signal

@_test_()
def _test_amplify(_test_):
    n_pts = 2**14
    
    # Basic test of linear gain
    p_i = 1e-9
    s_i = scale_signal(np.random.randn(n_pts), p_i)
    for G in [0.3, 1, 3, 8]:
        s_o = amplify(s_i, G)
        p_o = signal_power(s_o)
        _test_.assert_isclose(10**(G/10.), p_o/p_i, "Linear gain differs by more than 0.1%", rtol=0.001)

    # Test 1 dB compression point with single tone, as is standard.
    # Two tones & noise don't compress in the same ratio!
    G = 8 # Arbitrary choices for gain & 1dBCP
    IP1dBCP = -18
    for H,D in zip([0, 3.3, 14, 24], [1, 0.458, 0.044, 0]): # Headroom to 1dB point, and gain loss at each (1dB, 10%, 1%, 0%). All in dB
        p_i = 10**((IP1dBCP-H)/10.)
        s_i = np.sin(2*np.pi*100*np.arange(n_pts)/n_pts)
        s_i = scale_signal(s_i, p_i)
        s_o = amplify(s_i, G, IP1dBCP=IP1dBCP)
        p_o = signal_power(s_o)
        _test_.assert_isclose(G-D, 10*np.log10(p_o/p_i), "Gain @1dBCP headroom=%d dB differs by more than 0.71%%"%(H), rtol=0.0071)
    
    # Test two tone operation in compression regime, for loss of gain of the fundamental signal (i.e. narrow band).
    n_pts = 2**16 # Below this the tests start failing
    f_s = 1. # Arbitrary
    f_c = [0.13/np.pi*f_s, 0.14/np.pi*f_s]
    s_in = np.sum([np.sin(2*np.pi*f*np.arange(n_pts)/f_s) for f in f_c], axis=0)
    P_i = 10*np.log10(signal_power(s_in))
    
    PWR_FUND = lambda PSD,F: np.sum(PSD[(F>=0.99*f_c[0]) & (F<=1.01*f_c[-1])]) # Power excluding any harmonic signals, which appear either above or below the fundamentals in frequency
    s0 = amplify(s_in, G)
    HDRM, COMP = [19, 16, 13, 10, 6, 3, 0, -3, -6], {"IP1dBCP":[], "IIP2":[], "IIP3":[]}
    for H in HDRM: # Compression point is above the operating point by this many dB
        pylab.figure(); pylab.suptitle("_test_amplify\nHeadroom %g dB"%H)
        pylab.subplot(211); pylab.plot(s0)
        pylab.subplot(212); p0 = PWR_FUND(*pylab.psd(s0, n_pts, f_s, label='in')) # Reference measurement
        IPCP = P_i + H # Compression point is above the operating point by this many dB
        for CP in ["IP1dBCP", "IIP2", "IIP3"]:
            s_o = amplify(s_in, G, **{CP:IPCP})
            pylab.subplot(211); pylab.plot(s_o, label=CP, alpha=0.8)
            pylab.subplot(212); P,F = pylab.psd(s_o, n_pts, f_s, label=CP, alpha=0.8); p_o = PWR_FUND(P,F) # Test measurement
            GAIN = 20*np.log10(np.std(s_o)/np.std(s_in)) # Gain dB
            COMP[CP].append(p0/p_o - 1) # "gain compression" fraction
            _test_.assert_is(GAIN <= G, "Gain @%s headroom=%+d dB should be < %gdB, but is %gdB"%(CP,H,G,GAIN))
            _test_.assert_is(COMP[CP][-1] > 0, "Gain compression @%s headroom=%+d dB should be > 0, but is %g"%(CP,H,COMP[CP][-1]))
        pylab.legend()
    pylab.figure(); pylab.suptitle("_test_amplify, two tone input")
    pylab.plot(HDRM, COMP["IP1dBCP"], '.-', label="IP1dBCP")
    pylab.plot(HDRM, COMP["IIP2"], 'o-', label="IIP2")
    pylab.plot(HDRM, COMP["IIP3"], 'x-', label="IIP3")
    pylab.legend(); pylab.grid(True); pylab.xlabel("Headroom to *CP [dB]"); pylab.ylabel("Compression of the fundamental signals [linear fraction]")


def attenuator(signal, f_s, L, T0=290):
    """ A passive attenuator at thermodynamic equilibrium """
    return amplify(add_noise(signal,f_s,NF_dB=abs(L),T0=T0), -abs(L))

@_test_()
def _test_attenuator_(_test_):
    f_s = 18e9 # Arbitrary
    n_pts = 2**16 # > 2**16 for rtol<=0.01 in additive noise
    
    # Tests of loss only (no additive noise)
    s_0 = np.random.randn(n_pts)
    p0 = signal_power(s_0)
    for L in [-3, 0.1, 1.1, 3, 10]:
        s = attenuator(s_0, f_s, L, T0=0)
        _test_.assert_isclose(abs(L), 10*np.log10(p0/signal_power(s)), "Wrong attenuation measured", rtol=0.001)

    # Tests of additive noise by confirming the amount of noise added is balanced by the amount of noise suppressed in thermodynamic equilibrium (T_in = T0)
    for T_in in [100, 290, 1000]:
        s_0 = scale_signal(np.random.randn(n_pts), kB*T_in*f_s/2)
        p0 = signal_power(s_0)
        for L in [-3, 0.1, 1.1, 3, 10]:
            s = attenuator(s_0, f_s, L, T0=T_in)
            _test_.assert_isclose(p0, signal_power(s), "Output noise differ from input noise when in equilibrium", rtol=0.01)


def amplifier(signal, f_s, f_3dB, G, IIP2=None, IIP3=None, IP1dBCP=None, OIP2=None, OIP3=None, OP1dBCP=None, Te=None, NF_dB=None, T0=290, Zin=1, Zout=1):
    """
        A band-limited amplifier (2nd order Butterworth at output) with input-referred noise.
        When any of the non-linear parameters have been set, the signal is up-sampled prior to, and down-sampled after amplification.
        This is done to limit the potential impact from aliasing of distortion products.
        
        @param f_3dB: (f_low,f_high) frequencies where an output filter rolls of at -3 dB, or None (but then mind the output DC!)
    """
    s = add_noise(signal, f_s, Te, NF_dB, T0, Zin) # Add input-referred noise
    s = amplify(s, G, IIP2, IIP3, IP1dBCP, OIP2, OIP3, OP1dBCP, Zin, Zout)
    if f_3dB: # Apply output gain roll-off, e.g. to suppress "DC" from second order distortion
        s = bpfilter(s, f_3dB, f_s, 3, N=2, ftype="butter")
    return s

@_test_()
def _test_amplifier_(_test_):
    f_s = 1 # Arbitrary
    n_pts = 2**16 # Below this the tests start failing
    
    # Basic test of gain, no band limiting or non-linearity
    p_i = 1e-9
    s_i = scale_signal(np.random.randn(n_pts), p_i)
    for G in [0.3, 1, 3, 8]:
        s_o = amplifier(s_i, f_s, None, G, Te=0)
        p_o = signal_power(s_o)
        _test_.assert_isclose(10**(G/10.), p_o/p_i, "Basic gain differs by more than 0.1%", rtol=0.001)

    # Basic test of gain and band limiting, no non-linearity
    G = 2 # Arbitrary dB
    pylab.figure(); pylab.suptitle("_test_amplifier_")
    s_o = amplifier(s_i, f_s, None, G, Te=0) # Reference
    pylab.psd(s_o, n_pts, f_s, label="in")
    p0 = signal_power(s_o)
    for BW in [0.04, 0.07, 0.3]: # BW as a fraction of the Nyquist band
        s_o = amplifier(s_i, f_s, (0,BW*f_s/2.), G, Te=0) # Band limited with total 4th order ~Butterworth filter so NEBW~BW*1.02 (only valid if BW << 1)
        pylab.psd(s_o, n_pts, f_s, label="BW=%g"%BW)
        p_o = signal_power(s_o)
        _test_.assert_isclose(BW, p_o/p0, "Gain with 3dB bandwidth %g differs by more than 22%%"%BW, rtol=0.22) # rtol=0.1 if amplifier()'s filter order >= 8
    pylab.legend()


class TransferFunction(object):
    """ A transfer function that may be specified in the frequency domain. """
    def __init__(self, spectral_shape):
        self.spectral_shape = spectral_shape
        self.scale = 1.
    
    def spectrum(self, f):
        """ @return: the complex AMPLITUDE GAIN at the specified frequency(ies) [linear scale] """
        if (self.scale == 0):
            return 0 * f
        else:
            try: # Try to avoid slow np.vectorize, if possible.
                return self.scale * self.spectral_shape(f)
            except ValueError:
                return self.scale * np.vectorize(self.spectral_shape)(f)

    def impulse(self, t):
        """ @return: the complex AMPLITUDE GAIN of the (unit) impulse response [linear scale] """
        f_s = 1/np.diff(t).mean()
        f = np.fft.fftshift(np.fft.fftfreq(len(t), 1/f_s))
        s_f = self.spectrum(f) * 1/f_s**.5 # Complex gain spectrum multiplied with FT of unit impulse
        s_t = np_fft.ifft(np.fft.ifftshift(s_f)) # Convert from frequency to time domain
        return s_t
    
    def __mul__(self, factor):
        """factor*TransferFunction == TransferFunction*factor: Multiplication in frequency domain
           @return: another TransferFunction """
        if isinstance(factor, TransferFunction):
            return TransferFunction(lambda f: self.spectrum(f)*factor.spectrum(f))
        elif np.isscalar(factor): # Make new, scaled copy
            m = TransferFunction(self.spectrum)
            m.scale *= factor
            return m
        raise NotImplementedError()
    
    def __call__(self, signal, f_s):
        """ Convert signal to frequency domain, multiply and then convert back to time series.
            @return: time series """
        n_pts = len(signal)
        t_s = np.arange(n_pts)*1/f_s
        
        f_ = np.fft.fftshift(np.fft.fftfreq(n_pts, 1/f_s))
        factor_spectrum = np.fft.fftshift(np_fft.fft(signal, n_pts)) # Amplitude/bin
        s_f = self.spectrum(f_) * factor_spectrum
        if (np.max(np.abs(s_f)) == 0): # Zero<-FT->zero so avoid unnecessary processing, especially making IFFT choke
            return 0*t_s
        s_t = np_fft.ifft(np.fft.ifftshift(s_f)) # Convert from frequency to time domain
        s_t -= np.mean(s_t) # Remove any constant offset
        
        all_real = lambda x, rtol=1e-6: (np.max(np.abs((x.imag))) < rtol*np.max(np.abs(x.real)))
        return s_t.real if all_real(s_t) else s_t # We use two-sided spectra, so imaginary part must be negligible -- but just in case
        
    def __lmul__(self, factor):
        return self.__mul__(factor)
    
    def __rmul__(self, factor):
        return self.__mul__(factor)
    
    def __rdiv__(self, factor):
        """=factor/TransferFunction: Multiplication by inverse of transfer function, in frequency domain"""
        inv_self = TransferFunction(lambda f: 1/self.spectrum(f))
        return factor*inv_self

    def __rtruediv__ (self, factor):
        return self.__rdiv__(factor)

@_test_()
def _test_TransferFunction(_test_):
    for f_s in [12.5,78]:
        for N in [6,13,16]:
            T = np.arange(2**N)*1/f_s
            S = np.random.randn(len(T))
            # Test conservation of power, multiplication with constant H
            for H in [0.001,0.1,1,17]:
                ST = TransferFunction(lambda f: H)(S, f_s)
                _test_.assert_isclose(H**2*signal_power(S), signal_power(ST), "2^%d Signal***Transfer(f:%g) power is wrong"%(N,H), rtol=1e-15)

    # Test multiplication
    f = np.arange(-1000,1001,1)
    # Multiply with H=1
    T1 = TransferFunction(lambda f: 1)
    for i,T2 in enumerate([T1, TransferFunction(lambda f: np.abs(f)), TransferFunction(lambda f: np.sqrt(np.abs(f)))]):
        T12 = T1*T2
        _test_.assert_isclose(T2.spectrum(f), T12.spectrum(f), "Multiplication with constant H=1 for T2=%d"%i, atol=1e-26)
    # Square / Square roots
    for i,T2 in enumerate([T1, TransferFunction(lambda f: np.abs(f)), TransferFunction(lambda f: np.sqrt(np.abs(f)))]):
        Tsqrt = TransferFunction(lambda f: np.sqrt(T2.spectrum(f)))
        Tsqr = Tsqrt*Tsqrt
        _test_.assert_isclose(T2.spectrum(f), Tsqr.spectrum(f), "Square of Square root fails for T2=%d"%i, rtol=1e-15)


def coax(signal, f_s, k1, k2, length, connectors=None, T0=290):
    """ A typical RF coaxial cable, implemented using TransferFunction for both attenuation and noise.
        Connector loss is '0.02+0.025*sqrt(GHz)' per connector, and '+0.1dB' for angled connectors.
        Total loss is clamped to guarantee L>=0dB.
        @param k1: [dB/GHz^.5/m]
        @param k2: [dB/GHz/m]
        @param length: [m]
        @param connectors: defines connectors - e.g. "N" (shorthand for "N,N"), "SMA" (i.e. "SMA,SMA") or "N,SMA";
                           prefix and/or append the character '^' to indicate angled connectors. Alternative format is to specify
                           the total connector loss for the assembly as `lambda f_GHz:total_connector_loss_dB` (default None).
    """
    if callable(connectors):
        L_c = connectors
    elif isinstance(connectors, str):
        # [SHF] <http://pdf.directindustry.com/pdf/radiall/ultra-low-loss-shf-cable-assemblies/16192-216259.html>  0.02+0.025*sqrt(GHz) per connector
        # [ASF] <http://www.atlantecrf.com/Cable_Assembly_Catalogue_eflip/pubData/source/Cable-Catalogue.pdf>  0.005+0.025*sqrt(GHz) per connector
        # [CCA] <https://www.timesmicrowave.com/DataSheets/Literature/CCA-R.pdf>  0.009 + 0.041*sqrt(GHz) per connector, +0.1dB per  right angle
        # solve(join(a+b*F^.5+(c*F^.5+d*F)*0.1-L01, a+b*F^.5+(c*F^.5+d*F)*1-L1)), F=[2,18], L01=[0.18,0.60], L1=[1.18,4.10], ratprint=false
        if (connectors.startswith("N") or connectors.startswith("SMA")):
            L_c0 = lambda f_GHz: 0.02 + 0.025*(f_GHz)**.5
        elif (connectors.startswith("^N") or connectors.startswith("^SMA")):
            L_c0 = lambda f_GHz: 0.12 + 0.025*(f_GHz)**.5
        elif (connectors.startswith(",")):
            L_c0 = lambda f_GHz: 0
        if (connectors.endswith("N") or connectors.endswith("SMA")):
            L_c1 = lambda f_GHz: 0.02 + 0.025*(f_GHz)**.5
        elif (connectors.endswith("N^") or connectors.endswith("SMA^")):
            L_c1 = lambda f_GHz: 0.12 + 0.025*(f_GHz)**.5
        elif (connectors.endswith(",")):
            L_c1 = lambda f_GHz: 0
        L_c = lambda f_GHz: L_c0(f_GHz) + L_c1(f_GHz)
    elif connectors: # Not None or 0
        L_c = lambda f_GHz: connectors
    else:
        L_c = lambda f_GHz: 0
    L = lambda f_GHz: np.clip(L_c(f_GHz) + (k1*(f_GHz)**.5+k2*(f_GHz))*length, 0,None) # numpy rendition of per-element max(L, 0)
    
    noise = scale_signal(np.random.randn(*np.shape(signal)), kB*1*f_s/2.) # Flat spectrum 1K noise
    noise = TransferFunction(lambda f: (T0*(10**(L(np.abs(f)/1e9)/10.)-1))**.5) (noise, f_s)
    signal = signal + noise # Input-referred noise
    attenuated = TransferFunction(lambda f: 10**(-L(np.abs(f)/1e9)/20.)) (signal, f_s)
    return attenuated

@_test_()
def _test_coax_(_test_):
    f_s = 18e9
    n_pts = 2**18 # >= 2**18 to converge as below
    p0 = 1 # Reference signal power, arbitrary
    
    # Tests of loss (no additive noise) with narrow band signal at spot frequency
    pylab.figure(); pylab.suptitle("_test_coax_")
    for f0 in [0.5e9, 2.8e9]:
        s_0 = bpfilter(np.random.randn(n_pts), [f0-20e6,f0+20e6], f_s, 0.1, [f0-100e6,f0+100e6], 40, ftype='cheby1')
        s_0 = scale_signal(s_0, p0)
        
        # Tests just length, with no connectors, loss[dB] \propto length[m]
        s = coax(s_0, f_s, 0.123,0.07, 1, connectors=None, T0=0)
        L1 = 10*np.log10(p0/signal_power(s))
        pylab.psd(s, n_pts//16, f_s)
        for l in [0.1, 1, 1.1, 3, 10]:
            s = coax(s_0, f_s, 0.123,0.07, l, connectors=None, T0=0)
            pylab.psd(s, n_pts//16, f_s)
            _test_.assert_isclose(L1*l, 10*np.log10(p0/signal_power(s)), "At %gMHz, wrong attenuation for length %g"%(f0/1e6,l), rtol=0.001)
        
        # Tests for connector losses, loss with connectors > 2*0.02dB + loss without connectors
        s = coax(s_0, f_s, 0.123,0.07, 1, connectors=None, T0=0)
        L1 = 10*np.log10(p0/signal_power(s))
        for connectors in ["N", "SMA", "N,N", "N,SMA"]:
            s = coax(s_0, f_s, 0.123,0.07, 1, connectors=connectors, T0=0)
            _test_.assert_is(L1+2*0.02 < 10*np.log10(p0/signal_power(s)), "At %gMHz, connectors %s don't increase loss"%(f0/1e6,connectors))
        
        # Tests just k2, with k1=0 and no connectors, loss[dB] = k2*f0_GHz*length
        for k2 in [0.01, 0.5, 1, 3]:
            s = coax(s_0, f_s, 0,k2, 1, connectors=None, T0=0)
            _test_.assert_isclose(k2*f0/1e9*1, 10*np.log10(p0/signal_power(s)), "At %gMHz, wrong attenuation for k2 %g"%(f0/1e6,k2), rtol=0.002)
    
    # Tests of loss SLOPE (no additive noise) with broad band signal at spot frequency
    s_0 = np.random.randn(n_pts)
    # With k1=0, slope = -k2*l [dB/GHz]
    pylab.figure(); pylab.suptitle("_test_coax_") 
    P0, F = pylab.psd(s_0, n_pts//256, f_s, scale_by_freq=True) # W/Hz, Hz
    DF = np.diff(F).mean()
    for l in [3, 10]:
        for k2 in [0.01, 0.5]:
            s = coax(s_0, f_s, 0,k2, l, connectors=None, T0=0)
            P, F = pylab.psd(s, n_pts//256, f_s, scale_by_freq=True) # W/Hz, Hz
            slope = np.mean(np.diff(10*np.log10(P/P0))[4:-4]) * 1e9/DF # dB/GHz; edge channel errors may have a big impact if slope is small 
            _test_.assert_isclose(-k2*l, slope, "Wrong slope for length %g, k2 %g"%(l,k2), rtol=0.003)
    
    # Tests of additive noise by confirming the amount of noise added is balanced by the amount of noise suppressed in thermodynamic equilibrium (T_in = T0)
    pylab.figure() 
    for T_in in [100, 290, 1000]:
        s_0 = scale_signal(np.random.randn(n_pts), kB*T_in*f_s/2)
        P0, F = pylab.psd(s_0, n_pts//256, f_s, scale_by_freq=True) # W/Hz, Hz
        p0 = signal_power(s_0)
        for l in [3, 10]:
            s = coax(s_0, f_s, 0.123,0.07, l, connectors=None, T0=T_in)
            _test_.assert_isclose(p0, signal_power(s), "Output noise differ from input noise when in equilibrium @ %gK"%T_in, rtol=0.01)
            P, F = pylab.psd(s, n_pts//256, f_s, scale_by_freq=True) # W/Hz, Hz
            slope = np.mean(np.diff(10*np.log10(P/P0))[4:-4]) * 1e9/DF # dB/GHz; edge channel errors may have a big impact if slope is small 
            _test_.assert_isclose(0, slope, "Wrong slope for length %g when in equilibrium @ %gK"%(l,T_in), atol=0.05) # absolute tolerance in dB


def quantizer_thresh(nbits, dBFS, Z0=1):
    """ By definition, level_width * Nlevels_tot = Vfs_pp 
        @param nbits: NOB
        @param dBFS: full scale RMS power level in dBW
        @param Z0: impedance relating FS power to the quantizer's amplitude scale, in Ohm (default 1, matched to signals module).
        @return: level_width
    """
    VFS = (2*Z0*10**(dBFS/10.))**.5
    level_width = VFS / (2**nbits)
    return level_width

def quantize(signal, nbits, level_width, inl_error='ADC12J4000', SFDR=None, as_int=False):
    """
        Quantizes the input signal to discrete, equi-spaced levels. Even though the output are
        discrete values representable by integers, the default behaviour is to scale the output
        with the thresholding level width to preserve the input scale.
        Note: inl_error may skew the quantization noise!
        @param level_width: typically N*std(signal) with N = 0.335 for 4b [EVLA memo 88].
        @param inl_error: a vector of integral nonlinearity errors for each of the levels, or ADC model name (default 'ADC12J4000').
        @param SFDR: peak harmonic distortion in [dBFS], only used if inl_error represents an ADC model (default None).
        @param as_int: True to return integers (0 - full scale), False to scale it to match the scale of the input (default False). 
        @return: a quantized version of the original "analogue" signal.
    """
    yrange = np.arange(-2**(nbits-1),2**(nbits-1),1)
    yrange = yrange - np.mean(yrange) # Central value of threshold levels
    # Distort the transfer function (at input thresholds)
    if (isinstance(inl_error, str) and SFDR is not None): # Distortion from SFDR specification
        inl_error = _load_inl_(inl_error,nbits, SFDR)[1]
    else:
        inl_error = None
    dnl = 0*yrange # DNL
    if (inl_error is not None): # Add distortion from integral nonlinearity to thresholds
        dnl[:-1] += np.diff(inl_error) # DNL measured upwards from first code
        dnl -= np.mean(dnl)
    xthresh = yrange + 0.5 + dnl # Upper value of level
    qsignal = signal/level_width
    qsignal[qsignal<=xthresh[0]] = yrange[0] # (-inf,min]
    for l,u,y in zip(xthresh[:-2],xthresh[1:-1],yrange[1:-1]):
        qsignal[np.logical_and(qsignal>l,qsignal<=u)] = y # (-0.5,0.5]
    qsignal[qsignal>xthresh[-2]] = yrange[-1] # (max,+inf)
    if as_int: # Convert to integers from 0 to full scale
        return np.asarray(qsignal-np.min(yrange), int)
    else: # Convert to same scale as input
        return level_width*qsignal

def _load_inl_(prototype, to_nbits, to_SFDR):
    """ Loads INL from prototype file & scales it to the new number of bits & SFDR.
        @param to_bits,to_SFDR: the characteristics of the output codes ([ideal NOB] & [dBFS])
        @return: (codes, INL) the full range of codes starting from 0, with INL accumulated up to the corresponding code
    """
    # Load the data
    fn = __file__+"/../__res__/%s_INL.csv"%prototype
    codes,inl = np.loadtxt(fn,delimiter=",",unpack=True) # Expect the full range of codes starting from 0, with INL accumulated up to the corresponding code
    NOB = int(np.log2(np.max(codes)+1) + 0.5)
    ENOB = NOB - np.log2(6*np.std(inl))
    SFDR = ENOB*9 # Scaling due to just quantization -- using SFDR from datasheets accompanying INL files doesn't work (why?)
#     print("DEBUG: %s: SFDR %.1fdB  ENOB %.1fbit (%.1fbit)"%(prototype,SFDR,ENOB,NOB-np.log2(2*(np.max(inl)-np.min(inl))))) # std is 100% for AT83 & ASNT but 1bit too optimistic for AC12J
#     SFDR,ENOB = {'ADC12J4000':(-62,8.8), # Datasheet typical SFDR -60 - -65dBFS, ENOB ~8.8bits
#                  'AT83AS008':(-58,7.7), # Datasheet typical SFDR -58dBFS, ENOB ~7.7bits.
#                  'ASNT7123':(-36,3.4)}[prototype] # Dataseet below 8 GHz, SFDR -36dBFS, ENOB ~3.4bits.
    
    # Re-sample in case there are missing codes
    K = np.arange(0,2**NOB,1); inl = np.interp(K, codes, inl); codes = K
    # Resample to output resolution without scaling, |SFDR| increases by at most 9*(to_nbits-NOB)
    K = np.arange(0,2**to_nbits,1); INL = np.interp(K*2**NOB/2**to_nbits, codes, inl)
    
    # Scale with SFDR
    SFDR = abs(SFDR) + 8*(to_nbits-NOB) # 8* works reasonably for all prototypes over a range of to_SFDR, 7* & 9* are worse
    to_SFDR = abs(to_SFDR) if to_SFDR else SFDR
    INL *= 10**((SFDR-to_SFDR)/20.)
    
    # Reduce missing codes e.g. caused by "small scale noise" as opposed to "large scale distortion" which would be valid
    DNL = np.diff(INL)
    _DNL = np.diff(lpfilter(INL, 0.0125, 1, 0.01, 0.5, 20, ftype="cheby1"))
    DNL[np.abs(DNL)>0.49] = _DNL[np.abs(DNL)>0.49]
    INL = np.cumsum(np.r_[0, DNL])
    
    INL -= np.mean(INL)
    return K, INL

@_test_()
def _test_quantize_ideal_(_test_):
    # Confirm there's no offset introduced in ideal ADC
    for nbits in [1,2,8,10,4]:
        resolution = 1e-3
        level_width = 1
        s = level_width * np.arange(-2**(nbits-1), 2**(nbits-1), resolution)
        q = quantize(s, nbits, level_width=level_width)
        _test_.assert_isclose(np.mean(s), np.mean(q), "%dbits: Mean of quantized signal has changed at > resoln*LSB"%nbits, atol=resolution*level_width)
        _test_.assert_isclose(np.abs(np.min(q)), np.abs(np.max(q)), "%dbits: Min & Max of quantized signal is distorted at > resoln*LSB"%nbits, atol=resolution*level_width)
        d = s - q
        _test_.assert_isclose(0, np.mean(d), "%dbits: Mean of quantization noise > resoln*LSB"%nbits, atol=resolution*level_width)
    # Make plot of transfer function for last nbits
    pylab.figure();
    # Ideal ADC 
    pylab.subplot(211); pylab.plot(s, q, '.')
    pylab.subplot(212); pylab.plot(s, d, '.')
    # Default ADC
    q = quantize(s, nbits, level_width=level_width, SFDR=-nbits*9)
    pylab.subplot(211); pylab.plot(s, q, '.'); pylab.grid(True);
    pylab.title("_test_quantize_ideal_\nInput to Output, %d bits scaled to SFDR~%.1fdBFS"%(nbits,-nbits*9))
    pylab.subplot(212); pylab.plot(s, s-q, '.'); pylab.grid(True); pylab.title("Input-Output, %d bits"%nbits)
    
@_test_()
def _test_quantize_(_test_):
    # These tests are based on a direct measurement of SFDR in frequency domain
    # B-H window to use so that spectral leakage shouldn't limit this test.
    bhw = lambda x: scipy.signal.windows.blackmanharris(len(x), sym=False)
    def spur_detector(F, P): # F,P from PSD
        P[0] = np.min(P) # Zero the DC bin, which is irrelevant and avoids unnecessary trouble
        Pmax = np.max(P)
        Fmax = F[P==Pmax]
        mask = np.logical_and(np.abs(F/Fmax-1)>0.1, P>np.percentile(P,50)) # Ignore the flanks of the tone, and also the floor
        Pspurs = np.sort(P[mask])[-5:] # Only the top 5
        Fspurs = [F[P==Pspur][0] for Pspur in Pspurs]
        return Fmax,Pmax, Fspurs,Pspurs
    
    # This demonstrates that all ADCs have SFDR that scale with NOB. TODO: convert to a real test?
    T = np.arange(1e5)*1/4e9
    s = 400*np.sin(2*np.pi*1.897e9*T) + 1e-6*np.random.randn(len(T))
    NOB = [4,6,8,10,12]
    for SFDR in [None,-30,-40,-50,-60,-70]:
        pylab.figure()
        if SFDR: pylab.plot(NOB,np.ones_like(NOB)*SFDR, 'k--', label="SFDR")
        for ADC in [None,'ADC12J4000','AT83AS008','ASNT7123','ADC12J3200']:
            measured_SFDR = []
            for nbits in NOB:
                level_width = np.max(s)/10**(-1/20.) / 2**(nbits-1) # Test signal is at -1dBFS
                q = quantize(s, nbits, level_width=level_width, inl_error=ADC, SFDR=SFDR) # Just scale with NOB
                P = 20*np.log10(np.abs(np_fft.rfft(q*bhw(q), len(q))))
                F = np.fft.rfftfreq(len(q), np.diff(T).mean())
                Fmax,Pmax, Fspurs,Pspurs = spur_detector(F, P)
                measured_SFDR.append(max(Pspurs)-Pmax)
            pylab.plot(NOB, measured_SFDR, 'o-', label=ADC)
        pylab.plot(NOB, -8*np.asarray(NOB), 'k', label="-8*NOB") # 8* appears a reasonable fit for all ADCs over a range up to SFDR
        pylab.xlabel("NOB [bits]"); pylab.ylabel("SFDR [dBc]")
        pylab.legend(); pylab.title("_test_quantize_")

    # Confirm SFDR is close to what it's specified to be. Test case comparable to ADC12J4000 datasheet.
    ADC = ['ADC12J4000','AT83AS008','ASNT7123','ADC12J3200'][0] # TODO: Improve over current PASS,FAIL = [(7,7), (2,12), (4,10), (7,7)]
    T = np.arange(1e5)*1/4e9
    s = 400*np.sin(2*np.pi*1.897e9*T) + 1e-6*np.random.randn(len(T)) # CW scales to 0dB/Hz, noise at ~120dB below fundamental: should be below SFDRmax = nbits*9.03 [dB]
    for nbits in [4,8,10,12]:
        level_width = np.max(s)/10**(-1/20.) / 2**(nbits-1) # Test signal is at -1dBFS
        pylab.figure()
        print("[%s]\n%dbit SFDR \t P_pk \t P_spur @ f_spur/f_pk..."%(ADC,nbits))
        for SFDR in [-30,-36,-52,-62,-999]: # dBFS
            q = quantize(s, nbits, level_width=level_width, inl_error=ADC, SFDR=SFDR)
            P,F = pylab.psd(q, len(q), Fs=1/np.diff(T).mean(), scale_by_freq=True, window=bhw, hold=True,
                            label="SFDR = %.1fdBFS"%(SFDR))
            Fmax,Pmax, Fspurs,Pspurs = spur_detector(F, 10*np.log10(P))
            print("%.f->%.f: \t%.f @ %.f - [\t%s]"%(SFDR, max(Pspurs)-Pmax, Pmax, Fmax, "; ".join(["%.1f @ %.2f"%(P,F/Fmax) for P,F in zip(Pspurs,Fspurs)])))
            if (SFDR>-9.03*nbits): # The fundamental limit is set by quantization
                _test_.assert_isclose(abs(SFDR), abs(Pmax-max(Pspurs)), "SFDR more than 4dB from what's requested", 4)
        pylab.psd(s, len(s), Fs=1/np.diff(T).mean(), scale_by_freq=True, window=bhw, hold=True,
                        label="Analogue")
        pylab.legend(); pylab.title("_test_quantize_: %s, %d bits"%(ADC,nbits))


def sample(signal, f_s, f_s_new, jitter_rms=0, nyqzone=0):
    """
        Re-sampling by interpolation (cubic spline fitting). Both time and amplitudes are re-sampled without
        scale change, so ENERGY IS CONSERVED but POWER IS NOT.
        
        Up-sampling by interpolation is equivalent to up-sampling by inserting zeroes, followed by low pass
        filtering. This eliminates the need for additional low pass filtering to mitigate repeat images.
        However, when down-sampling, low-pass filtering should be applied BEFORE sub-sampling, in order to
        mitigate aliasing of higher frequency components. For convenience you may use nyqzone=N | N>=1.
        
        @param f_s: sampling rate for "signal"
        @param f_s_new: desired sampling rate, or zero crossing clock signal sampled at a rate of f_s
        @param jitter_rms: RMS time interval error in sampling times, default 0. The applied phase noise spectrum
        exhibits a typical 1/f profile within f_s/100, with a baseline 23dB lower.
        @param nyqzone: >0 to also low-pass filter the signal ONLY for down-sampling (default 0 i.e. no filtering).
        @return: (t_new, s_new) s_new is a sub-sampled copy of signal, originally sampled at f_s but
        now sampled at f_s_new at time ticks t_new.
    """
    t_new = None # t_new & f_s_new may be derived from an actual clock signal!
    if not np.isscalar(f_s_new): # f_s_new is in fact the clock signal, so derive the sampling vector from it
        clk = f_s_new
        clk = np.roll(clk, shift=-1) * clk # Zero crossings at samples where sign changes; very last sample may be flagged spuriously!
        clk[clk<=0] = -1; clk[clk>0] = 0 # NZ at first sample after rising & falling crossings
        t_new = np.argwhere(clk[:-1]).ravel()[::2] * 1/f_s # Time at every second crossing 
        f_s_new = 1/np.mean(np.diff(t_new))
    
    if (f_s != f_s_new):
        # Up-sampling by FFT-based zero-padding ('scipy.signal.resample()') would have been an option only for nyqzone=1.
        if (f_s < f_s_new): # Upsample first to closest integer multiple to minimize interpolation artifacts
            N = int(f_s_new/f_s+0.5)
            signal = upsample(signal, N) * N # Creates Nyquist images & scale to conserve power
            f_s = f_s*N
            # Band limit before further resampling
            if (nyqzone == 1):
                signal = lpfilter(signal, 0.99*(f_s/N/2.), f_s, 0.01, (f_s/N/2.), 40, ftype="cheby1")
            elif (nyqzone > 1): 
                signal = bpfilter(signal, [(nyqzone-1)*(f_s/N/2.), nyqzone*(f_s/N/2.)], f_s, 0.01,
                                  [0.9*(nyqzone-1)*(f_s/N/2.), 1.1*nyqzone*(f_s/N/2.)], 40, ftype="cheby1")
        else: # Band limit before sub-sampling
            # TODO: Even for sub-sampling, first up-sample by e.g. 4, then LPF, then downsample. Example:
            # t_new,d_new = sample(sample(d, f_s, f_s_new*4)[1], f_s_new*4, f_s_new, jitter_rms, nyqzone=1)
            if (nyqzone == 1):
                signal = lpfilter(signal, 0.99*(f_s_new/2.), f_s, 0.01, (f_s_new/2.), 40, ftype="cheby1")
            elif (nyqzone > 1): 
                signal = bpfilter(signal, [(nyqzone-1)*(f_s_new/2.), nyqzone*(f_s_new/2.)], f_s, 0.01,
                                  [0.9*(nyqzone-1)*(f_s_new/2.), 1.1*nyqzone*(f_s_new/2.)], 40, ftype="cheby1")
    # For the above band limiting, Cheby I monotonic stopband is preferred over Cheby II and Elliptic 
    
    # Re-sample, which could still be required even if sample rates are the same
    t = np.arange(0, len(signal), step=1)*1/f_s
    if (t_new is None):
        t_new = t if ((f_s==f_s_new) and (jitter_rms==0)) else np.arange(0, len(signal), step=f_s/f_s_new)*1/f_s
    
    if (jitter_rms > 0):
        jitter = np.random.randn(len(t_new))
        jitter = lpfilter(jitter, 1e-4, 1, 0.1, 1e-2, 20, ftype="butter") + 1e-1*jitter # 1/f within f_s/100 + floor 20 dB lower (peak is 23 dB above floor)
        jitter *= jitter_rms/np.std(jitter)
        t_new += jitter
    
    # Final re-sampling (never more than +/-1/2 original Nyquist rate) by interpolation filtering
    signal = interp(t_new, t, signal)
    
    return t_new, signal

@_test_()
def _test_sample(_test_):
    F_S = 1
    
    ## Regular sampling cases
    T = np.arange(2**20)*1/F_S
    
    # Simplest cases: sub-sampling of a linear ramp
    S = T
    for N in [0.1, 0.5, 1]: # With f_s_new a frequency
        t, s = sample(S, F_S, N*F_S) # Can be out by a small fraction of a cycle
        _test_.assert_isclose(S[::int(1./N)], s, "Sub-sampling by %g is wrong"%N, atol=1e-9/F_S)
    for N in [0.0625, 0.125, 0.25]: # With f_s_new a clock signal, must be Nyquist sampled i.e. < 0.5
        t, s = sample(S, F_S, np.sin(2*np.pi*N*F_S*T)) # Starts sampling at first sample _after_ zero crossing, may be out  by one sample
        _test_.assert_isclose(S[::int(1./N)], s, "Sub-sampling by %g using a clock signal is wrong"%N, atol=1.00000001/F_S)
    
    S = np.random.randn(len(T))
    # TODO: I've commented out three lines down below, that make the tests fail but I'm not convinced it is what it should be.
#    S += np.sin(2*np.pi*0.2375*F_S*T) # This messes with power estimate when down-sampling with aliasing
      
    # Down sampling with aliasing (which preserves power)
    for N in [1,2,4,8]:
        t, s = sample(S, F_S, F_S/float(N), nyqzone=0) # 0 means no anti-aliasing filtering
        _test_.assert_isclose(T[::N], t, "Down-sampling by factor %d: time values must match"%N)
        _test_.assert_isclose(S[::N], s, "Down-sampling by factor %d: signal values must match"%N, rtol=1e-9)
        _test_.assert_isclose(signal_power(S), signal_power(s), "Down-sampling by factor %d: signal power must match"%N, rtol=0.01)
    # Down sampling with anti-aliasing -- this is more lossy, and wide band power is decreased
    for N in [1,2,4,8]:
        t, s = sample(S, F_S, F_S/float(N), nyqzone=1)
        _test_.assert_isclose(T[::N], t, "Under-sampling Nyq zone 1 by factor %d: time values must match"%N)
#         _test_.assert_isclose(S[::N], s, "Under-sampling Nyq zone 1 by factor %d: signal values must match < 10%%"%N, rtol=0.1)
        _test_.assert_isclose(signal_power(S), N*signal_power(s), "Under-sampling Nyq zone 1 by factor %d: signal power must match < 3%%"%N, rtol=0.03)
  
    # Up sampling
    for NYQ in [1,2]: # Must re-sample to >> max(NYQ)*F_S, which is why N is chosen as below
        for N in [1,3,4,5]: # Up-sampling by aliasing & interpolation is also not precise, but power is scaled automatically
            t, s = sample(S, F_S, F_S*N, nyqzone=NYQ)
            _test_.assert_isclose(T, t[::N], "Over-sampling Nyq zone %d by factor %d: time values must match"%(NYQ,N))
#             _test_.assert_isclose(S, s[::N], "Over-sampling Nyq zone %d by factor %d: signal values must match < 10%%"%(NYQ,N), rtol=0.1)
            _test_.assert_isclose(signal_power(S),  signal_power(s), "Over-sampling Nyq zone %d by factor %d: signal power must match < 10%%"%(NYQ,N), rtol=0.1)
    
    ## Sampling with jitter
    jit_noise = lambda j_rms, f_c, amp_c: 0.5*(amp_c*2*np.pi*f_c*j_rms)**2 # Approximation valid for j_rms*f_C<<0.5
    T = np.arange(2**21)*1/F_S
    S = scale_signal(np.random.randn(len(T)), 1) # Level is arbitrary; this choice makes it easy to estimate the equivalent Noise Figure
    NFFT = len(S)//128 # PSD averaging is useful, but trades for resolution
    window = scipy.signal.windows.blackmanharris(NFFT, sym=False) # Sidelobes < -100 dB
    # np.median is used below as a robust way to estimate the "noise floor" without influence of the CW or phase noise
    # Jitter with just noise
    print(">> Jitter with just noise")
    pylab.figure()
    P0 = np.median(pylab.psd(S, NFFT, Fs=F_S, scale_by_freq=True, window=window, label="REFERENCE", linestyle='-.')[0])
    for j_rms in [0,0.01,0.1,0.5,1]: # Multiple of 1/F_S
        s = sample(S, F_S,F_S, j_rms/F_S)[1]
        P = np.median(pylab.psd(s, NFFT, Fs=F_S, scale_by_freq=True, window=window, label="RMS %g/F_S"%j_rms)[0])
        print(P/P0, 10*np.log10(P))
    pylab.legend()
    # Now also with a tone added
    pylab.figure()
    for f_C in [0.13179*F_S,0.2375*F_S]: # < F_S/2. These don't seem to cause significant numerical artifacts
        C = scale_signal(np.sin(2*np.pi*f_C*T), signal_power(S)*1e6)
        P0 = np.median(pylab.psd(S+C, NFFT, Fs=F_S, scale_by_freq=True, window=window, label="REFERENCE", linestyle='-.')[0])
        print(">> Jitter with noise+CW")
        for j_rms in [0.01,0.1,1]: # Multiple of 1/F_S
            s = sample(S+C, F_S,F_S, j_rms/F_S)[1]
            P = np.median(pylab.psd(s, NFFT, Fs=F_S, scale_by_freq=True, window=window, label="RMS %g/f_C"%(j_rms*f_C))[0])
            E = jit_noise(j_rms/F_S,f_C,np.max(C))
            if (j_rms*f_C/F_S<0.25): # The approximation is only valid for <<0.5!
                _test_.assert_isclose(E, P, "Jitter floor doesn't scale as expected for RMS %g/f_C"%(j_rms*f_C/F_S), rtol=1)
            print(P/P0, 10*np.log10(P), 10*np.log10(E), "\t(%g/f_C)"%(j_rms*f_C/F_S))
    pylab.legend()
    
    # Jitter scales with tone amplitude
    print(">> Jitter with CW amplitude scaling")
    f_C = 0.2375*F_S # < F_S/2. This doesn't seem to cause significant numerical artifacts
    C = scale_signal(np.sin(2*np.pi*f_C*T), signal_power(S)*1e6)
    j_rms = 0.1 # Multiple of 1/f_C, and <<0.5/f_C
    pylab.figure()
    P0 = np.median(pylab.psd(S, NFFT, Fs=F_S, scale_by_freq=True, window=window, label="REFERENCE", linestyle='-.')[0])
    for a in [0.01,0.1,1,10,100]:
        s = sample(S+a*C, F_S,F_S, j_rms/f_C)[1]
        P = np.median(pylab.psd(s, NFFT, Fs=F_S, scale_by_freq=True, window=window, label="Amplitude x %g"%a)[0])
        E = jit_noise(j_rms/f_C,f_C,a*np.max(C))
        _test_.assert_isclose(E, P, "Jitter floor doesn't scale as expected for amplitude x %g"%a, rtol=1)
        print(P/P0, 10*np.log10(P), 10*np.log10(E), "\t(%g/f_C)"%(j_rms))
    pylab.legend()

    # Jitter scales with RMS*fC
    pylab.figure()
    for f_C in [0.13179*F_S,0.2375*F_S]: # < F_S/2. These don't seem to cause significant numerical artifacts
        C = scale_signal(np.sin(2*np.pi*f_C*T), signal_power(S)*1e6)
        P0 = np.median(pylab.psd(S+C, NFFT, Fs=F_S, scale_by_freq=True, window=window, label="REFERENCE", linestyle='-.')[0])
        print(">> Jitter with RMS scaling")
        for j_rms in [0.05,0.1,0.2,0.4,0.8,1]: # Multiple of 1/f_C. Curious, from >= 0.4 the noise floor stabilizes?
            s = sample(S+C, F_S,F_S, j_rms/f_C)[1]
            P = np.median(pylab.psd(s, NFFT, Fs=F_S, scale_by_freq=True, window=window, label="RMS %g/f_C"%j_rms)[0])
            E = jit_noise(j_rms/f_C,f_C,np.max(C))
            if (j_rms<0.25): # The approximation is only valid for <<0.5/f_C
                _test_.assert_isclose(E, P, "Jitter floor doesn't scale as expected for RMS %g/f_C"%j_rms, rtol=1)
            print(P/P0, 10*np.log10(P), 10*np.log10(E), "\t(%g/f_C)"%(j_rms))
    pylab.legend()
    

@_test_()
def _test_resample(_test_):
    # TODO: currently a visual inspection, can it be automated?
    f_s = 1e9
    samplerate = 6*f_s # Must be >> f_s to be able to faithfully recover 2nd Nyquist zone
    t = np.arange(0,2**14)*1/f_s
    s = 0.01*np.random.randn(len(t)) + np.sum([np.sin(2*np.pi*F*t) for F in [10e6,25e6,45e6,55e6]],axis=0)
    pylab.figure()
    pylab.psd(s, len(s), f_s, label='s', alpha=0.8)
    r0 = sample(s, f_s, samplerate, nyqzone=0)[1]
    pylab.psd(r0, len(r0), samplerate, label='r0', alpha=0.8)
    r1 = sample(s, f_s, samplerate, nyqzone=1)[1]
    pylab.psd(r1, len(r1), samplerate, label='r1', alpha=0.8)
    r2 = sample(s, f_s, samplerate, nyqzone=2)[1]
    pylab.psd(r2, len(r2), samplerate, label='r2', alpha=0.8)
    r3 = sample(s, f_s, samplerate, nyqzone=3)[1]
    pylab.psd(r3, len(r3), samplerate, label='r3', alpha=0.8)
    r4 = sample(s, f_s, samplerate, nyqzone=4)[1]
    pylab.psd(r4, len(r4), samplerate, label='r4', alpha=0.8)
    r5 = sample(s, f_s, samplerate, nyqzone=5)[1]
    pylab.psd(r5, len(r5), samplerate, label='r5', alpha=0.8)
    pylab.legend(loc='best').get_frame().set_alpha(0.7); pylab.grid(True);
    pylab.title("IF THIS DOES NOT LOOK RIGHT, THIS TEST FAILS")


if __name__ == "__main__":
    import pylab
    _test_filters_()
    _test_add_noise()
    _test_amplify()
    _test_amplifier_()
    _test_quantize_ideal_()
    _test_quantize_()
    _test_sample()
    _test_resample()
    _test_attenuator_()
    _test_TransferFunction()
    _test_coax_()
    pylab.show()
