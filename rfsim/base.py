"""
    Copyright @ 2019 SARAO (http://www.ska.ac.za). All rights reserved.
    
    1) Constants that should be used consistently throughout this module.
    2) The definitive power scale definition as should be used consistently throughout this module.
    
    @author: aph@ska.ac.za
"""
import numpy as np
import scipy.interpolate as spi
import scipy.signal as sig
import traceback
try:
    import mkl_fft._numpy_fft as np_fft
except:
    np_fft = np.fft


kB = 1.38e-23 # Boltzmann's constant
MHz = 1e6 # Convenient constants
Hz = 1e-6*MHz
microsec = 1e-6


class _test_(object):
    """ Yet another unit test framework """
    def __init__(self, fixed_random=True):
        """@param fixed_random: True to seed np.random to generate the same test signals, facilitates isolating changes in behaviour (default True)"""
        self.fixed_random = fixed_random
        self.reset()
    
    def reset(self):
        self.n_passed = 0
        self.n_failed = 0
        
    def __call__(self, function): # Test decorator
        def wrapper(*args, **kwargs):
            trace = traceback.format_list([[function.__code__.co_filename, function.__code__.co_firstlineno, function.__name__, None]])[0]
            print("ENTERING TEST: %s"%trace.rstrip())
            rstate = np.random.mtrand.get_state()
            if self.fixed_random:
                np.random.seed(1)
            try:
                return function(self, *args, **kwargs)
            except Exception as e:
                print("  FATAL ERROR: %s %s"%(e,traceback.format_tb(e.__traceback__)))
            finally:
                np.random.mtrand.set_state(rstate)
                print("  PASSED: %d"%(self.n_passed))
                print("  FAILED: %d\n"%(self.n_failed))
                self.reset()
        return wrapper
    
    def fail(self, message):
        self.__fail__(message) # Add depth to the traceback stack
    
    def __fail__(self, message, nest=1):
        self.n_failed += 1
        trace = traceback.extract_stack()[-2-nest] # -1 = here, -2 = where this was called from in _test_, -3 == original call to _test_
        trace = [t for t in trace[:3]]+[None] # Delete the code string, which is at #4
        trace = traceback.format_list([trace])[0].splitlines()
        comment = "#"+trace[1].split("#")[1] if ("#" in trace[1]) else "" # Any comment against the code that generates the failure
        print("FAILED: %s\n  %s\t\t%s"%(trace[0], message, comment)) 
    
    def assert_is(self, truth, message, **kwargs):
        if not truth:
            self.__fail__(message, **kwargs)
        else:
            self.n_passed += 1
    
    def assert_isclose(self, expect, query, message, atol=1e-26, rtol=0, **kwargs):
        expect, query = np.atleast_1d(expect), np.atleast_1d(query)
        OK = np.isclose(query, expect, rtol, atol, equal_nan=True)
        diff = query-expect
        rdiff = diff[expect!=0]/expect[expect!=0]
        TOL = "(atol %g, rtol %g)"%(np.max(np.abs(diff)), np.max(np.abs(rdiff)) if (len(rdiff)>0) else np.nan)
        _str_ = lambda x: str(x) if (len(x) < 100) else "\n%s ... %s"%(x[:30], x[-30:])
        self.assert_is(np.all(OK), "%s. Expected %s, got %s, difference %s%s."%(message,_str_(expect),_str_(query),_str_(diff),TOL), nest=kwargs.get('nest',1)+1)
    
    def assert_isequal(self, expect, query, message, **kwargs):
        self.assert_isclose(expect, query, message, atol=0, rtol=0, nest=kwargs.get('nest',1)+1)
    

class circular_buffer(object):
    """ A circular generator to yield the next element in an infinite loop (or up to maxlen). """
    def __init__(self, elements, maxlen=-1):
        self.elements = elements
        self.head = 0
        self.tail = maxlen
    
    def __iter__(self):
        return self
    
    def __next__(self):
        if (self.head == self.tail):
            raise StopIteration
        try:
            return self.elements[self.head % len(self.elements)]
        finally:
            self.head += 1
    
    def clip(self, maxlen):
        """ @return: the current buffer, with length adjusted """
        self.tail = self.head + maxlen
        return self


def sum_dB(P): # This == mean_dB(P) + 10*log10(len(P))
    return 10*np.log10(np.sum(10**(P/10.)))

def mean_dB(P): # This == RMS(v)
    return 10*np.log10(np.mean(10**(P/10.)))


def signal_power(signal, Z0=1):
    """ Uses Z0 to relate amplitude and power.
        @param signal: time domain input signal series.
        @param Z0: the impedance relating amplitude and power (default 1, matched to signals module)
        @return: the total integrated power of the signal [W] (linear scale)
    """
    return np.std(signal)**2/Z0

@_test_()
def _test_signal_power_(_test_):
    # Test that std dev and PSD methods give equivalent results
    def alt_signal_power(signal): # This is EQUIVALENT to but SLOWER than above
        PSD = np.abs( np_fft.fft(signal) / len(signal) )**2 # PSD [linear/point]
        return np.sum(PSD.real) # Imaginary part of PSD of real-values signal is guaranteed to be << 0
    
    for s in [np.random.randn(1003),
              np.sin(2*np.pi* 10 * np.arange(0,1003,0.01)),
              np.sin(2*np.pi* 10 * np.arange(0,1003,0.01))+4*np.cos(2*np.pi* 3 * np.arange(0,1003,0.01))]:
        # With default sample rate
        P = signal_power(s)
        P_alt = alt_signal_power(s)
        _test_.assert_isclose(P, P_alt, "Equivalent power measurement test", rtol=1e-2)
    
    
    # Scaling tests with white noise
    s = np.random.randn(1003)
    s /= np.std(s) # Power == 1
    # Test impedance scale
    for Z0 in [1, 50, 100]:
        _test_.assert_isclose(1**2 / Z0, signal_power(s, Z0=Z0), "RMS power for Gaussian Noise @ Z0=%g"%Z0, rtol=1e-9)
        
    # Tests with pure tone
    s = 1*np.sin(2*np.pi* 10 * np.arange(0,1003,0.01)) # Power == 1/2
    # Test impedance scale
    for Z0 in [1, 50, 100]:
        _test_.assert_isclose((1/2**.5)**2 / Z0, signal_power(s, Z0=Z0), "RMS power for CW @ Z0=%g"%Z0, rtol=1e-9)
    

def scale_signal(signal, P_signal, Z0=1):
    """ Scales the amplitude of a signal for the specified power (w.r.t Z0).
        @param signal: time domain signal series.
        @param P_signal: the total integrated power in the particular component [W] (linear scale).
        @param Z0: the impedance relating amplitude and power (default 1, matched to signals module)
        @return: scaled copy of signal.
    """
    P_signal0 = signal_power(signal, Z0)
    return signal*(P_signal/P_signal0)**.5

@_test_()
def _test_scale_signal_(_test_):
    for i,s in enumerate([np.random.randn(1003),
                          np.sin(2*np.pi* 10 * np.arange(0,1003,0.01)),
                          np.sin(2*np.pi* 10 * np.arange(0,1003,0.01))+4*np.cos(2*np.pi* 3 * np.arange(0,1003,0.01))]):
        
        S1, S2, S10 = scale_signal(s, 1), scale_signal(s, 2), scale_signal(s, 10)
        P1 = signal_power(S1)
        _test_.assert_isclose(signal_power(S2), 2*P1, "2x power test on s_%d"%i, rtol=1e-9)
        _test_.assert_isclose(signal_power(S10), 10*P1, "10x power test on s_%d"%i, rtol=1e-9)

        S2_1, S10_1 = scale_signal(S2, 1), scale_signal(S10, 1)
        _test_.assert_isclose(P1, signal_power(S2_1), "Scaling up & down by x2 test on s_%d"%i, rtol=1e-9)
        _test_.assert_isclose(P1, signal_power(S10_1), "Scaling up & down by x10 test on s_%d"%i, rtol=1e-9)

    s = np.random.randn(1003)
    s /= np.std(s)
    P = signal_power(s, Z0=1)
    for Z0 in [1, 50, 100]:
        S = scale_signal(s, P, Z0=Z0)
        _test_.assert_isclose(P, signal_power(S, Z0=Z0), "Power scaling for Gaussian Noise @ Z0=%g"%Z0, rtol=1e-9)
    
    s = np.sin(2*np.pi* 10 * np.arange(0,1003,0.01))
    P = signal_power(s, Z0=1)
    for Z0 in [1, 50, 100]:
        S = scale_signal(s, P, Z0=Z0)
        _test_.assert_isclose(P, signal_power(S, Z0=Z0), "Power scaling for CW @ Z0=%g"%Z0)


def upsample(signal, N, axis=0):
    """ Upsamples signal by a factor N, by inserting N-1 zeroes in the series. The output is not
        scaled so that the total average power is decreased by factor N.
        The outcome is of course aliased images in the respective Nyquist bands. This is different
        from scipy.resample, which is hard coded for Nyquist zone 1.
        @param N: the signal length will be increased from 1 to N.
        @param axis: the axis to re-sample along (default 0)
        @return: the up-sampled signal """ 
    if (N > 1):
        # First add the necessary zeroes along a new dimension (new axis appended)
        newsignal = np.dstack([signal]+[np.zeros_like(signal)]*(N-1))
        # Then roll the axis to be re-sampled so that it is the second from last
        newsignal = np.rollaxis(newsignal, axis, -1)
        # Now reshape the array to collapse the last two axes
        newshape = list(signal.shape)
        newshape[axis] = newshape[-1]
        newshape[-1] = -1
        newsignal = newsignal.reshape(newshape)
        # Now roll the axis back to where it started from
        newsignal = np.rollaxis(newsignal, -1, axis)
        return newsignal
    else:
        return signal

@_test_()
def _test_upsample(_test_): # TODO: convert from visual inspection to automated evaluation
    x = np.arange(1,10)
    print(x)
    print(upsample(x,1))
    print(upsample(x,2))
    print(upsample(x,3))
    x = np.meshgrid(np.arange(1,10),np.arange(-5,5))[0]
    print(x.shape,x)
    print(upsample(x,1, axis=0))
    print(upsample(x,2, axis=0))
    print(upsample(x,2, axis=1))
    print(upsample(x,2))
    print(upsample(x,3))


def interp(x_new, x, y, kind='spline'):
    """ Interpolates the data (real & complex separately).
    
        When sub-sampling by decimation interpolation, high frequency components remain as aliased
        images (and power is conserved). To address this, low-pass filtering must be applied BEFORE
        decimation interpolation.
        
        Up-sampling by interpolation is equivalent to up-sampling by inserting zeroes, followed by
        low pass filtering. This provides attenuation of repeat images above first Nyquist zone and
        implies higher than To rather isolate
    
        Linear interpolation effectively filters, leaving a sinc^2 response outside the original band.
        The default interpolation function fits a cubic spline, which suppresses repeat images by > 50dB.
        'pchip' image suppression is mid-way between 'linear' and the cubic 'spline'.
        @param x, y: the existing (one-dimensional) data to interpolate from.
        @param kind: 'linear', 'pchip', 'spline' (default 'spline').
        @return: y_new, a series of complex values if y was complex, otherwise real values.
    """
    def _interp(x_new, x, y):
        if (kind == 'linear'):
            return np.interp(x_new, x, y)
        elif (kind == 'pchip'):
            return spi.pchip(x, y)(x_new)
        elif (kind == 'spline'):
            d_spline = spi.splrep(x, y, k=min([3,len(x)-1]))
            return spi.splev(np.asarray(x_new), d_spline, der=0)
        else: # TODO: supports {'zero', 'nearest', 'slinear', 'quadratic', 'cubic'}, but seems no better than above, and poorly implemented?
            return spi.interp1d(x, y, kind, bounds_error=False, fill_value=0)(x_new)
    
    if (len(x_new)==len(x) and np.allclose(x_new,x, 0, (x_new[1]-x_new[0])*1e-9)):
        return np.copy(y)
    elif (np.iscomplexobj(y)):
        return _interp(x_new, x, y.real) + 1j*_interp(x_new, x, y.imag)
    else:
        return _interp(x_new, x, y)


def cossum_win(M, sym, coeffs):
    """Implements a cosine-summed window. See e.g. F. J. Harris, 1978.
       @param M: number of samples
       @param sym: True for symmetric (e.g. for filter design), False for FFT symmetry.
       @parma coeffs: list of real coefficients for cosine series
       @return: the values for the window
    """
    if M < 1:
        return np.array([])
    if M == 1:
        return np.ones(1, 'd')
    odd = M % 2
    if not sym and not odd:
        M = M + 1
    n = np.arange(0, M)
    fac = n * 2 * np.pi / (M - 1.0)
    w = np.sum([(-1)**k*a_k*np.cos(k*fac) for k,a_k in enumerate(coeffs)], axis=0)
    if not sym and not odd:
        w = w[:-1]
    return w

#Some coefficients from B. Henning <https://www.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/46092/versions/3/previews/coswin.m/index.html>
def blackman_exact(M, sym=False): return cossum_win(M, sym, [7938/18608., 9240/18608., 1430/18608.])
def blackmannuttall(M, sym=False): return cossum_win(M, sym, [0.4243800934609435, 0.4973406350967378, 0.07827927144231873]) # 3 Term Cosine (Blackman-Nutall), 71.482 dB, NBW 1.70371 bins, 7.44490 dB gain
def blackmanharris5(M, sym=False): return cossum_win(M, sym, [0.3232153788877343, 0.4714921439576260, 0.1755341299601972, 2.849699010614994e-2, 1.261357088292677e-3]) # 5 Term Cosine, 125.427 dB, NBW 2.21535 bins, 9.81016 dB gain
def blackmanharris6(M, sym=False): return cossum_win(M, sym, [0.2935578950102797, 0.4519357723474506, 0.2014164714263962, 0.04792610922105837, 0.005026196426859393, 0.0001375555679558877]) # 6 Term Cosine, 153.566 dB, NBW 2.43390 bins, 10.64612 dB gain
def cosine7(M, sym=False): return cossum_win(M, sym, [2.712203605850388e-1, 4.334446123274422e-1, 2.180041228929303e-1, 6.578534329560609e-2, 1.076186730534183e-2, 7.700127105808265e-4, 1.368088305992921e-5]) # 7 Term Cosine, 180.468 dB, NBW 2.63025 bins, 11.33355 dB gain
def cosine8(M, sym=False): return cossum_win(M, sym, [2.533176817029088e-1, 4.163269305810218e-1, 2.288396213719708e-1, 8.157508425925879e-2, 1.773592450349622e-2, 2.096702749032688e-3, 1.067741302205525e-4, 1.280702090361482e-6]) # 8 Term Cosine, 207.512 dB, NBW 2.81292 bins, 11.92669 dB gain
def cosine9(M, sym=False): return cossum_win(M, sym, [2.384331152777942e-1, 4.005545348643820e-1, 2.358242530472107e-1, 9.527918858383112e-2, 2.537395516617152e-2, 4.152432907505835e-3, 3.685604163298180e-4, 1.384355593917030e-5, 1.161808358932861e-7]) # 9 Term Cosine, 234.734 dB, NBW 2.98588 bins, 12.45267 dB gain
def cosine10(M, sym=False): return cossum_win(M, sym, [2.257345387130214e-1, 3.860122949150963e-1, 2.401294214106057e-1, 1.070542338664613e-1, 3.325916184016952e-2, 6.873374952321475e-3, 8.751673238035159e-4, 6.008598932721187e-5, 1.710716472110202e-6, 1.027272130265191e-8]) # 10 Term Cosine, 262.871 dB, NBW 3.15168 bins, 12.92804 dB gain
def cosine11(M, sym=False): return cossum_win(M, sym, [2.151527506679809e-1, 3.731348357785249e-1, 2.424243358446660e-1, 1.166907592689211e-1, 4.077422105878731e-2, 1.000904500852923e-2, 1.639806917362033e-3, 1.651660820997142e-4, 8.884663168541479e-6, 1.938617116029048e-7, 8.482485599330470e-10]) # 11 Term Cosine, 289.635 dB, NBW 3.30480 bins, 13.34506 dB gain


def _nfft_(x, out_convert=None):
    """ An FFT implementation providing access to the output of the butterfly stages.
        DIT is implemented using a recursive algorithm, as per the following references:
          [1] https://rosettacode.org/wiki/Fast_Fourier_transform
    
        @param x: the data series to convert. NB: length must be a power of 2!
        @param out_convert: if provided must be 'lambda A,stage: convert(A)' applied at
                            output of each stage (default None). May be a list of functions.
                            'stage' = -M..0 for first..last DIT stage.
        @return: a result comparable to numpy.fft.fft()
    """
    assert (np.log2(len(x)) % 1 == 0), "Data series length is %d, must be a power of 2!"%len(x)
    out_convert = [out_convert] if callable(out_convert) else out_convert
    
    X = [xk for xk in x] # Recursive FFT using in-place modification of lists to speed up
    def fft_rec(x, r=0): # r: 0..-M for outer..innermost recursion shell
        n = len(x)
        if (n == 1): # This is the input value of the first stage
            return x
    
        even = fft_rec(x[::2], r-1)
        odd = fft_rec(x[1::2], r-1)
    
        w = _nfft_twiddle_(n)
        
        m = n//2
        for k in range(m):
            t = w[k] * odd[k]
            x[k] = even[k] + t
            x[m + k] = even[k] - t
        
        if out_convert is not None:
            for oc in out_convert:
                x = oc(x, r) # Stage 0..N is == np.log2(n)-1, but it's faster to use == r..0
        return x
    X = fft_rec(X)
    return np.asarray(X)

_nfft_twiddles_ = {}
def _nfft_twiddle_(N):
    """Generate the twiddle factors for _nfft_() """
    if (N not in _nfft_twiddles_.keys()):
        _nfft_twiddles_[N] = [np.exp(-2j*np.pi*k/N) for k in range(N//2)]
    return _nfft_twiddles_[N]

def nfft(x, n=None, axis=-1, padding=('constant',0), out_convert=None):
    """ An FFT implementation providing access to the output of the butterfly stages.
        DIT is implemented using a recursive algorithm, as per the following references:
          [1] https://rosettacode.org/wiki/Fast_Fourier_transform
    
        @param x: the data series to convert.
        @param padding: behaviour when padding the data series (default ('constant',0))
        @param out_convert: if provided must be 'lambda A,stage: convert(A)' applied at
                            output of each stage (default None). May be a list of functions.
                            'stage' = -M..0 for first..last DIT stage.
        @return: a result comparable to numpy.fft.fft(norm=None)
    """
    # This makes _nfft_() treat 'n' & 'axis' like numpy.fft.fft
    # If n is not 2^N then pads the data to next higher 2^N and resamples the output to n
    # padding='wrap' and 'symmetric' may yield better results than other choices?
    N = np.shape(x)[axis]
    n = n if n else N # Number of input samples requested to be processed
    o = n # Number of points required to be in output
    if (n < N): # Use only the first 'n' samples along the desired axis
        x = np.take(x, range(n), axis=axis)
        X = nfft(x, n, axis=axis, out_convert=out_convert)
        return X
    # Ensure that nfft() processes 2^N samples, pad if necessary
    n = n if (np.log2(n)%1 == 0) else 2**int(np.log2(n)+0.5) # Ensure length is 2^n
    if (n > N): # Pad then transform then resample output to match requested
        pad = [(0,0)]*len(np.shape(x)); pad[axis] = ((n-N)//2,n-N-(n-N)//2) # Pad at both ends
        if (padding[0] == 'constant'):
            x = np.pad(x, pad, 'constant', constant_values=padding[1])
        else: # 'wrap', 'symmetric', 'edge' or 'reflect'
            x = np.pad(x, pad, padding)
        X = nfft(x, n, axis=axis, out_convert=out_convert)
        X = spi.interp1d(np.linspace(0,1,n), X, 'nearest', axis=axis)(np.linspace(0,1,o))
        return X
    
    if (axis != -1):
        x = np.swapaxes(x, axis, -1)
    xshape = np.shape(x)
    m = int(np.prod(xshape) / n)
    X = [_nfft_(x if len(xshape)==1 else x[k], out_convert=out_convert) for k in range(m)]
    X = np.reshape(X, xshape)
    if (axis != -1):
        X = np.swapaxes(X, axis, -1)
    return X

def nfft_shifter(shift_pattern):
    """ Make a converter to scale the output, if requested for a stage, by a factor 0.5 (float).
        @param shift_pattern: binary string pattern controlling shift right per stage, where
                              MSB & LSB correspond to first(-M) & last(0) stages. Pad above MSB.
        @return: a convert function for _nfft_()
    """
    shift_pattern = shift_pattern[::-1] # Flip so that ordering matches -stage = -{-M(first)..0(last)}
    def out_convert(A, stage):
        if (shift_pattern[-stage] == "1"): # -stage = -{-M(first)..0(last)}
            A = np.asarray(A)/2. # Virtual shift i.e. no immediate loss in fidelity
        return A
    return out_convert

def asint(intval, nbits, pt_in=0, pt_out=0, overflow='wrap', rounding='trunc'):
    """ 'Cast' a signed value to an integer with specified number of bits. Input and output
        values are aligned with respect to their fixed-point binary points.
        @param nbits: the number of bits to represent the output, including the sign bit.
        @param pt_in: the position of the binary point in the input value (default 0)
        @param pt_out: the position of the binary point in the output value (default 0)
        @param overflow: 'wrap' or 'saturate' (default 'wrap')
        @param rounding: 'trunc', '+/-inf' or 'even' (default 'trunc')
        @return: re-interpretation of intval
    """
    assert (pt_out <= nbits-1), "Number of bits in the fractional part can't exceed N-1!"
    
    intval = intval * 2**(pt_out-pt_in) # Adjust the number of fraction bits of the input prior to truncating, to not lose fractional parts
    if rounding=='trunc':
        intval = int(intval) if np.isscalar(intval) else np.asarray(intval, int)
    elif rounding=='+/-inf': 
        # Symmetric rounding away from zero, as described in Vivado Design 
        # Suite Reference Guide UG958 (v2018.3), page 30. Rounds x.5 fractions
        # (i.e. 'mid points') to the nearest integer away from zero.
        pos_midpts = np.fmod(intval,1)==0.5
        neg_midpts = np.fmod(intval,1)==-0.5
        intval_r = np.round(intval)
        intval_r[pos_midpts] = np.ceil(intval[pos_midpts]) 
        intval_r[neg_midpts] = np.floor(intval[neg_midpts])
        intval = int(intval_r) if np.isscalar(intval) else np.asarray(intval_r, int)
    elif rounding=='even':
        # 'Even' or 'Convergent' rounding, as described in Vivado Design Suite
        # Reference Guide UG958 (v2018.3), page 30. Rounds x.5 fractions (i.e. 
        # 'mid points') to the nearest even number. The numpy round() function 
        # follows this convention.
        intval = int(np.around(intval)) if np.isscalar(intval) else np.asarray(np.around(intval), int)
    
    # Isolate the desired bits & handle overflow
    signbit = 1 << (nbits - 1) # 100..0
    mask = (1 << nbits) - 1    # 111..1
    if (overflow == 'saturate'): # Clip so there's no wrapping in the step after this
        MIN, MAX = -signbit, signbit-1
        try: # Assume input is an array
            intval[intval>MAX] = MAX; intval[intval<MIN] = MIN
        except TypeError: # Otherwise it's a primitive
            intval = MAX if intval>MAX else (MIN if intval<MIN else intval)
    # 2's complement of X = flipdigits(X) + 1. This can be simplified as follows:
    #     flipdigits(X) = X - (0111...1) = X - (2*signbit-1)
    #     -(2*signbit-1) + 1 = -signbit
    # so: flipdigits(X) + 1 = X - signbit
    outval = ((intval & mask) ^ signbit) - signbit # Isolate wanted bits & flip sign carrying bit, then to 2's complement

    return outval

def ascint(complex_intval, *args, **kwargs):
    # asint() applied separately to the real & imaginary parts of complex values
    return asint(np.real(complex_intval),*args,**kwargs) + 1j*asint(np.imag(complex_intval),*args,**kwargs)


@_test_()
def _test_asint_(_test_):
    Na = 13 # Number of bits that carry information
    a = 2**Na-1 - np.random.randint(32) # A random number with no more than Na bits
    
    # Treat all numbers as fixed point with 0 fractional bits
    A = asint(a, Na+1) # Since a>0 the MSB at Na+1 is 0, no overflow
    _test_.assert_isequal(a, A, "Quantise to Na+1 bits must be perfect")
    # Reduce number of integer bits
    for M in range(1,6):
        A = (2**(Na+1+M)-1) - (2**(Na+1)-1) # 11...1(xM) 0...0(xNa+1)
        Ap = asint(a + A, Na+1, 0, 0)
        _test_.assert_isequal(a, Ap, "Wrong result when integer part is reduced by %d"%M)
    # Overflow
    As = asint(a, Na, overflow='saturate') # The MSB at Na is interpreted as sign bit, causes overflow
    _test_.assert_isequal(2**(Na-1)-1, As, "Saturated to wrong value")
    Aw = asint(a, Na, overflow='wrap') # The MSB at Na is interpreted as sign bit, causes overflow
    _test_.assert_is((Aw < 0) and (-Aw < a), "Wrapped to incorrect value")
    
    # Treat all numbers as fixed point with M fractional input & output bits
    for M in range(Na-4):
        Ap = asint(a, Na+1, M, M)
        _test_.assert_isequal(a, Ap, "When in_pt=out_pt=%d there must be no effect"%M)
    
    # Treat all numbers as fixed point with M fractional input bits
    for M in range(1,Na-4): # ALready tested 0,0
        Ap = asint(a, Na+1, M, 0)
        _test_.assert_isequal(a//2**M, Ap, "With in_pt=%d,out_pt=0 it must be just shifts!"%M)
        # Converse of the above, shift input in anticipation of change of point
        Ap = asint(a*2**M, Na+1, M, 0)
        _test_.assert_isequal(a, Ap, "With in_pt=%d,out_pt=0 it must be just shifts!"%M)
    
    # Treat all numbers as fixed point with M fractional output bits
    for M in range(1,6): # Already tested 0,0
        Ap = asint(a, Na+1+M, 0, M) # Grow to accommodate the moving point
        _test_.assert_isequal(a*2**M, Ap, "With in_pt=0,out_pt=%d & nbits growing to accommodate it must be just right shifts!"%M)
        # Increasing fractional output bits preserves floating point fractions
        Ap = asint(a+2**-M, Na+1+M, 0, M) # Add a floating point fraction that will scale to +1 in output
        _test_.assert_isequal(a*2**M+1, Ap, "Floating point fraction must be preserved when out_pt=%d"%M)
        
        As = asint(a, Na+1, 0, M, overflow='saturate') # Overflow, moving the point of the output increases the number of bits required to prevent overflow
        _test_.assert_isequal(2**Na-1, As, "With in_pt=0,out_pt=%d & no bit growth, it must saturate!"%M)

def nfft_int(nbits, pt_in=0, pt_out=0, overflow='wrap', rounding='+/-inf'):
    """ Make a converter to scale the output for each stage to an integer
        @param nbits: number of least significant bits to retain.
        @param pt_in: the position of the binary point in the input value (default 0)
        @param pt_out: the position of the binary point in the output value (default 0)
        @param overflow: 'wrap' or 'saturate' (default 'wrap')
        @return: a convert function for _nfft_()
    """
    def out_convert(A, stage):
        A = ascint(A, nbits, pt_in, pt_out, overflow, rounding)
        return A
    return out_convert

of_counters = {} # Counts per stage, where stage = -M(first)..0(last)
def nfft_overflowmonitor(MAX=0, dump=False):
    """ Make a converter to count overflows per stage, using global of_counters register!
        @param MAX: threshold value (+/-) that defines overflow.
        @param dump: True to report what's in the log (default False)
        @return: a convert function for _nfft_()
    """
    global of_counters
    if dump:
        print(["%d:%3d"%(k,v) for k,v in sorted(of_counters.items())])
        nfft_overflowmonitor.dumped = of_counters # Keep a reference till next time
    of_counters = {}
    def out_convert(A, stage):
        of = np.where(np.abs(A) > MAX)[0] # The values which overflow
        of_counters[stage] = of_counters.get(stage,0) + len(of)
        return A
    return out_convert

max_perstage = {} # Max value per stage, where stage = -M(first)..0(last)
def nfft_maxmonitor(dump=False, fixpt=0):
    """ Make a converter to track the maximum amplitude of the signal per stage, using global max_perstage register!
        @param dump: True to report what's in the log (default False)
        @param fixpt: position of the binary point to scale recorded values to integers (default 0).
        @return: a convert function for _nfft_()
    """
    global max_perstage
    if dump:
        nbits = lambda x: np.nan if (x == 0) else int(np.log2(x)+0.5)
        print(["%d:%3d Q%g.%d"%(k,v,nbits(v)-fixpt,fixpt) for k,v in sorted(max_perstage.items())])
        nfft_maxmonitor.dumped = max_perstage # Keep a reference till next time
    max_perstage = {}
    def out_convert(A, stage):
        maxval = np.max(np.abs(A))
        max_perstage[stage] = np.max([max_perstage.get(stage,0), maxval])
        return A
    return out_convert

@_test_()
def _test_nfft_(_test_):
    f_s = 4. # Sampling frequency
    f_t = [0.2, 0.5] # Test tone frequencies
    # Length 2^N inputs, 1-D array
    for N in [2**6, 2**10, 2**14]:
        ## Test of underlying _nfft_
        t = np.arange(N)*1/f_s
        a = np.cos(2*np.pi*f_t[0]*t) + 0.55*np.cos(2*np.pi*f_t[1]*t)
        #a += np.random.randn(len(t))
          
        An = _nfft_(a)
        A = np_fft.fft(a, norm=None)
        _test_.assert_isclose(np.max(np.abs(A)), np.max(np.abs(An)), "For N=%d, _nfft_ gain differs from numpy.fft.fft"%N, atol=0, rtol=1e-15)
        An /= np.max(np.abs(An))
        A /= np.max(np.abs(A))
        _test_.assert_isclose(np.abs(A), np.abs(An), "For N=%d, _nfft_ differs from numpy.fft.fft"%N, atol=1e-15, rtol=0)
        Ar = np_fft.rfft(a); Ar /= np.max(np.abs(Ar))
        _test_.assert_isclose(np.abs(Ar), np.abs(An[:len(Ar)]), "For N=%d, _nfft_ differs from numpy.fft.rfft"%N, atol=1e-15, rtol=0)
  
        ## Test of nfft slicing
        A = An
        An = nfft(a); An /= np.max(np.abs(An))
        _test_.assert_isclose(np.abs(A), np.abs(An), "For N=%d, nfft differs from _nfft_"%N, atol=1e-25, rtol=0)
        AN = nfft(a, N); AN /= np.max(np.abs(AN))
        _test_.assert_isclose(np.abs(An), np.abs(AN), "For N=%d, nfft(N) differs from _nfft_()"%N, atol=1e-25, rtol=0)
        # Taking reduced number of samples (still 2^M)
        A = np_fft.fft(a, N//2); A /= np.max(np.abs(A))
        AN = nfft(a, N//2); AN /= np.max(np.abs(AN))
        _test_.assert_isclose(np.abs(A), np.abs(AN), "For N=%d, nfft(N/2) differs from numpy.fft.fft(N/2)"%N, atol=1e-15, rtol=0)
    
    # Length 2^N inputs, 2-D array
    for N in [2**6, 2**14]:
        a = np.sin(2*np.pi*f_t[0]*np.arange(16*N)) + np.cos(2*np.pi*f_t[1]*np.arange(16*N))
        a = a.reshape((-1,N))
        # All 2^N samples
        for axis in [0,1,-1]:
            A = np_fft.fft(a, axis=axis); A /= np.max(np.abs(A))
            AN = nfft(a, axis=axis); AN /= np.max(np.abs(AN))
            _test_.assert_isclose(np.abs(A), np.abs(AN), "For N=%d, nfft(N/2,axis=%d) differs from numpy.fft.fft(N/2,axis=%d)"%(N,axis,axis), atol=1e-15, rtol=0)
        # Taking reduced number of samples (still 2^M)
        A = np_fft.fft(a, N//2, axis=1); A /= np.max(np.abs(A))
        AN = nfft(a, N//2, axis=1); AN /= np.max(np.abs(AN))
        _test_.assert_isclose(np.abs(A), np.abs(AN), "For N=%d, nfft(N/2,axis=1) differs from numpy.fft.fft(N/2,axis=1)"%N, atol=1e-15, rtol=0)

    # 2^N zero padded supersets of shorter inputs
    for N in [2**8]:
        t = np.arange(N//2)*1/f_s
        a = np.cos(2*np.pi*f_t[0]*t) + 0.55*np.cos(2*np.pi*f_t[1]*t)
         
        A = np_fft.fft(a, N); A /= np.max(np.abs(A))
        An = nfft(a, N); An /= np.max(np.abs(An))
        _test_.assert_isclose(np.abs(A), np.abs(An), "For N=%d>len(x), nfft(padding=default) differs from numpy.fft.fft"%N, atol=1e-15, rtol=0)
    
    # NOT length 2^N subsets of longer inputs. Currently FAILS
    for N in [500]:
        t = np.arange(2*N)*1/f_s
        a = np.cos(2*np.pi*f_t[0]*t) + 0.55*np.cos(2*np.pi*f_t[1]*t)
         
        A = np_fft.fft(a, N); A /= np.max(np.abs(A))
        An = nfft(a, N); An /= np.max(np.abs(An))
        _test_.assert_isclose(np.abs(A), np.abs(An), "For N=%d<len(x), nfft(padding=default) differs from numpy.fft.fft"%N, atol=1e-15, rtol=0)
    
    # NOT length 2^N inputs. Currently FAILS
    for N in [1000, 2000]:
        f_s = 4.
        t = np.arange(N)*1/f_s
        a = np.cos(2*np.pi*f_t[0]*t) + 0.55*np.cos(2*np.pi*f_t[1]*t)
         
        A = np_fft.fft(a); A /= np.max(np.abs(A))
        An = nfft(a); An /= np.max(np.abs(An)) # Try e.g. padding='wrap' - not a significant improvement?
        _test_.assert_isclose(np.abs(A), np.abs(An), "For N=%d, nfft(padding=default) differs from numpy.fft.fft"%N, atol=1e-15, rtol=0)
#     # Info plot, only for the last iteration
#     Anw = nfft(a, padding='wrap'); Anw /= np.max(np.abs(Anw))
#     plot_diffs(range(A.shape[-1]), [A,An,Anw], labels=['numpy fft', 'nfft(padding=default)', 'nfft(padding=wrap)'], show=True)
#     
# def plot_diffs(F,X, labels=None, show=False):
#     """ @param: F: frequency vector, X: list of FFT vectors """
#     M = '-.oxs^v'
#     labels = M if (labels is None) else labels
#     #X = [x/np.max(np.abs(X)) for x in X] # Reference all x relative to max(X) - "DC gain" differences scale error spectra
#     X = [x/np.max(np.abs(x)) for x in X] # Normalize each x relative to itself - "DC gain" differences disappear
#     import pylab
#     pylab.figure(); pylab.subplot(2,1,1)
#     for x,m,l in zip(X,M,labels):
#         pylab.plot(F, np.abs(x), m, label=l)
#     pylab.legend()
#     pylab.subplot(2,1,2, sharex=pylab.gca())
#     for x,m,l in zip(X[1:],M[1:],labels[1:]):
#         pylab.plot(F, np.abs(np.abs(X[0])-np.abs(x)), label=l)
#     pylab.legend()
#     if show: pylab.show()


class filterbank(object):
    """ A filterbank object converts a real signal series into complex, nonnegative frequency components.
        It is capable of not just returning the FFT coefficients, but also the frequency vector associated with it.
        @param fft_func: alternative FFT function to use, must be compatible with the default (default numpy.fft.rfft)
    """
    def __init__(self, fft_func=np_fft.rfft):
        self._fft_func_ = fft_func
        
    def fft(self, signal):
        """ @return (time,frequency)-shaped amplitude spectrum [complex amplitude/bin] for f >= 0 """
        raise Exception("This is an abstract class, not implemented")
    
    def __call__(self, signal, samplerate=0, nyqzone=-1, flipzone=True):
        """
            Generate, scale and flip the frequency axes as requested.
            @param samplerate: non-zero to override the filterbank's '.samplerate', if any is defined (default 0).
            @param nyqzone: Nyquist zone to translate frequencies to sampled domain (default -1 i.e. none).
            @param flipzone: True to flip the frequency axis for even Nyquist zones (nyqzone>=2) (default True).
            @return frequency [Hz] & (time,frequency)-shaped amplitude spectrum [complex amplitude/Hz]) for f >= 0
        """
        FV = self.fft(signal)
        samplerate = self.samplerate if (samplerate == 0) else samplerate
        FV *= (1/samplerate)**.5 # Normalize numpy FFT "/bin" to "/sqrt(Hz)"
        freq = np.arange(FV.shape[1])*samplerate/2./FV.shape[1]
        
        # Translate frequencies to display in specified Nyquist zone
        if (nyqzone == 1):
            pass
        elif (nyqzone == 2):
            freq = 2*np.max(freq) - freq
        if (nyqzone > 0 and nyqzone%2 == 0 and flipzone): # Flip along the frequency axis so that frequency is always increasing with list index
            freq = np.flip(freq,0)
            FV = np.flip(FV,1)
        
        return (freq, FV)


class pfb_filterbank(filterbank):
    """ @author Danny C. Price, Spectrometers and Polyphase Filterbanks in Radio Astronomy, 2016. arXiv 1607.03579
        Simple implementation of a polyphase filterbank.
        
        Copied, with minor adaptation, from https://github.com/telegraphic/pfb_introduction/blob/master/pfb.py
        
        @param n_taps: number of taps in the PFB
        @param n_chan: number of output channels = 1+n_chan//2
        @param window_fn: a function to generate FIR window coefficients 'lambda N: <N coefficients>' or a name (default "hamming")
        @param w_cutoff: 6dB cut-off frequency of the channel response, normalised to Nyquist frequency (default 1.0).
        @param fft_func: alternative FFT function to use, must be compatible with the default (default numpy.fft.rfft)
    """
    def __init__(self, n_taps, n_chan, window_fn="hamming", w_cutoff=1.0, fft_func=np_fft.rfft):
        filterbank.__init__(self, fft_func)
        self.M = n_taps
        self.P = n_chan
        self.window_fn = window_fn
        self.w_cutoff = w_cutoff
    
    def generate_fir_coeffs(self, method='price'):
        """ Generates PFB FIR coefficients.
        @param method: Choice of FIR design approach (default = 'price'). 
        @return: window * sinc as required for pfb_fir_frontend() """
        M, P, w_cutoff = self.M, self.P, self.w_cutoff
        if (isinstance(self.window_fn, str)):
            # Note that fftbins=False option must be specified to ensure symmetric
            # sampling of the window around zero.
            win_coeffs = sig.get_window(self.window_fn, M*P, fftbins=False)
        else:
            win_coeffs = self.window_fn(M*P)
            
        if method=='price':
            # Sinc function as in original method by D. Price.
            sinc = sig.firwin(M * P, cutoff=1.0/P*w_cutoff, window="boxcar")
        elif method=='casper':
            # Sinc as used in CASPER PFB generated by MATLAB function pfb_coeff_gen_calc.
            # This function has a slight error in that it doesn't sample sinc(x) 
            # symmetrically around zero, leading to small error in coefficient
            # values.    
            sinc = np.sinc(w_cutoff*((np.arange(0,M*P))/(P*1.0)-M/2))
        elif method=='symmetric':
            # Sinc function sampled symmetric around zero. 
            sinc = np.sinc(w_cutoff*((np.arange(0,M*P)+0.5)/(P*1.0)-M/2))        
         
        win_coeffs *= sinc
        
        # Price method normalises coefficients by processing gain. No normalisation is 
        # applied to other methods, in which the coefficients are sampled from window
        # functions with max amplitude of 1.0.
        if method=='price':
            pg = np.sum(np.abs(win_coeffs)**2)
            win_coeffs /= pg**.5 # Normalizing here (including sinc) closely matches the results obtained for a single tap PFB and a windowed DFT.
        
        return win_coeffs
    
    def pfb_fir_frontend(self, x, fir_coeffs):
        """ @return: shape(W, P) from 'x' broken into an integer number W of non-overlapping M*P segments combined over M-taps """
        M, P = self.M, self.P
        W = int(len(x)/(M*P))
        x = x[:W*M*P] # Trim the signal to an integer multiple of len(fir_coeffs), data may be discarded without notice
        x_p = x.reshape((W*M, P)).T
        h_p = fir_coeffs.reshape((M, P)).T
        x_summed = np.zeros((P, M * W - M + 1))
        for t in range(0, M*W-M+1):
            x_weighted = x_p[:, t:t+M] * h_p
            x_summed[:, t] = x_weighted.sum(axis=1)
        return x_summed.T
    
    def pfb_fft(self, x_fir, y_fir=None):
        """ @return: shape(W, P//2+1) linear complex amplitude [/bin], for each of W non-overlapping and consecutive time segments """
        P = self.P
        if y_fir is not None:
            # If two inputs are specified, call a biplex fft function.
            x_pfb, y_pfb = self._fft_func_(x_fir, y_fir, axis=1)
            return x_pfb[:,:P//2+1], y_pfb[:,:P//2+1]
        else:
            x_pfb = self._fft_func_(x_fir, n=P, axis=1)[:,:P//2+1] # fft_func can either be numpy rfft or numpy fft
            return x_pfb
    
    def fft(self, x):
        """ Equivalent to 'pfb_fft(pfb_fir_frontend(x, generate_fir_coeffs()))'
            @return: (time, frequency)-shaped amplitude spectrum [complex amplitude/bin], only freq >=0 Hz, like numpy.rfft
        """
        fir_coeffs = self.generate_fir_coeffs()
        x_fir = self.pfb_fir_frontend(x, fir_coeffs)
        x_pfb = self.pfb_fft(x_fir)
        return x_pfb
    
@_test_()
def _test_pfb_(_test_):
    def pfb_spectrometer(x, n_taps, n_chan, n_int, window_fn="hamming"):
        x_pfb = pfb_filterbank(n_taps, n_chan, window_fn)
        x_psd = np.abs(x_pfb.fft(x))**2
        
        # Trim array so we can do time integration
        x_psd = x_psd[:(x_psd.shape[0]//n_int)*n_int]
        
        # Integrate over time, by reshaping and summing over axis (efficient)
        x_psd = x_psd.reshape(x_psd.shape[0]//n_int, n_int, x_psd.shape[1]) # time x n_int x n_chan/2
        x_psd = x_psd.mean(axis=1) # time x n_chan/2
        
        return x_psd
    
    import pylab as plt
    M     = 1          # Number of taps
    P     = 1024       # Number of 'branches', also fft length
    W     = 1000       # Number of windows of length M*P in input time stream
    n_int = 1          # Number of time integrations on output data

    # Generate a test data steam
    samples = np.arange(M*P*W)
    noise   = np.random.random(M*P*W) 
    freq    = 1
    amp     = 1
    cw_signal = amp * np.sin(samples * freq) + amp*np.sin(samples*freq*1.01)
    data = noise + cw_signal
    
    # Basic data shape tests
    pfb = pfb_filterbank(n_taps=M, n_chan=P)
    X = pfb.fft(data)
    Y = np_fft.rfft(data, P)
    _test_.assert_isequal(Y.shape[-1], X.shape[-1], "PFB.fft() length differs from numpy.fft.rfft")
    _test_.assert_isequal((W,P//2+1), X.shape, "PFB.fft() result has the wrong shape!")
    f, X = pfb(data, samplerate=1)
    _test_.assert_isequal((W,P//2+1), X.shape, "PFB() result has the wrong shape!")
    
    # Visual inspection
    X_psd = pfb_spectrometer(data, n_taps=M, n_chan=P, n_int=n_int, window_fn="blackmanharris")
    plt.plot(10*np.log10(X_psd.mean(axis=0)))

    plt.figure()
    plt.imshow(10*np.log10(X_psd), aspect='auto')
    plt.colorbar()
    plt.xlabel("Channel")
    plt.ylabel("Time")
    
    plt.show()


class fft_filterbank(filterbank):
    def __init__(self, samplerate, f_resolution=-1, NT=-1, window=sig.windows.blackmanharris, correct_PG=True, fft_func=np_fft.rfft):
        """ Makes a one-sided filterbank function for use by PSD etc.
            @param samplerate: [Hz]
            @param f_resolution: if > 0 this prescribes the desired spectral resolution (trades off against time resolution) [Hz].
            @param NT: if > 0 this prescribes time resolution that there are NT time points (default -1).
            @param window: the windowing function as 'lambda len,sym:winvals' (default blackmanharris).
            @param correctPG: False to not scale the spectra to correct for processing gain of the filterbank (default True).
            @param fft_func: alternative FFT function to use, must be compatible with the default (default numpy.fft.rfft)
        """
        filterbank.__init__(self, fft_func)
        self.samplerate = samplerate
        self.f_resolution = f_resolution
        self.NT = int(NT)
        self.window = window
        self.correct_PG = correct_PG
        
    def _window_(self, fftlen):
        """ Creates the (periodic) window and takes care of processing gain correction as per correct_PG.
            Note that   numpy.fft.fft(signal*_window_)   will be correctly scaled to [/point]. Further scale
            transformations will be required e.g. to convert to amplitude density [/Hz] multiply by sqrt(1/f_s).
            @return: the window values
        """
        try: # Try to use periodic window (DFT-even) as recommended in [Harris 1976/78]
            win = self.window(fftlen, sym=False)
        except: # Doesn't use the "sym" keyword, so cannot enforce compliance
            win = self.window(fftlen)
        
        if (self.correct_PG):
            pg = np.sum(np.abs(win)**2) # Harris' Incoherent (power) Processing Gain (eq 14b), same applied in matplotlib.mlab._spectral_helper() 
        else:
            pg = np.sum(win)**2 # The Coherent (power) Processing Gain of [Harris 1976/78] eq 13 
            
        return win / pg**.5
    
    def fft(self, signal):
        """
            Computes the time series of single-sided FFT as subset of 2x the two-sided spectrum:
            If configured the window is automatically scaled to corrected for processing gain in which case the noise floor power will be correct. 
            @return (time,frequency)-shaped amplitude spectrum [complex amplitude/Hz] for f >= 0
        """
        if (self.NT > 0): # Explicit time resolution
            fftlen = (len(signal)//self.NT //2)*2  # Ensure it's at least an even number, or else FFT is extremely slow
        else: # Explicit frequency resolution
            fftlen = (int(self.samplerate/(self.f_resolution)) //2)*2  # Ensure it's at least an even number, or else FFT is extremely slow
        N_spectra = int(len(signal) / fftlen)
        
        win = self._window_(fftlen) # Normalized for processing gain if necessary
        
        # Generate time series of spectra
        V = np.reshape(signal[:N_spectra*fftlen],(-1,fftlen)) * win
        FV = self._fft_func_(V, axis=1, n=fftlen)
        FV = 2**.5 * FV[:,:fftlen//2]
        FV[:,0] /= 2**.5 # Half the DC bin
        
        return FV


@_test_()
def _test_fft_filterbank_(_test_):
    N = 2**10
    w = sig.windows
    # Confirm Coherent Processing Gain as employed in filterbank, against values tabulated in [Harris 1976/78]
    for window,pg in [(w.boxcar,1), (w.triang,0.5), (w.hamming,0.54), (w.blackman,0.42), (w.blackmanharris,0.36)]:
        win = window(N, sym=False)
        PG = np.sum(win) # Harris'eq 13, normalized below as stated for table in [Harris 1976/78]
        _test_.assert_isclose(pg, PG/float(N), "%s Harris' eq 13 PG differs more than 1%% from his tabulated value"%window, rtol=0.01)
        
        _FB = fft_filterbank(1e3, 1, window=window, correct_PG=False) # Coherent gain is the opposite of incoherent/noise gain
        _win = _FB._window_(N) # Returns _win = win/PG**.5  with PG a power gain factor
        PG = (win/_win) # PG corrected for by filterbank, as amplitude gain and normalized below as for Harris'table
        _test_.assert_isclose(pg, PG.mean()/float(N), "%s Filterbank's coherent PG differs more than 1%% from tabulated value"%window, rtol=0.01)
    

def im_freqs(f1, f2, order, f_ignore=None):
    """ Generates a set of possible frequencies of intermodulation products.
        @param f1,f2: two frequencies from which to compute possible intermodulation frequencies.
        @param f_ignore: [(f_min,f_max)..] identifying frequency ranges to ignore, excluding end points (default None).
        @param order: an integer to consider all combinations m+n=order.
        @return: an un-ordered set of all (m*f1+n*f2, m*f1-n*f2) where m+n=order and m,n>=0.
    """
    f_im = []
    f_ignore = f_ignore if (f_ignore is not None) else []
    def append_or_ignore(f):
        for (f_min,f_max) in f_ignore:
            if (f>f_min and f<f_max):
                return # Ignore
        f_im.append(f) # Append
    
    for I in range(0,order+1):
        append_or_ignore(abs(I*f1 + (order-I)*f2))
        append_or_ignore(abs(I*f1 - (order-I)*f2))
    return set(f_im)
    

def im_blame(f_fund, f_im, orders=[2,3], f_tol=0):
    """ Suggests a possible combination of frequencies and intermodulation order that could give rise
        to an observed intermodulation product.
        @param f_fund: list of fundamental frequencies
        @param f_im: the frequency of the intermodulation product
        @param orders: the orders of intermodulation to consider (default [2,3]).
        @param f_tol: max tolerance in f to still consider a match (default 0).
        @return: [(f1,f2,order,f_all_im)...] of possible candidates
    """
    f_fund = np.sort(f_fund)
    results = []
    for i in range(len(f_fund)):
        f1 = f_fund[i]
        for j in range(i+1,len(f_fund)):
            f2 = f_fund[j]
            for o in orders:
                f_trial = im_freqs(f1, f2, o)
                if (np.min(np.abs(np.asarray(list(f_trial))-f_im)) <= f_tol):
                    results.append([f1,f2,o,f_trial])
    return results

    
def clip_blame(f_fund, f_im, f_sample=None, f_tol=0):
    """ Suggests a possible combination of frequencies and intermodulation order that could give rise
        to an observed intermodulation product.
        @param f_fund: list of fundamental frequencies
        @param f_im: the frequency of the intermodulation product
        @param f_sample: the sampling frequency if the clipped signal is also sampled (default None)
        @param f_tol: max tolerance in f to still consider a match (default 0).
        @return: [(f1,f2,order,f_all_im)...] of possible candidates
    """
    f_fund = np.sort(f_fund)
    results = []
    for i in range(len(f_fund)):
        f1 = f_fund[i]
        for o in [3,5]: # Zero-mean CW signals generate harmonics at 3rd & 5th, rapidly falling off 
            f_trial = [f1*o] + ([] if f_sample is None else [(f_sample-f1)*o, (f_sample-f1)*(o+1)])
            if (np.min(np.abs(np.asarray(list(f_trial))-f_im)) <= f_tol):
                results.append([f1,o,f_trial])
    return results

    
if __name__ == "__main__":
    _test_signal_power_()
    _test_scale_signal_()
    _test_upsample()
    _test_asint_()
    _test_nfft_()
    _test_pfb_()
    _test_fft_filterbank_()