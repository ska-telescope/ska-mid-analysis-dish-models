"""
    Copyright @ 2019 SARAO (http://www.ska.ac.za). All rights reserved.
    
    Some special signals for simulations that don't quite belong in rfiuck.siggen
    
    @author: aph@ska.ac.za
"""
from __future__ import division
import numpy as np
from rfiuck.siggen import MHz, signal_power, scale_signal


def CHIRP_series(t_sample, f_start, f_stop, DT, P_dBm):
    """
        Generates a frequency chirp signal, with tone frequency ramping linearly from specified
        start to stop frequencies over the time span DT, then remains at f_stop for the duration.
        
        Note: the phase of the tone is varied as follows:
            phi = 2pi * (f_start + Delta_f/Delta_T*t) *t
        which results in the actual rate of change of frequency of the tone being 2*Delta_f/Delta_T
            omega = 2pi*f = d/dt (phi) = d/dt (2pi * (f_start + Delta_f/Delta_T*t) *t)
            i.e. f = f_start + 2*Delta_f/Delta_T*t
            i.e. d/dt (f) = 2*Delta_f/Delta_T
        Therefore, to agree with the expectation that d/dt (f) = (f_start-f_stop)/T, it follows that
             Delta_f/Delta_T = (f_start-f_stop)/T / 2
        
        @param t_sample: time series for which to generate the signal [sec].
        @param f_start, f_stop: frequencies [Hz].
        @param DT: the time interval over which the frequency changes after starting from f_start at t_sample[0] [sec].
        @param P_dBm: the total integrated power of the chirped series [dBm].
        @return: the chirp signal series.
    """
    f = f_start + 0.5*(f_stop-f_start)/DT * (t_sample-t_sample[0]) # Ramp
    f[t_sample-t_sample[0] > DT] = f_stop # Constant
    s = np.sin(2*np.pi*f*t_sample)
    return scale_signal(s,10**(P_dBm/10.)*1e-3)


def gaussian_series(t_sample, HPBW_MHz, f_c_MHz, Pr=None):
    """
        Generates a random series of Gaussian pulses. The Fourier relations are:
        F(omega) = 1/sqrt(2a)*e^(-omega^2/(4a))  <->  e^(-a*t^2) = f(t)
        with a = (pi*HPBW)^2/(4*ln(2))
        
        F(omega-omega_c) = 1/sqrt(2a)*e^(-(omega-omega_c)^2/(4a))  <->  f(t)*e^(j*omega_c*t)
        
        @param t_sample: time series for which to generate the signal [sec].
        @param HPBW_MHz, f_c_MHz: the centre frequency and half power bandwidth of the pulse [MHz].
        @param Pr: the probability of a pulse occurring in any 1/2HPBW time interval, or
                   None for a single pulse starting at t_sample=0 (default None)
        @return: the signal series.
    """
    a = (np.pi*HPBW_MHz*MHz)**2 / (4*np.log(2))
    omega_c = 2*np.pi*f_c_MHz*MHz
    dT = 1/(2*HPBW_MHz*MHz)
    # Determine which pulses to generate
    if (Pr):
        N = np.arange( int( (t_sample[-1]-t_sample[0])/dT )) # Indices of all possible pulses
        N = N[np.random.rand(len(N)) < Pr] # Indices selected with probability
    else:
        N = [0]
    # Generate the pulses
    samples = np.zeros(len(t_sample), np.complex64)
    for n in N:
        pulse = np.exp(-a * (t_sample-n*dT)**2) * np.exp(1j*omega_c*(t_sample-n*dT))
        samples += pulse
    
    return samples.real # The imaginary part is not negligible, but this works -- should it really be samples+conj(samples)?

def _CHIRPED_gaussian_series(t_sample, HPBW_MHz, f_start_MHz, f_stop_MHz, DT, P_dBm):
    """
        Generates a frequency chirp signal of Gaussian pulses with frequency ramping linearly from specified
        start to stop frequencies over the time span DT, then remains at f_stop for the duration.
        
        Note: the centre frequency of the pulses are varied as follows:
            f_c = f_start + Delta_f/Delta_T*t
            i.e. d/dt (f) = Delta_f/Delta_T
        Therefore, to agree with the expectation that d/dt (f) = (f_start-f_stop)/T, it follows that
             Delta_f/Delta_T = (f_start-f_stop)/T
        
        @param t_sample: time series for which to generate the signal [sec].
        @param f_start_MHz, f_stop_MHz: frequencies [MHz].
        @param DT: the time interval over which the frequency changes after starting from f_start at t_sample[0] [sec].
        @param P_dBm: the total integrated power of the chirped series [dBm].
        @return: the chirp signal series.
    """
    p = gaussian_series(t_sample, HPBW_MHz, f_start_MHz, Pr=1)
    chirp = CHIRP_series(t_sample, f_start_MHz*MHz, f_stop_MHz*MHz, DT, signal_power(p)*1000)
    chirped_p = p*chirp
    return scale_signal(chirped_p, 10**(P_dBm/10)*1e-3)

    
def CHIRPED_gaussian_series(t_sample, HPBW_MHz, f_start_MHz, f_stop_MHz, DT, P_dBm):
    """
        Generates a frequency chirp signal of Gaussian pulses with frequency ramping linearly from specified
        start to stop frequencies over the time span DT, then remains at f_stop for the duration.
        
        Note: the centre frequency of the pulses are varied as follows:
            f_c = f_start + Delta_f/Delta_T*t
            i.e. d/dt (f) = Delta_f/Delta_T
        Therefore, to agree with the expectation that d/dt (f) = (f_start-f_stop)/T, it follows that
             Delta_f/Delta_T = (f_start-f_stop)/T
        
        @param t_sample: time series for which to generate the signal [sec].
        @param f_start, f_stop: frequencies [Hz].
        @param DT: the time interval over which the frequency changes after starting from f_start at t_sample[0] [sec].
        @param P_dBm: the total integrated power of the chirped series [dBm].
        @return: the chirp signal series.
    """
    T_dwell = 6/(HPBW_MHz*MHz)
    Df_MHz = (f_stop_MHz-f_start_MHz)/DT * T_dwell
    a = (np.pi*HPBW_MHz*MHz)**2 / (4*np.log(2))
    N = np.arange( int( (t_sample[-1]-t_sample[0])/T_dwell )) # Indices of all possible pulses
    # Generate the pulses
    samples = np.zeros(len(t_sample), np.complex64)
    for n in N:
        t_n = n*T_dwell
        f_c = min([f_start_MHz*MHz + n*Df_MHz, f_stop_MHz*MHz]) # Ramp
        omega_c = 2*np.pi*f_c
        pulse = np.exp(-a * (t_sample-t_n)**2) * np.exp(1j*omega_c*(t_sample-t_n))
        samples += pulse
    
    chirped_p = samples.real # The imaginary part is not negligible, but this works -- should it really be samples+conj(samples)?
    return scale_signal(chirped_p, 10**(P_dBm/10)*1e-3)


def OFDM_series(t_sample, f_lte_MHz, K=64):
    """
    http://dspillustrations.com/pages/posts/misc/python-ofdm-example.html
        @param K: the number of OFDM subcarriers (default 64)
    """
    CP = K//4  # length of the cyclic prefix: 25% of the block
    P = 8 # number of pilot carriers per OFDM block
    pilotValue = 3+3j # The known value each pilot transmits
    
    allCarriers = np.arange(K)  # indices of all subcarriers ([0, 1, ... K-1])
    
    pilotCarriers = allCarriers[::K//P] # Pilots is every (K/P)th carrier.
    
    # For convenience of channel estimation, let's make the last carriers also be a pilot
    pilotCarriers = np.hstack([pilotCarriers, np.array([allCarriers[-1]])])
    P = P+1
    
    # data carriers are all remaining carriers
    dataCarriers = np.delete(allCarriers, pilotCarriers)
    
#     print ("allCarriers:   %s" % allCarriers)
#     print ("pilotCarriers: %s" % pilotCarriers)
#     print ("dataCarriers:  %s" % dataCarriers)
#     plt.plot(pilotCarriers, np.zeros_like(pilotCarriers), 'bo', label='pilot')
#     plt.plot(dataCarriers, np.zeros_like(dataCarriers), 'ro', label='data')    

    # 16-QAM constellation
    mu = 4 # bits per symbol (i.e. 16QAM)
    payloadBits_per_OFDM = len(dataCarriers)*mu  # number of payload bits per OFDM symbol
    
    mapping_table = {
        (0,0,0,0) : -3-3j,
        (0,0,0,1) : -3-1j,
        (0,0,1,0) : -3+3j,
        (0,0,1,1) : -3+1j,
        (0,1,0,0) : -1-3j,
        (0,1,0,1) : -1-1j,
        (0,1,1,0) : -1+3j,
        (0,1,1,1) : -1+1j,
        (1,0,0,0) :  3-3j,
        (1,0,0,1) :  3-1j,
        (1,0,1,0) :  3+3j,
        (1,0,1,1) :  3+1j,
        (1,1,0,0) :  1-3j,
        (1,1,0,1) :  1-1j,
        (1,1,1,0) :  1+3j,
        (1,1,1,1) :  1+1j
    }
#     for b3 in [0, 1]:
#         for b2 in [0, 1]:
#             for b1 in [0, 1]:
#                 for b0 in [0, 1]:
#                     B = (b3, b2, b1, b0)
#                     Q = mapping_table[B]
#                     plt.plot(Q.real, Q.imag, 'bo')
#                     plt.text(Q.real, Q.imag+0.2, "".join(str(x) for x in B), ha='center')    


    def generate_block():    
        # Random bit sequence
        bits = np.random.binomial(n=1, p=0.5, size=(payloadBits_per_OFDM, ))
    #     print ("Bits count: ", len(bits))
    #     print ("First 20 bits: ", bits[:20])
    #     print ("Mean of bits (should be around 0.5): ", np.mean(bits))
    
        # Serial-to-parallel
        def SP(bits):
            return bits.reshape((len(dataCarriers), mu))
        bits_SP = SP(bits)
    #     print ("First 5 bit groups")
    #     print (bits_SP[:5,:])
        
        # Map bits to symbols
        def Mapping(bits):
            return np.array([mapping_table[tuple(b)] for b in bits])
        QAM = Mapping(bits_SP)
    #     print ("First 5 QAM symbols and bits:")
    #     print (bits_SP[:5,:])
    #     print (QAM[:5])    
    
        # Construct the OFDM frame - data & carriers
        def OFDM_symbol(QAM_payload):
            symbol = np.zeros(K, dtype=complex) # the overall K subcarriers
            symbol[pilotCarriers] = pilotValue  # allocate the pilot subcarriers 
            symbol[dataCarriers] = QAM_payload  # allocate the pilot subcarriers
            return symbol
        OFDM_data = OFDM_symbol(QAM)
    #     print ("Number of OFDM carriers in frequency domain: ", len(OFDM_data))
        
        # Convert to time domain
        def IDFT(OFDM_data):
            return np.fft.ifft(OFDM_data)
        OFDM_time = IDFT(OFDM_data)
    #     print ("Number of OFDM samples in time-domain before CP: ", len(OFDM_time))    
    
        # Add cyclic prefix to the symbol
        def addCP(OFDM_time):
            cp = OFDM_time[-CP:]               # take the last CP samples ...
            return np.hstack([cp, OFDM_time])  # ... and add them to the beginning
        OFDM_withCP = addCP(OFDM_time)
    #     print ("Number of OFDM samples in time domain with CP: ", len(OFDM_withCP))
        return OFDM_withCP

    # APH innovating: modulate onto carrier
    OFDM_TX = np.exp(-1j*2*np.pi*f_lte_MHz*1e6*t_sample)
    OFDM_withCP = generate_block()
    N = len(OFDM_withCP)
    for n in range(int(len(t_sample)/N)):
        OFDM_TX[n*N:(n+1)*N] *= OFDM_withCP
        OFDM_withCP = generate_block()
    
#    # The wireless channel
#     channelResponse = np.array([1, 0, 0.3+0.3j])  # the impulse response of the wireless channel
#     H_exact = np.fft.fft(channelResponse, K)
#     plt.plot(allCarriers, abs(H_exact))
#    
#     SNRdb = 25  # signal to noise-ratio in dB at the receiver 
#
#     def channel(signal):
#         convolved = np.convolve(signal, channelResponse)
#         signal_power = np.mean(abs(convolved**2))
#         sigma2 = signal_power * 10**(-SNRdb/10)  # calculate noise power based on signal power and SNR
#         
#         print ("RX Signal power: %.4f. Noise power: %.4f" % (signal_power, sigma2))
#         
#         # Generate complex noise with given variance
#         noise = np.sqrt(sigma2/2) * (np.random.randn(*convolved.shape)+1j*np.random.randn(*convolved.shape))
#         return convolved + noise
#     OFDM_RX = channel(OFDM_TX)
#     plt.figure(figsize=(8,2))
#     plt.plot(abs(OFDM_TX), label='TX signal')
#     plt.plot(abs(OFDM_RX), label='RX signal')
#     plt.legend(fontsize=10)
#     plt.xlabel('Time'); plt.ylabel('$|x(t)|$');
#     plt.grid(True);

    return OFDM_TX
