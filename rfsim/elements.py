"""
    Copyright @ 2019 SARAO (http://www.ska.ac.za). All rights reserved.
    
    Framework for high level "elements" that transform signals along a signal path.

    @author: aph@ska.ac.za
"""
from __future__ import division
import numpy as np
import scipy as sp
import scipy.interpolate as spi

from rfsim.base import MHz, mean_dB, _test_
from rfsim.twoports import bpfilter, sample, quantize, quantizer_thresh, upsample
from rfsim.probes import _probe, NOISEFIGURE, GAIN, PSD, TIMESERIES, HISTOGRAM, P_dBm, FX_LOGGER, TESTTONETOOL
from rfiuck import siggen


OVERSAMPLE = 3*np.e # Oversampling factor ~8.5, ensures sample rate f_s is >> the highest frequencies encountered & is not factorable

class ReceptorSimulator(object):
    """
        This class defines the standard simulation approach. It is typically only necessary to override
        the analogue path and set the static parameters.
    """
    def __init__(self, band, f_low, f_high, f_sample, nbits, dBmFS, jitter_rms=0, adc_prototype=None, SFDR=None, CLKGEN=None, N_avg=4, TABULATE=[]):
        """
            @param band: a string identifier for the band
            @param jitter_rms: RMS phase noise of a 1/f spectrum with a floor ~20dB below the peak
            @param adc_prototype: specifies the INL model used by apply_sampler()
            @param SFDR: ADC SFDR [dB]
            @param CLKGEN: a function like `lambda t,f_s:sin(2*pi*f_s*t)` to generate the clock to use in sample(signal,f_sample,jitter_rms) (default None)
            @param N_avg: > 1 to increase the averaging applied for PSD, Gain and Noise figure (default 4)
            @param TABULATE: any subset of ["G","NF_K"|"NF_dB","IMD","SFDR","SINAD"] to print out tables of logged values (default [] i.e. "NF_K")
        """
        self.band = band
        self.f_low = f_low
        self.f_high = f_high
        self.f_sample = f_sample
        self.nbits = nbits
        self.dBmFS = dBmFS
        self.jitter_rms = jitter_rms
        self.inl_model = adc_prototype
        self.SFDR = SFDR
        self.CLKGEN = CLKGEN
        
        self.f_s = f_s = OVERSAMPLE*f_sample # >> the highest frequencies encountered, not factorable
        
        self.nyqzone = 1 if (f_high<=f_sample/2.) else 2
        # Probes with default presets
        self._P_dBm = P_dBm()
        self._TS = TIMESERIES(f_s, traces="23D", title=self.band)
        self._PSD = PSD(f_s, Navg=N_avg, traces="123", title=self.band)
        xlim = [0,(self.nyqzone-0.5)*f_sample]
        self._G =  GAIN(f_s, Navg=N_avg*64, traces='AD', xlim=xlim, title=self.band)
        _NF_u,_NF_d = ("dB",mean_dB) if ("NF_dB" in TABULATE) else ("K",np.mean)
        self._NF = NOISEFIGURE(f_s, Navg=N_avg*64, unit=_NF_u, traces='AD', xlim=xlim, ylim=[0,30], title=self.band)
        
        # These loggers discard outliers beyond 2.5% .. 97.5% for robustness against numerical artifacts e.g two tone tests 
        self.cascade_N = self._PSD.add_logger(FX_LOGGER("N [dBm/Hz]", [(f_low+f_high)/MHz/2.], RBW=(f_high-f_low)/MHz, det_func=mean_dB, discard_pct=5))
        self.cascade_G = self._G.add_logger(FX_LOGGER("G [dB]", [(f_low+f_high)/MHz/2.], RBW=(f_high-f_low)/MHz, det_func=mean_dB, discard_pct=5))
        self.cascade_T = self._NF.add_logger(FX_LOGGER("Teq [%s]"%_NF_u, [(f_low+f_high)/MHz/2.], RBW=(f_high-f_low)/MHz, det_func=_NF_d, discard_pct=5))
        
        self.TABULATE = TABULATE
        f_ignore = [(f_sample/2./MHz,np.inf)] if (f_low < f_sample/2.) else [(0,f_sample/2./MHz),(f_sample/MHz,np.inf)]
        self._TTTOOL = TESTTONETOOL(MHz, f_ignore=f_ignore)
    
    def make_refplane(self, t, s):
        """ NB: use like `s = make_refplane(t, s)` """
        s = self._TTTOOL.attach_to(s, self.f_s, self._PSD) # Two tone linearity measurements ref'd to Feed input
        self._G.ref_signal = self._NF.ref_signal = s[:] # Establish the reference plane for Gain and NF - makes a copy to be safe against in-place modification!
        return s
    
    def apply_analogue(self, t, s, T_amb=290, **kwargs):
        """
            Simulates the complete analogue lineup, from EM@atmosphere to digital input.
            This implementation does nothing except for defining the reference plane for gain & noise figure.
            
            @param T_amb: thermodynamic temperature for noise calculations, 0 to eliminate noise (default 290) [K]
            @return: (time, amplitude) time series at analogue output 
        """
        return t, self.make_refplane(t, s)
    
    def apply_sampler(self, t, s, T_amb=290, f_offset=0, **kwargs):
        """
            Simulates the conversion from RF to digital domain.
            This implementation does everything that should be necessary up to output of ADC.
            
            @param t,s: input (amplitude) time series, scaled for 0dBi antenna port [sec] and [V]
            @param T_amb: thermodynamic temperature for noise calculations, 0 to eliminate noise (default 290) [K]
            @param f_offset: clock offset frequency (default 0) [Hz]
            @return: (t_sampled, d_sampled) unsigned int (0..2^nbits-1) amplitude time series
        """
        f_sample = self.f_sample + f_offset # ADC sample rate actually applied
        sample_clk = f_sample if (self.CLKGEN is None) else self.CLKGEN(t,f_sample) # sample() still adds jitter on top of this
        
        # Sample
        _probe("3.ADC analogue in", s, self._P_dBm, self._TS(trace="2"), self._G(trace="A"), self._NF(trace="A"))
        jitter_rms = 0 if (T_amb==0) else self.jitter_rms
        t_sample, z = sample(s, self.f_s, sample_clk, jitter_rms=jitter_rms)
        self._TTTOOL.adjust_freq(Df=0, f_nyquist=f_sample/2.)
        _probe("3.Sampled @ %.2fMHz"%(f_sample/MHz), z, self._P_dBm, self._PSD(trace="3",f_s=f_sample), self._TS(trace="3",samplerate=f_sample))
        # Quantize -- note that this preserves the amplitude scale
        nbits = self.nbits; level_width = quantizer_thresh(nbits, dBFS=self.dBmFS-30)
        d = quantize(z, nbits, level_width, inl_error=self.inl_model, SFDR=self.SFDR, as_int=False)
        _probe("3.Quantised @ %d bits"%nbits, d, self._P_dBm, self._PSD(trace="3",f_s=f_sample), self._TS(trace="3",samplerate=f_sample,fmt='.'),
                        self._G(trace='D',f_s=f_sample), self._NF(trace='D',f_s=f_sample))
        _probe("3.Sampled-Quant[%g/div]"%level_width, z-d, self._TS(trace="D",samplerate=f_sample,fmt='.',ylim=[-10*level_width,10*level_width]))
        d = np.asarray(d/level_width+2**(nbits-1), int) # Convert to int 0..2^nbits-1, as would have been with as_int=True
        
        self._DIG = TIMESERIES(f_sample,traces="T",sharex=False,title=self.band).nest(HISTOGRAM(f_sample,Nfullscale=2**nbits,traces="H",stem=False))
        _probe("3.Quantised @ %d bits"%nbits, d, self._DIG(trace="T"), self._DIG(trace="H"))
        
        return t_sample, d
    
    def select_subbands(self, t_sample, d, f_c, BW_SUB, UP,DOWN, f_c_out=-1, nbits_out=np.inf, suppression=40, ftype="cheby2", decimating_filter=None, **kwargs):
        """
            Converts from raw digital domain to output one or more sub-bands.
            This implementation first filters to reduce quantisation noise (SNR increases), then up-converts
            the [f_c+-SUB_BAND/2]GHz range to (N+1/4)*f_s_out and finally resamples to f_s_out.
            This down-converts the signal to baseband, frequency ordering is preserved.
            The numerical precision is increased (quantisation noise and spurs are suppressed relative to in-band signal).
            
            @param t_sample,d: discrete (amplitude) time series, in [sec] and unsigned int (0..2^nbits-1).
            @param f_c: 1 or more centre frequencies ("on the sky") [Hz] for each sub-band.
            @param BW_SUB: the minimum bandwidth [Hz] of a sub-band, without guard band.
            @param UP,DOWN: integers defining the sampling frequency of the output sub-band as f_s_out = f_s(input)*UP/DOWN.
            @param f_c_out: specify this to override the default centre frequency of the output sub-bands (default < 0, implies f_s_out/4)
            @param suppression: anti-aliasing suppression for the band select filter (default 40) [dB].
            @param ftype: the filter type to use if no 'decimating_filter' is given (default 'cheby2').
            @param decimating_filter: instead of ftype, specify a function like `lambda d, f_c_sampled, f_sampled, suppression, UP, DOWN: filter(d)` (default None)
            @param kwargs: optional arguments, currently ignored. 
            @return: (t_sub,d_sub),... unsigned int (0..2^nbits_out-1) amplitude time series for each centre frequency.
        """
        f_c = np.atleast_1d(f_c)
        f_s = 1/np.diff(t_sample).mean()
        f_s_out = f_s*UP/float(DOWN) # Sample rate of DDC output
        f_c_out = f_c_out if (f_c_out >= 0) else f_s_out/4. # f_c is translated to this centre frequency in the output
        
        d = d - np.mean(d) # Convert to zero mean to prevent ringing through filters
        
        _PSD = PSD(UP*f_s, Navg=self._PSD.Navg, traces="i"+("".join(map(str,range(len(f_c))))), title=self.band)
        _probe("4.ADC Output", d, _PSD(trace="i",f_s=f_s))
        
        results = []
        
        # Perform DDC
        if (decimating_filter is None): # Without a decimating filter the approach is to upsample, then filter, then downsample.
            # Upsample once, since that's the same for all sub-bands
            f_s = UP*f_s
            d = upsample(d, UP) * UP # Creates Nyquist images & scale to conserve power
            t_sample = t_sample[0] + np.arange(len(d))*1/f_s # Note: If t_sample was originally irregularly spaced, it is now regular.
            GB = min(f_c_out,f_s_out/2.-f_c_out)-BW_SUB/2. # Minimum guard band on either end
            _filter_ = lambda x, F_c: 0j+bpfilter(x, (F_c-BW_SUB/2.,F_c+BW_SUB/2.), f_s, 0.1, (F_c-BW_SUB/2.-GB,F_c+BW_SUB/2.+GB), suppression, ftype=ftype)
        else:
            _filter_ = lambda x, F_c: decimating_filter(x, F_c, f_s, suppression, UP,DOWN)
        
        for i, f_c in enumerate(f_c):
            # Convert RF frequency to Nyquist frequency.
            F_c = f_c if (f_c < f_s/2.) else f_s-f_c
            # Filter
            d_ss = _filter_(d, F_c)
            if (decimating_filter is None):
                t_ss, f_ss = t_sample, f_s
            else:
                t_ss, f_ss = t_sample[0] + np.arange(len(d_ss))*1/f_s_out, f_s_out
            _probe("4.BPF @ %.fMH"%(f_c/MHz), d_ss, _PSD(trace="i",f_s=f_ss))
            
            # Translate down to 1st Nyquist zone
            DF = f_c_out-F_c if (f_c < f_s/2.) else (f_s_out-f_c_out)-F_c # If sampled in 2nd Nyquist zone then translate to 3rd (-1st) Nyquist zone to restore original frequency sense.
            d_ss *= np.exp(2.0j*np.pi*DF*t_ss)
            _probe("4.Translated by %.fMHz"%(DF/MHz), d_ss, _PSD(trace=str(i),f_s=f_ss))
            
            if (decimating_filter is None): # Downsample each sub-band
                d_ss = 2*bpfilter(d_ss, (0,f_s_out/2.), f_s, 3, N=8, ftype='butter') # Perhaps it's a hack, but this is necessary to avoid aliasing with the upsampled approach
                t_ss, d_ss = t_ss[::DOWN], d_ss[::DOWN]
                _probe("4.DDC from %.fMHz @ %.fMsps"%(f_c/MHz,f_s_out/MHz), d_ss, _PSD(trace=str(i),f_s=f_s_out))
            
            d_ss = np.real(d_ss)
            
            # Collect standard metrics for this sub-band
            self._TTTOOL.adjust_freq(DF if (f_c < f_s/2.) else -(f_s+DF), f_nyquist=f_s_out/2.) # Frequency translation for CW test signals
            _probe("4.DDC from %.fMHz @ %.fMsps"%(f_c/MHz,f_s_out/MHz), d_ss, self._PSD(trace=None,f_s=f_s_out))
        
            results.append([t_ss, d_ss])
        
        # Apply output conditioning
        for i,(t_ss,d_ss) in enumerate(results):
            assert np.isclose(f_s_out, 1/np.diff(t_ss).mean(), rtol=0,atol=1e-3), "DDC output sample rate differs from expected rate!"
            if np.isfinite(nbits_out): # Re-quantise to nbits_out
                d_ss *= 2**(nbits_out-self.nbits) # Apply constant scale to take advantage of possibly enhanced output resolution
                d_ss = np.asarray(np.round(d_ss-0.5), int) + 2**(nbits_out-1) # Convert to int 0..2^nbits-1
                d_ss = np.clip(d_ss, 0, 2**nbits_out-1) # Enforce final bit range
                results[i][-1] = d_ss
            _probe("4.DDC from %.fMHz @ %.fMsps, %d bits"%(f_c/MHz,f_s_out/MHz,nbits_out), d_ss, _PSD(trace=str(i),f_s=f_s_out), self._PSD(trace=None,f_s=f_s_out))
        
        # Time series only of the last sub-band
        Nfullscale = 2**nbits_out if np.isfinite(nbits_out) else np.max(d_ss)-np.min(d_ss)+1
        self._DDC = TIMESERIES(f_s_out,traces="T",sharex=False,title=self.band).nest(HISTOGRAM(f_s_out,Nfullscale=Nfullscale,traces="H",stem=False))
        _probe("4.Re-quantised of DDC %.fMHz"%(f_c/MHz), d_ss, self._DDC(trace="T"), self._DDC(trace="H"))
        return (results if len(results)>1 else results[0]) # 'unpacked'
    
    def apply_postprocessing(self, t_sample, d, **kwargs):
        """
            Simulates the conversion from raw digital domain to output product.
            This implementation does nothing.
            
            @param t_sample,d: discrete (unsigned int amplitude) time series, scaled for 0dBi antenna port [sec] and [V]
            @return: (t_sampled, d) signed int amplitude time series.
        """
        return t_sample, d
    
    def evaluate(self, t, s, probes=True, **kwargs):
        """
            Simulates the complete lineup, from RF to digital domain.
            
            See arguments for apply_analogue() & apply_sampler().
            @param t,s: input (amplitude) time series, scaled for 0dBi antenna port [sec] and [V]
            @param probes: (default True)
            @return: (t_sampled, s_sampled) i.e. (time vector, voltage series) 
        """
        _TTTOOL, cascade_G, cascade_N, cascade_T = self._TTTOOL, self.cascade_G, self.cascade_N, self.cascade_T
        cascade_G.clear(); cascade_N.clear(); cascade_T.clear()
        
        if probes:
            _probe.suppressed.clear()
        else:
            _probe.suppressed.extend([GAIN,NOISEFIGURE,PSD,TIMESERIES,HISTOGRAM])
        t, s = self.apply_analogue(t, s, **kwargs)
        t_sample, d = self.apply_sampler(t, s, **kwargs)
        output = self.apply_postprocessing(t_sample, d, **kwargs)
        _probe.suppressed.clear()
        
        if probes: # Summarise results
            fig = self.plot_cascade("N", markers='.', subplot=211, grid=True, title=self.band)
            for TABULATED in ["IMD","IP2","IP3"]:
                self.plot_cascade(TABULATED, subplot=211, fig=fig)
            self.plot_cascade("NF_K", markers="o",color='r',legend_loc='upper left', subplot=212, fig=fig)
            fig.sca(fig.gca().twinx()); self.plot_cascade("G", markers="v",legend_loc='upper right', grid=False, fig=fig) # Independent y-axis
            
            for TABULATED in self.TABULATE:
                if (TABULATED == "G"): cascade_G.tabulate(round_decimals=3)
                elif (TABULATED in ["NF_K","NF_dB"]): cascade_T.tabulate(round_decimals=3)
                elif (TABULATED == "ENOB"):
                    _TTTOOL.tabulate("SINAD", ylabel="ENOB [bits]", transform=lambda SINAD:(SINAD-1.76+0)/6.02, round_decimals=3) # TODO: apply correction if HDRM>0dB
                else: _TTTOOL.tabulate(TABULATED, round_decimals=3)
        return output

    def plot_cascade(self, TABULATED, fig=None, **kwargs):
        style = dict(markersize=6, grid=True, fig=fig); style.update(kwargs)
        if (TABULATED == "G"): fig = self.cascade_G.plot(**style)
        elif (TABULATED == "N"): fig = self.cascade_N.plot(**style)
        elif (TABULATED in ["NF_K","NF_dB"]): fig = self.cascade_T.plot(**style)
        elif (TABULATED == "ENOB"):
            style["ylabel"] = style.get("ylabel", "ENOB [bits]")
            self._TTTOOL.plot("SINAD", transform=lambda SINAD:(SINAD-1.76+0)/6.02, **style) # TODO: apply correction if HDRM>0dB
        else: fig = self._TTTOOL.plot(TABULATED, **style)
        return fig



log10_0 = np.vectorize( lambda x,eps=1e-3: np.log10(eps if (x<eps) else x) ) # log10 except for x=0
class ClockGenerator(object):
    """ Clock generator based on a sampled phase noise spectrum. """
    def __init__(self, f_offset, L_phi, f_clock=0):
        """
            @param f_offset, L_phi: discrete samples of offsets from the clock frequency [Hz] with corresponding single sideband phase noise power [dBc/Hz]
            @param f_clock: clock frequency to use unless overridden later (default 0) [Hz]
        """
        self.f_offset = np.array(f_offset)
        # Must resample in LOG space since integrating in linear space will over-estimate coarse sampled logarithmic curves
        self.L_phi = spi.interp1d(log10_0(self.f_offset), L_phi, 'quadratic', bounds_error=False, fill_value=(L_phi[0],L_phi[-1])) # self.L_phi is in LOG,LOG scale!
        self.f_clock = f_clock
        
    def __call__(self, t, f_clock=None):
        """ Generates the clock signal time series, including the specified phase noise contribution.
            @param t: time intervals where the clock values are required [sec].
            @param f_clock: override the initialised clock frequency (default None) [Hz]
            @return: sqrt(2)*sin(2*pi*f_clock*t) + iFT(L_phi(f-f_clock))
        """
        f_clock = f_clock if (f_clock is not None) else self.f_clock
        clock = 2**.5*np.sin(2*np.pi*f_clock*t) * len(t)**.5 # 0dB in same power scale as L_phi is evaluated below
        noise = siggen.PowerSpectrumSignal(t, lambda f: 10**(self.L_phi(log10_0(np.abs(f-f_clock)))/10.))
        return clock + noise
    
    def jitter_rms(self, f_min=0, f_max=None, f_clock=None):
        """ @param f_min, f_max: integration boundaries, f_max defaults to max of the original samples of L_phi (default 0,None) [Hz]
            @param f_clock: override the initialised clock frequency (default None) [Hz]
            @return: rms jitter [sec] integrated over the dual-sided phase noise spectrum centred on f_clock
        """
        f_clock = f_clock if (f_clock is not None) else self.f_clock
        f_max = np.max(self.f_offset) if (f_max is None) else f_max
        # Interpolate in LOG space, then convert to linear scale and integrate
        DF = np.log10(f_max)/1e3 # 1e3 points in LOG space should be adequate
        F = np.log10(f_min+1e-12) + np.arange(1e3)*DF # Avoid 0
        rms_rad = (2*sp.trapz(10**(self.L_phi(F)/10.), 10**F))**.5
        return rms_rad / (2*np.pi*f_clock)


@_test_()
def _test_ClockGenerator_(_test_):
    clkgen = ClockGenerator([0.1,1,10,100,1e3,10e3,100e3,1e6], [-13,-43,-73,-103,-128,-146,-148,-148], f_clock=4e9) # Hz, dBc/Hz [SKA-TEL-SADT-0000620 par 4.4.3 table 2]
    f_min=10; f_max=1e6; RMS_sec=28.3e-15
    _test_.assert_isclose(RMS_sec, clkgen.jitter_rms(f_min=f_min, f_max=f_max), "Phase noise from L(f) differs more than 2%% from expected", rtol=0.02)
    
    # Visual inspection of L_phi from 0 to 10 MHz
    import pylab as plt
    plt.figure(); plt.plot(log10_0(np.arange(10*f_max)), clkgen.L_phi(log10_0(np.arange(10*f_max))))
    
    # "Measure" the phase noise from the phase noise spectrum
    n_pts = 2**20
    f_s = clkgen.f_clock * OVERSAMPLE
    t = np.arange(n_pts)*1/f_s
    # Visual inspection of time series 
    clk = clkgen(t)
    plt.figure()
    plt.plot(t[:int(n_pts/OVERSAMPLE)], clk[:int(n_pts/OVERSAMPLE)], 'o-') # Only a short segment to avoid memory issues
    plt.plot(t[-int(n_pts/OVERSAMPLE):], clk[-int(n_pts/OVERSAMPLE):], 'o-')
    
    # Number of points limits the frequency resolution of the PSD (D_f = f_s/n_pts), so exaggerate the phase noise spectrum for this measurement
    # Or perhaps better to choose f_clock or f_s so that there's an integer number of clock cycles over n_pts?
    clkgen = ClockGenerator(np.r_[clkgen.f_offset[-1]*(2**np.arange(1,1+len(clkgen.f_offset)))], clkgen.L_phi(np.log10(clkgen.f_offset)), f_clock=clkgen.f_clock)
    clk = clkgen(t)
    from rfsim.base import cosine10 as win # The window floor must be << min(L_phi) -- cosine10 & 11 work for this specific test case
    P,F = plt.mlab.psd(clk, n_pts, Fs=f_s, window=win(n_pts),pad_to=2*n_pts) # W/Hz, Hz normalized for incoherent gain. Window & pad to improve frequency resolution.
    # Clock power spectrum == Phase noise spectrum (relative to fundamental tone power) by construction. AVOID normalizing to max(P) since FFT coherent gain != incoherent gain.
    f_min, f_max = 10*(F[1]-F[0]), clkgen.f_offset[-1]
    RMS_sec = clkgen.jitter_rms(f_min, f_max)
    bounds_low = ((clkgen.f_clock-F)>=f_min) & ((clkgen.f_clock-F) <= f_max) # f_min & f_max bounds below tone
    bounds_high = ((F-clkgen.f_clock)>=f_min) & ((F-clkgen.f_clock) <= f_max) # f_min & f_max bounds above tone
    plt.figure(); plt.plot(F[np.abs(F-clkgen.f_clock)<=2*f_max], 10*np.log10(P[np.abs(F-clkgen.f_clock)<=2*f_max]))
    plt.plot(F[bounds_low | bounds_high], 10*np.log10(P[bounds_low | bounds_high]))
    rms_rad = (sp.trapz(P[bounds_low], F[bounds_low]) + sp.trapz(P[bounds_high],F[bounds_high]))**.5
    rms_sec = rms_rad / (2*np.pi*clkgen.f_clock)
    _test_.assert_isclose(RMS_sec, rms_sec, "Phase noise from time series differs more than 10%% from expected", rtol=0.1)
    
    plt.show()


if __name__ == "__main__":
    _test_ClockGenerator_()
