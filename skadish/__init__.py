"""
    Copyright @ 2019 SARAO (http://www.ska.ac.za). All rights reserved.
    
    Simulation models for the SKA1_MID DISH element.
    
    Example of use:
    
        import numpy as np
        from skadish.dish_model import SPFB1Simulator
        from rfiuck.siggen import ContinuumNoiseSignal
        
        t = np.arange(2**20)/(8.9*4e9)
        DISH_IN = ContinuumNoiseSignal(t,10,350,spec_index=-2.3)
        
        sim = SPFB1Simulator(TABULATE=["G","NF_K"], CLKGEN=None)
        sim.evaluate(t, DISH_IN, atten=8, f_offset=1800)
    
    @author: aph@ska.ac.za
"""