#!/usr/bin/env python
"""
    Digital Down Conversion for SKA1_MID SPFRx.
    
    @author Stephen.Harrison@nrc-cnrc.gc.ca, 16 July 2019
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat


# Simple Mathematical model.
# This model doesn't store any context, you have to process the whole signal at once.
def applyFilter(x, h, L, M):
    """Apply band 5 filter model based on resampling ratio L/M and sample rate."""
    y      = np.zeros(int(len(x)*float(L)/M)).astype(np.complex)

    # Pad out the impulse response so that length is divisible by L
    padLen = int(np.ceil(len(h)/float(L)))*L-len(h)
    h      = np.append(h, np.zeros(padLen))
    N      = len(h)

    # Apply the filter in a simple loop.
    for n in np.arange(int(len(x)*L/M-N/L)):
        x_ind = int(np.floor(n*float(M)/L))
        h_ind = int((n*M)%L)
        y[n] = np.sum(x[x_ind:x_ind+N//L][::-1]*h[h_ind::L])

    return y

def applyDDC(x, h, L, M, fc, fs, fo=1.5e9):

    # Shift the filter to the desired center frequency
    # Note that the prototype filter is designed in the fs*L rate
    # And needs to be shifted like this for it to work.
    h_shift = h*np.exp(2.0j*np.pi*fc/(fs*L)*np.arange(len(h)))

    # Apply the filter
    # Convert the ADC code to the voltage value it represents
    y = applyFilter(x+0.5, h_shift, L, M)

    # Apply final frequency mixing. 
    y *= np.exp(-2.0j*np.pi*(fc-fo)*M/(fs*L)*np.arange(len(y)))

    # Take real part, clip, quantize
    y = np.real(y) 

    y = np.clip(y, -7, 7) # -8 is not used: invalid sample value according to the ICD
    y = np.round(y)

    return y

def Band5BDDC(x, h, fc, fs):

    return applyDDC(x, h, 3, 8, fc, fs)


def Band5ADDC(x, h, fc, fs):

    return applyDDC(x, h, 2, 3, fc, fs)

# The following ...DDCFilter() are simply a re-packaging of part of the code originally spread over __main__ and applyDDC() 
def Band5BDDCFilter(x, fc, fs, SAtt, L, M):
    h = loadmat(__file__+'/../coeffs/DDC_B5_B_Prototype_FLT_SAtt_%ddB.mat' %(SAtt))['h'][0]
    # Compensate for loss
    h *= fs/2.5e9*np.sqrt(2) # TODO APH changed 16/2.5 to this
    # Shift the filter to the desired center frequency
    # Note that the prototype filter is designed in the fs*L rate
    # And needs to be shifted like this for it to work.
    h_shift = h*np.exp(2.0j*np.pi*fc/(fs*L)*np.arange(len(h)))

    # Apply the filter
    # Note that the filter should operate on zero mean data 
    return applyFilter(x, h_shift, L, M)

def Band5ADDCFilter(x, fc, fs, SAtt, L, M):
    h = loadmat(__file__+'/../coeffs/DDC_B5_A_Prototype_FLT_SAtt_%ddB.mat' %(SAtt))['h'][0]
    # Compensate for loss
    h *= fs/2.5e9*np.sqrt(2) # TODO APH changed 9/2.5 to this
    # Shift the filter to the desired center frequency
    # Note that the prototype filter is designed in the fs*L rate
    # And needs to be shifted like this for it to work.
    h_shift = h*np.exp(2.0j*np.pi*fc/(fs*L)*np.arange(len(h)))

    # Apply the filter
    # Note that the filter should operate on zero mean data 
    return applyFilter(x, h_shift, L, M)

# The following are meant to illustrate the use of the ...DDCFilter() functions
def _Band5BDDC_(x, fc, fs, SAtt=30, fo=1.5e9):
    L, M = 3, 8
    filtered = Band5BDDCFilter(x, fc, fs, SAtt, L, M)
    filtered *= np.exp(-2.0j*np.pi*(fc-fo)*M/(fs*L)*np.arange(len(filtered)))
    return np.real(filtered)
def _Band5ADDC_(x, fc, fs, SAtt=30, fo=1.5e9):
    L, M = 2, 3
    filtered = Band5ADDCFilter(x, fc, fs, SAtt, L, M)
    filtered *= np.exp(-2.0j*np.pi*(fc-fo)*M/(fs*L)*np.arange(len(filtered)))
    return np.real(filtered)


if __name__ == '__main__':
    SAtt = 30
    f0 = 8.3e9

    h5a = loadmat('coeffs/DDC_B5_A_Prototype_FLT_SAtt_%ddB.mat' %(SAtt))['h'][0]
  
    # Compensate for loss
    h5a *= 9/2.5*np.sqrt(2)
  
    x = np.random.normal(scale=2.98, size=2**19)
    x += np.cos(2*np.pi*f0/9e9*np.arange(2**19))
  
    # Convert voltage values to ADC codes, clip and quantize.
    x = np.clip(x-0.5, -8, 7)
    x = np.round(x)
  
    y = Band5ADDC(x, h5a, 3e9, 9e9)
    if True: # Demonstrate re-factored functions
        oy = y
        y = _Band5ADDC_(x+0.5, 3e9, 9e9, SAtt) # Shift input levels to zero mean for DDC
        y = np.clip(y, -7, 7) # -8 is not used: invalid sample value according to the ICD
        y = np.round(y)
        assert np.all(oy==y), "Unexpected discrepancy in time series!"
          
        plt.figure()
        plt.psd(x+0.5, Fs=9e9)
        L, M = 2, 3
        if (f0<=9e9/2):
            plt.psd(Band5ADDCFilter(x+0.5, f0, 9e9, SAtt, L=L, M=M), Fs=L/M*9e9)
        else: # For fc > fs/2, n*fs - fc works only if n = N*L+1!?
            plt.psd(Band5ADDCFilter(x+0.5, (0*L+1)*9e9 - f0, 9e9, SAtt, L=L, M=M), Fs=L/M*9e9)
            plt.psd(Band5ADDCFilter(x+0.5, (1*L+1)*9e9 - f0, 9e9, SAtt, L=L, M=M), Fs=L/M*9e9)
            plt.psd(Band5ADDCFilter(x+0.5, (2*L+1)*9e9 - f0, 9e9, SAtt, L=L, M=M), Fs=L/M*9e9)
    fy = np.fft.fft(y, 2048)[:1024] # TODO: APH changed y[:2048] to y, 2048
    f = np.linspace(0, 3, 1024)
  
    plt.figure()
    plt.plot(f, 20.*np.log10(np.abs(fy)))
    plt.xlabel('Freq (GHz)')
    plt.ylabel('dB')
    plt.show()

    h5b = loadmat('coeffs/DDC_B5_B_Prototype_FLT_SAtt_%ddB.mat' %(SAtt))['h'][0]

    # Compensate for loss
    h5b *= 16/2.5*np.sqrt(2)

    x = np.random.normal(scale=2.98, size=2**20)
    x += np.cos(2*np.pi*f0/16e9*np.arange(2**20))
    x += 2.*np.cos(2*np.pi*1.1*f0/16e9*np.arange(2**20))
    x = np.clip(x-0.5, -8, 7)
    x = np.round(x)

    y = Band5BDDC(x, h5b, 6e9, 16e9)
    if True: # Demonstrate re-factored functions
        oy = y
        y = _Band5BDDC_(x+0.5, 6e9, 16e9, SAtt) # Shift input levels to zero mean for DDC
        y = np.clip(y, -7, 7) # -8 is not used: invalid sample value according to the ICD
        y = np.round(y)
        assert np.all(oy==y), "Unexpected discrepancy in time series!"
        
        plt.figure()
        plt.psd(x+0.5, Fs=16e9)
        L, M = 3, 8
        if (f0<=16e9/2):
            plt.psd(Band5ADDCFilter(x+0.5, f0, 16e9, SAtt, L=L, M=M), Fs=L/M*16e9)
        else: # For fc > fs/2, n*fs - fc works only if n = N*L+1!?
            plt.psd(Band5ADDCFilter(x+0.5, (0*L+1)*16e9 - f0, 16e9, SAtt, L=L, M=M), Fs=L/M*16e9)
            plt.psd(Band5ADDCFilter(x+0.5, (1*L+1)*16e9 - f0, 16e9, SAtt, L=L, M=M), Fs=L/M*16e9)
            plt.psd(Band5ADDCFilter(x+0.5, (2*L+1)*16e9 - f0, 16e9, SAtt, L=L, M=M), Fs=L/M*16e9)
    fy = np.fft.fft(y, 2048)[:1024] # TODO: APH changed y[:2048] to y, 2048
    f = np.linspace(0, 3, 1024)

    plt.figure()
    plt.plot(f, 20.*np.log10(np.abs(fy)))
    plt.xlabel('Freq (GHz)')
    plt.ylabel('dB')
    plt.show()
