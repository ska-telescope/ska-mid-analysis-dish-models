"""
    Basic application of rfiuck + rfsim packages, for SKA1_MID.
    This is intended to be used as starting point for a program to generate reference RFI datasets,
    as well as for generating DISH signal path performance reports.
    
    Tuning required:
    1) setup_DISH() - update attenuation values for RFI use cases > 0
    
    @author: aph@ska.ac.za
"""
import numpy as np
from rfiuck.demo import RFI_UC, show
from rfiuck.siggen import MHz, ContinuumNoiseSignal, WhiteNoiseSignal
from rfiuck.siggen import sig_generator, save_data, load_data_generator
from rfsim.elements import ClockGenerator
from skadish.dish_model import SPFB1Simulator, SPFB2Simulator, SPFB3Simulator, SPFB4Simulator, SPFB5aSimulator, SPFB5bSimulator


def tx_from_DISH(t_s, DISH_OUT, band, rfi_uc, offset_index, blk, f_c=None):
    """ Transmit from DISH to consumer" i.e. save DISH output to file, per block. All sub-bands are packaged  together.
        The following detail in "ICD: SKA1 Dish to CSP", SKA-TEL-SKO-0000124 (rev 4) is not currently implemented:
          "meta_frame" is completely omitted
          "sample_frame" details not implemented:
              encode sample values in two's complement
              interleave dual polarization streams
              interleave dual sub-band streams
    """
    DISH_OUT = DISH_OUT # No additional conditioning done for simulations
    save_data("DISH_OUT_%s_UC_%d_OSI_%04d_Block_%d_Change.mat"%(band,rfi_uc,offset_index,blk), t_s, DISH_OUT, f_c)

def rx_from_DISH(band, rfi_uc, blocks, offset_index, debug=True):
    """ "Consumer receive from DISH" i.e. load DISH output from file, per block. """
    loader = load_data_generator(["DISH_OUT_%s_UC_%d_OSI_%04d_Block_%d_Change.mat"%(band,rfi_uc,offset_index,blk) for blk in np.atleast_1d(blocks)])
    if debug:
        for DISH_OUT in loader:
            t_s, DISH_OUT, f_c = DISH_OUT # No additional conditioning done for simulations
            for t_s,d_s,f_c in zip(t_s, DISH_OUT, f_c):
                Z = {"B1":1,"B2":1,"B3":2,"B4":2,"B5a":3,"B5b":4}[band] # JUST APPROX, FOR DISPLAY!! Bands 3, 4, 5a & 5b have postprocessing that changes the ADC zones and also flips the sense!
                # Overplot non=overlapping time series on figures 200-201 which are hopefully not yet allocated
                show(t_s, d_s, "DISH %s%s Output"%(band,", %gMHz"%(f_c/MHz)), nyqzone=Z, f_resolution=10*MHz, overplot_fig=[200,201])
    return loader


def apply_DISH(sim, star_gen, rfi_uc, atten, n_pts, n_blocks, offset_step=1800,dish_offsets=[0],dish_delays=[0], probes=True, **kwargs):    
    """ Generates input signal and applies DISH processing. This simply adds Tsys then applies bandpass filtering and sampling.
        Results are passed on to tx_from_DISH().
        
        @param sim: an instance of ReceptorSimulator.
        @param star_gen: generator function
        @param rfi_uc: RFI Use Case ID (as accepted by RFI_UC())
        @param atten: analogue attenuation [dB].
        @param n_pts, n_blocks: the number of points per block, and number of consecutive blocks to generate & process.
        @param offset_step: sampling clock offset step (default 1800) [Hz]
        @param dish_offsets: Each dish is represented by a unique frequency offset as an integer in the range (0-2222) (default 0)
        @param dish_delays: Each dish has a delay towards the common star (default 0) [sec]
        @param probes: False to disable all probes except RMS power, which speeds up execution (default True).
        @param kwargs: additional arguments passed to sim.evaluate(), especially sub-banding through "f_c=[...]"
    """
    rfi_gen = RFI_UC(sim.band, rfi_uc) # The same generator instances are used for all DISHes
     
    def generate_input(t_delay=0):
        """ Generates input ("analogue") data, following a generator pattern.
            @param t_delay: the delay for "rfi" relative to the "star" signal (star signal is stationary, rfi signal is delayed) [sec]
            @yield: time (without delay), signal (delayed relative to time[0])
        """
        f_s = sim.f_s
        siggen = star_gen.delay(t_delay=0) + rfi_gen.delay(t_delay=t_delay) # star is DELAY TRACKED and RFI is not
        for blk in range(n_blocks):
            t = (np.arange(0, n_pts)+n_pts*blk)*1./f_s
            yield (t, siggen(t))
    
    for offset_index,delay in zip(dish_offsets,dish_delays): # For each dish a unique frequency offset and specified delay 
        for blk,(t,DISH_IN) in enumerate(generate_input(t_delay=delay)):
            print("[%s: OffsetStepIndex %d, Block %d]"%(sim.band, offset_index, blk))
            OUT = sim.evaluate(t, DISH_IN, atten=atten, f_offset=offset_step*offset_index, probes=probes, **kwargs)
            # Re-shape OUT to suit tx_from_DISH. A little bit clumsy.
            f_c = np.atleast_1d(kwargs.get("f_c",None))
            OUT = [OUT] if (len(f_c) == 1) else OUT
            tx_from_DISH([TD[0] for TD in OUT], [TD[1] for TD in OUT], sim.band, rfi_uc, offset_index, blk, f_c)


def setup_DISH(band, CLKGEN, usecase, **kwargs):
    """ Standard set up for the simulators. 
        @param CLKGEN: the clock signal generator for the reference clock's jitter. None to set up the simulation with jitter_rms
                       representing the total system level allocation.
        @param usecase: either RFI_UC ID or one of {"SINAD", "IMD"} to select a specific test tone and probe configurations.
        @param kwargs: passed on, to be applied to sim.evaluate(**kwargs)
        @return: (sim, RFIusecase, atten, kwargs)
    """
    testtone = usecase
    if (testtone == "SINAD"):
        TABULATE = ["SFDR","SINAD","ENOB"]
        usecase = 0 # No RFI
    elif (testtone == "IMD"):
        TABULATE = ["G","IMD","IP2","IP3"]
        usecase = 0 # No RFI
    else: # No test tones
        TABULATE = ["G","NF_K"]
        testtone = None
    
    clkargs = dict(CLKGEN=CLKGEN)
    if (CLKGEN is None): # Override the per-DISH allocation with the total system-level allocation [JB-ECP] SKA1 System Budget: MID Sampling Clock Jitter, SKA-TEL-SKO-0000680, rev A UPDATED for ECP-160022
        clkargs["jitter_rms"] = {"B1":0.358e-12,"B2":.358e-12,"B3":.602e-12,
                                 "B4":1.33e-12,"B5a":0.707e-12,"B5b":1.33e-12}.get(band,0)
    
    clkargs["N_avg"] = 2
    if (band == "B1"):
        sim = SPFB1Simulator(TABULATE=TABULATE, **clkargs)
        atten = 6 + {0:0, 50:10, 90:22, 98:40}[usecase] # Attenuation to scale the signal, including RFI, to -12dBFS
        F_tt = [[370.1171875,379.8828125], [1014.6484375,1024.4140625]] # Prime number of cycles for 2**12 samples
    elif (band ==  "B2"):
        sim = SPFB2Simulator(TABULATE=TABULATE, **clkargs)
        atten = 4 + {0:0, 50:10, 90:30, 98:45}[usecase] # Attenuation to scale the signal, including RFI, to -12dBFS
        F_tt = [[1083.0078125,1090.8203125], [1563.4765625,1575.1953125]] # Prime number of cycles for 2**12 samples
    elif (band ==  "B3"):
        sim = SPFB3Simulator(TABULATE=TABULATE, **clkargs)
        atten = 10 + {0:0, 50:3, 90:3, 98:6}[usecase] # Attenuation to scale the signal, including RFI, to -12dBFS
        F_tt = [[1775.78125,1786.71875], [2905.46875,2916.40625]] # Prime number of cycles for 2**12 samples
    elif (band ==  "B4"):
        sim = SPFB4Simulator(TABULATE=TABULATE, **clkargs)
        atten = 12 + {0:0, 50:0, 90:4, 98:33}[usecase] # Attenuation to scale the signal, including RFI, to -12dBFS
        F_tt = [[3003.90625,3019.53125], [4988.28125,4996.09375]] # Prime number of cycles for 2**12 samples
    elif (band ==  "B5a"):
        sim = SPFB5aSimulator(TABULATE=TABULATE, **clkargs)
        atten = 13 + {0:0, 50:10, 90:14, 98:48}[usecase] # Attenuation to scale the signal, including RFI, to -10dBFS
        kwargs["f_c"] = kwargs.get("f_c", [6000*MHz,7200*MHz])
        F_tt = [[4840.576171875,4849.365234375], [8400.146484375,8422.119140625]] # Prime number of cycles for 2**12 samples
    elif (band ==  "B5b"):
        sim = SPFB5bSimulator(TABULATE=TABULATE,  **clkargs)
        atten = 17 + {0:0, 50:0, 90:15, 98:40}[usecase] # Attenuation to scale the signal, including RFI, to -10dBFS
        kwargs["f_c"] = kwargs.get("f_c", [10000*MHz,14000*MHz])
        F_tt = [[8605.46875,8621.09375], [15042.96875,15050.78125]] # Prime number of cycles for 2**12 samples
    
    if (testtone is not None): # Test tone(s) to setup
        F_tt = F_tt[0] # TODO: expose how the frequencies are chosen
        gain = 88 - atten # dB, the nominal cascaded gain with atten=0; within +-1dB the same for all bands
        if (testtone == "SINAD"): # Single tone @ -60dBm ~ Pnom+30dB (noise ~0.5LSB); atten below -> -6.5dBm==-6.5dBFS so that CW covers full scale without clipping [CW: (1.7+4*6.02)/6.5 = 3.97bit; Tcascade: (1.7+4*6.02)/60=0.43bit]
            sim._TTTOOL.setup([F_tt[0]], 10**((sim.dBmFS-7-gain-30+1)/10.), intermod_orders=[1]) # Add a single tone @ -7dBFS, leaving ~1dB margin to full scale 
        elif (testtone == "IMD"):
            sim._TTTOOL.setup(F_tt, 10**((sim.dBmFS-10-gain-30+1)/10.), intermod_orders=[2,3]) # Add two tones @ -10dBFS total (-7dBFS each) so that sum(CW) leaves ~1dB margin to full scale
        
    return sim, usecase, atten, kwargs


def generate_performance_reports(band, CLKGEN, n_pts=2**20, dish_offset=999, **kwargs):
    """ Use the simulators to generate DISH output when standard test signals are employed.
        For these performance characterisation tests it is deemed sufficient to work with a single DISH & a single block.
        
        @param CLKGEN: reference lock generator as accepted by the simulators
        @param kwargs: additional arguments to be passed to simulator's evaluate() function
    """
    # Especially for dominating input signals (e.g. RFI) use a generator to prevent differences in input between simulation runs from  introducing uncertainty in results.
    star = sig_generator(WhiteNoiseSignal,Teq=0)
    
    # Gain & noise figure vs attenuation, without RFI
    sim, rfi, atten, _kwargs = setup_DISH(band, CLKGEN, usecase=0, **kwargs)
    fig = None
    for DA in [0,6,12,18,24,30]:
        apply_DISH(sim, star, rfi, atten+DA, n_pts, n_blocks=1, dish_offsets=[dish_offset],dish_delays=[0], **_kwargs)
        fig = sim.plot_cascade("G", fig, subplot=211, ylabel="G [dB] atten=%d dB"%(atten+DA), title="%s Summary"%(band))
        sim.plot_cascade("NF_K", fig, subplot=212, ylabel="Teq [K] atten=%d dB"%(atten+DA))
    
    # IMD vs attenuation
    sim, rfi, atten, _kwargs = setup_DISH(band, CLKGEN, usecase="IMD",T_amb=0, **kwargs)
    figs = [None, None]
    for DA in [0,6,12,18,24,30]:
        apply_DISH(sim, star, rfi, atten+DA, n_pts, n_blocks=1, dish_offsets=[dish_offset],dish_delays=[0], **_kwargs)
        figs[0] = sim.plot_cascade("G", figs[0], subplot=211, ylabel="G [dB] atten=%d dB"%(atten+DA), title="%s Summary"%(band))
        sim.plot_cascade("IMD", figs[0], subplot=212, ylabel="SFDR [dBc] atten=%d dB"%(atten+DA),
                         transform=lambda IMD: np.sort(IMD,0)[-1]-np.sort(IMD,0)[-2]) # sorted[-1]=max (i.e test tone); sorted[-2] (i.e. largest spur)
        figs[1] = sim.plot_cascade("N", figs[1], ylabel="N [dBm/Hz] atten=%d dB"%(atten+DA), fmt='k-+', title="%s Summary"%(band))
        sim.plot_cascade("IMD", figs[1], ylabel="IMD [dBm/Hz]")
        sim.plot_cascade("IP2", figs[1], ylabel="OIP2 [dBm]")
        sim.plot_cascade("IP3", figs[1], ylabel="OIP3 [dBm]")
    
    # SINAD(+ENOB)
    star = sig_generator(WhiteNoiseSignal,Teq=10e-6) # At most 20 microKelvin! G & NF measurement needs a non-zero input signal, but > 20 microKelvin compromises SINAD & ENOB.
    fig = None
    for clkgen in [None, CLKGEN]:
        sim, rfi, atten, _kwargs = setup_DISH(band, clkgen, usecase="SINAD",T_amb=0, **kwargs)
        apply_DISH(sim, star, rfi, atten, n_pts, n_blocks=1, dish_offsets=[dish_offset],dish_delays=[0], **_kwargs)
        fig = sim.plot_cascade("SFDR", fig, subplot=211, ylabel="SFDR [dBc] CLKGEN=%s"%(clkgen), title="%s Summary"%(band))
        sim.plot_cascade("ENOB", fig, subplot=212, ylabel="ENOB [bits] CLKGEN=%s"%(clkgen))


def generate_RFI_datasets(band, usecase, CLKGEN, n_pts=2**20, n_blocks=1, probes=True, **kwargs):
    """ Basic use of the simulators to generate DISH output for various RFI use cases.
        @param usecase: as accepted by RFI_UC
        @param CLKGEN: reference lock generator as accepted by the simulators
        @param probes: False to disable all probes except RMS power, which speeds up execution (default True).
        @param kwargs: additional arguments to be passed to simulator's evaluate() function
    """
    sim, rfi, atten, kwargs = setup_DISH(band, CLKGEN, usecase, **kwargs)
    # Using a generator makes coherent signals across blocks & DISHes
    star = sig_generator(ContinuumNoiseSignal,T0=1,f0_MHz=350,spec_index=-0.2)
    # If needed, extend dish_offsets & delays to generate datasets for more than just a single DISH
    apply_DISH(sim, star, rfi, atten, n_pts, n_blocks, dish_offsets=[0],dish_delays=[0], probes=probes, **kwargs)


if __name__ == '__main__':
    clkgen = ClockGenerator([0.1,1,10,100,1e3,10e3,100e3,1e6], [-13,-43,-73,-103,-128,-146,-148,-148], f_clock=4e9) # Hz, dBc/Hz [SKA-TEL-SADT-0000620 par 4.4.3 table 2]
#     clkgen = None # Use simple pink noise "total jitter budget" clock
    
    if True: # RFI use cases
        usecase = 0 # 0 = just sky noise
        for B in ["B1","B2","B3","B4","B5a","B5b"]:
            generate_RFI_datasets(B, usecase, clkgen, n_blocks=1, probes=False)
#             rx_from_DISH(B, usecase, blocks=[0], offset_index=0, debug=True) # This line included as a demonstration
    
    else: # Performance cases
        for B in ["B1","B2","B3","B4","B5a","B5b"]:
            generate_performance_reports(B, clkgen, flag_param=0.1)
