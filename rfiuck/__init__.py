"""
    Copyright @ 2019 SARAO (http://www.ska.ac.za). All rights reserved.
    
    A library for generating signals for simulations, with a demonstration
    that generates signals for "RFI use cases".
    
    @author: aph@ska.ac.za
"""