"""
    Copyright @ 2019 SARAO (http://www.ska.ac.za). All rights reserved.
    
    An example of an RFI Use Case to demonstrate the rfiuck package.
    Note on terminology: 'signal generator' functions generate time series that are coherent between repeated calls.
    
    @author: aph@ska.ac.za
"""
from rfiuck.siggen import MHz, generate_TDMA, generate_IMT, generate_SSR, generate_SSR_S, generate_DME, generate_RADAR, sig_generator, WhiteNoiseSignal
import numpy as np
import scipy.signal
from matplotlib import pyplot


def RFI_UC(band, case, SSR="S"):
    """ RFI Use Cases covering 100 - 20000 MHz. As documented in [SKA-TEL-SKO-0000TBD-01].
        
        @param band: "B1"..."B5b", to omit signals that are speculative / uncertain, unless critical to consider for a band.
        @param case: 0: no RFI
                     50: ~50th percentile eg. night time
                     90: ~90th percentile eg. night & day time including airplanes
                     98: ~98th percentile eg. 24hrs including IRDIIUM and other strange signals
        @param SSR: SSR mode to use, either "A/C" or "S" (default "S")
        @return: a generator function like 'lambda t_sample: series'
    """
    if (case == 0): # Zero signal
        return sig_generator(lambda time: 0*time)
    
    S = generate_TDMA(-100, 235, 2/20e3, 1, "1") # DAB, always on
    S += generate_TDMA(-100, 680, 2/8e6, 1, "1", NPSK=8) # DTTV, always on
    if (case == 50): # ~50% levels e.g. night time
        S += generate_IMT(-90, 930, [950],["1"]) # IMT-900 with single voice channel, always on
        S += generate_TDMA(-95, 1250, 2/10e6, 1, "1") # GNSS, always on
        S += generate_TDMA(-95, 1575, 2/10e6, 1, "1") # GNSS, always on
        S += generate_TDMA(-90, 1545, 2/10.5e3, 1, "1", NPSK=4) # INMARSAT Aero Sat-to-Plane
        S += generate_TDMA(-100, 1980, 2/5e6, 1, "1") # Terrestrial fixed, always on
        
    elif (case == 90): # ~90% levels
        S += generate_IMT(-80, 930, [950],["1"]) # IMT-900 with single voice channel, always on
        S += generate_SSR(-65, T_repeat=100e-6) if (SSR=="A/C") else generate_SSR_S(-65, T_repeat=1e-3)
        S += generate_DME(-65, 1115, pattern="100")
        S += generate_TDMA(-90, 1250, 2/10e6, 1, "1") # GNSS, always on
        S += generate_TDMA(-90, 1575, 2/10e6, 1, "1") # GNSS, always on
        S += np.sum([generate_TDMA(-80, 1545+i/2., 2/10.5e3, 1, "1", NPSK=4) for i in range(2)], axis=0) # INMARSAT Aero Sat-to-Plane
        S += generate_TDMA(-90, 1980, 2/5e6, 1, "1") # Terrestrial fixed, always on
        
    elif (case == 98): # ~98% levels
        S += generate_IMT(-80, 930, [945,950],["10","11"]) # IMT-900 with two voice channels, may be on simultaneously
        S += generate_SSR(-55, T_repeat=100e-6) if (SSR=="A/C") else generate_SSR_S(-55, T_repeat=1e-3)
        S += generate_DME(-55, 1115, pattern="100100") + \
             generate_DME(-55, 1050, pattern="100010").delay(3e-6) # Simulation peaks DON'T coincide -- otherwise typically leads to clipping, which should just result in time-domain excision.
        S += generate_TDMA(-90, 1250, 2/10e6, 1, "1") # GNSS, always on
        S += generate_TDMA(-90, 1575, 2/10e6, 1, "1") # GNSS, always on
        S += np.sum([generate_TDMA(-80, 1545+i/2., 2/10.5e3, 1, "1", NPSK=4) for i in range(10)], axis=0) # INMARSAT Aero Sat-to-Plane
        S += np.sum([generate_TDMA(-105, 1620+i, 2/31.5e3, 128/31.5e3, "1010", NPSK=4) for i in range(10)], axis=0) # IRIDIUM
        S += generate_TDMA(-90, 1980, 2/5e6, 1, "1") # Terrestrial fixed, always on
    
    # Relevant for the higher bands only
    if (band > "B3"):
        if (case==50):
            S += generate_TDMA(-90, 11800, 2/50e6, 1, "1") # GEOS wide area beam
        if (case == 90): # ~90% levels
            S += generate_TDMA(-90, 4430, 2/15e6, 1, "1") # Fixed terrestrial links
            S += generate_TDMA(-90, 6700, 2/30e6, 1, "1") # Fixed terrestrial links
            S += generate_RADAR(-65, 9375, 2e-6, 2e-3) # Aeronautical Weather radar
            S += generate_TDMA(-80, 11800, 2/50e6, 1, "1") # GEOS wide area beam
        elif (case == 98): # ~98% levels
            S += generate_TDMA(-60, 3150, 2/500e3, 1, "1101") # RNAV
            S += generate_TDMA(-50, 4430, 2/15e6, 1, "1") # Fixed terrestrial links
            S += generate_TDMA(-50, 6700, 2/30e6, 1, "1") # Fixed terrestrial links
            S += generate_RADAR(-55, 9375, 2e-6, 2e-3) # Aeronautical Weather radar
            S += generate_TDMA(-80, 11800, 2/50e6, 1, "1") # GEOS wide area beam
            S += np.sum([generate_RADAR(-63, 13500, 1.7e-6, 233e-6).delay(i*1.7e-6) for i in range(2)], axis=0) # LEO Earth observing radar e.g. GPM-Dual Precipitation Radar
    return S


def show(t_sample, S, title, f_resolution=1e6, nyqzone=1, minmax=True, overplot_fig=None):
    """
        Generates three figures to display a waterfall map (time vs frequency), time series amplitude and also a Power Spectral Density plot.
        The Blackman-Harris windowing function is applied in the FFT.
        @param t_sample: series of sampling instants [s].
        @param S: the (amplitude) signal series sampled at time instants t_sample.
        @param nyqzone: Nyquist zone in which the signal has been sampled (default 1).
        @param minmax: True to add min & max hold to PSD in faint grey lines (default True).
        @param overplot_fig: list of two figure numbers to overplot time & PSD onto (default None).
    """
    S = S*(1e3)**.5 # Convert from W to mW scale
    f_s = 1/np.diff(t_sample).mean()
    f_c = (nyqzone-1)*f_s/2
    if (nyqzone %2 == 0): # Flip sideband for display purposes by mixing with f_s/2
        S[::2] *= -1
    
    fftlen = (int(f_s/f_resolution) //2)*2  # Ensure it's at least an even number, or else FFT is extremely slow
    win = scipy.signal.windows.blackmanharris(fftlen, sym=False) # sym=False for a DFT-even window as recommended in [Harris 1976/78]
    win /= np.sum(np.abs(win)**2)**.5 # Normalized for processing gain of window
    
    # A spectrogram
    N_spectra = int(len(S) / fftlen)
    V = np.reshape(S[:N_spectra*fftlen],(-1,fftlen)) # Break into time chunks
    FV = np.fft.fft(V*win, axis=1, n=fftlen) * (1/f_s)**.5 # Normalize numpy FFT to "/sqrt(Hz)"
    FV = 2*np.abs(FV[:,:fftlen//2])**2
    FV[0] /= 2. # Half the DC bin
    FV = 10*np.log10(FV) # To "Power spectral density [dBm/Hz]"
    freq = f_c + np.fft.fftfreq(fftlen, 1.0/f_s)[:fftlen//2]
    time = np.arange(N_spectra)*fftlen/f_s + t_sample[0]
    freq /= MHz # MHz
    time /= 1e-6 # microsec
    pyplot.figure()
    pyplot.imshow(FV, aspect='auto', extent=(freq[0],freq[-1], time[-1],time[0]),
                  vmin=np.percentile(FV,66), vmax=np.percentile(FV,99), cmap='hot', interpolation=None)
    pyplot.xlabel('Frequency [MHz]'); pyplot.ylabel('Time [$\mu$s]')
    pyplot.title(title)

    # A time series
    STEP = int(1 + len(S)//1e5) # This limits figures to no more than 1e5 points (per block)
    pyplot.figure(overplot_fig[0] if overplot_fig else None)
    pyplot.plot(t_sample[::STEP]/1e-6, S[::STEP], '.-', alpha=0.4)
    pyplot.xlabel('time [microsec]'); pyplot.ylabel('Amplitude [V/sqrt(Ohm)]'); pyplot.grid(True)
    pyplot.title(title)

    # A PSD
    pyplot.figure(overplot_fig[1] if overplot_fig else None)
    if minmax:
        pyplot.plot(freq, np.max(FV,axis=0), 'k', freq, np.min(FV,axis=0), 'k', alpha=0.1)
    pyplot.plot(freq, np.mean(FV,axis=0))
    pyplot.xlabel('Frequency [MHz]'); pyplot.ylabel('PSD [dBm/Hz]'); pyplot.grid(True)
    pyplot.title(title)

    
def demo_siggen(n_pts=2**22): # A demo of just the signal generation capabilities
    f_s = 4000*MHz
    RFI = sig_generator(lambda t: np.sin(2*np.pi*0.9e9*t))
    for blk in range(3): # 3 blocks / frames of size n_pts to be generated
        t = (np.arange(0, n_pts)+n_pts*blk)*1./f_s
        DISH0_IN = RFI(t) # Continuous across blocks
        DISH1_IN = RFI.delay(0.2e-9)(t) # Also coherent w.r.t. un-delayed series
        show(t, DISH0_IN, "Input 0", overplot_fig=[100,101])
        show(t, DISH1_IN, "Input 1", overplot_fig=[100,201])


def demo_RFI(f_s=11e9, n_pts=2**22): # Demo of some RFI signals. Generates a PSD and a spectrogram
    t_sample = np.arange(n_pts)/f_s
    
    # Some signals
    S = generate_DME(-55, 1050, pattern="010100")+generate_DME(-55, 1115, pattern="100100")
    S = S(t_sample) + WhiteNoiseSignal(t_sample, Teq=0.1) # Add some receiver noise
    show(t_sample, S, "DME")
    
    # More signals
    S = generate_TDMA(-100, 235, 2/20e3, 1, "1") # DAB, always on
    S += generate_TDMA(-100, 680, 2/8e6, 1, "1", NPSK=8) # DTTV, always on
    S += generate_IMT(-80, 930, [945,950],["10","11"]) # IMT-900 with two voice channels, may be on simultaneously
    S += generate_SSR(-55, T_repeat=55e-6)
    S += generate_DME(-55, 1115, pattern="100100") +\
         generate_DME(-55, 1050, pattern="100010").delay(-6e-6) # For demo peaks DON'T coincide -- otherwise typically leads to clipping, which should just result in time-domain excision.
    S += generate_TDMA(-85, 1250, 2/10e6, 1, "1") # GNSS, always on
    S += np.sum([generate_TDMA(-70, 1545+i/2., 2/10.5e3, 1, "1", NPSK=4) for i in range(10)], axis=0) # INMARSAT Aero Sat-to-Plane
    S += np.sum([generate_TDMA(-105, 1620+i, 2/31.5e3, 128/31.5e3, "1010", NPSK=4) for i in range(10)], axis=0) # IRIDIUM
    S += generate_IMT(-90, 1950) # IMT-1900, always on
    S += generate_TDMA(-60, 3150, 2/500e3, 1, "1100") # RNAV
    S = S(t_sample) + WhiteNoiseSignal(t_sample, Teq=30) # Add some receiver noise
    show(t_sample, S, "demo_RFI", f_resolution=.25*MHz) # Resolves INMARSAT & IRIDIUM combs at expense of time resolution


def demo_RFI_UC(band, case, n_pts=2**22): # A demo of the RFI_UC function
    f_s = 4000*MHz
    RFI = RFI_UC(band, case)
    for blk in range(3): # 3 blocks / frames of size n_pts to be generated
        t = (np.arange(0, n_pts)+n_pts*blk)*1./f_s
        DISH0_IN = RFI(t) # Continuous across blocks
        show(t, DISH0_IN, "DISH[0] %s Input"%band, overplot_fig=[100,101])


if __name__ == '__main__':
#     demo_siggen()
#     demo_RFI()
    demo_RFI_UC("B2", 50)
